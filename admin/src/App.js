import React from "react";
import {Redirect, Route} from "react-router-dom";
import {
  HydraAdmin,
  hydraDataProvider as baseHydraDataProvider,
  fetchHydra as baseFetchHydra,
  ResourceGuesser
} from "@api-platform/admin";
import parseHydraDocumentation from "@api-platform/api-doc-parser/lib/hydra/parseHydraDocumentation";
import authProvider from "./authProvider";
import Login from "./layout/Login";
import {tags} from "./resourses/tags";
import {users} from "./resourses/users";
import {projects} from "./resourses/projects";
import {educational_levels} from "./resourses/educational_levels";
import {organizations} from "./resourses/organizations";
import {creative_works} from "./resourses/creative_works";
import {questions} from "./resourses/questions";
import {answers} from "./resourses/answers";
import {people} from "./resourses/people";
import {media_objects} from "./resourses/media_objects";
import {banks} from "./resourses/banks";
import {bank_accounts} from "./resourses/bank_accounts";
import {organization_unconfirmed_members} from "./resourses/organization_unconfirmed_members";
import {reviews} from "./resourses/reviews";
import {video_objects} from "./resourses/video_objects";
import {document_objects} from "./resourses/document_objects";
import {archive_objects} from "./resourses/archive_objects";
import {image_objects} from "./resourses/image_objects";

const entrypoint = process.env.REACT_APP_API_ENTRYPOINT;

const fetchHeaders = () => ({
  Authorization: `Bearer ${localStorage.getItem("token")}`,
});
const fetchHydra = (url, options = {}) =>
  localStorage.getItem("token")
    ? baseFetchHydra(url, {
      ...options,
      headers: new Headers(fetchHeaders()),
    })
    : baseFetchHydra(url, options);
const apiDocumentationParser = (entrypoint) =>
  parseHydraDocumentation(
    entrypoint,
    localStorage.getItem("token")
      ? {
        headers: new Headers(fetchHeaders()),
      }
      : {}
  ).then(
    ({api}) => ({api}),
    (result) => {
      // Only useful when the API endpoint is secured
      if (result.status === 401) {
        // Prevent infinite loop if the token is expired
        localStorage.removeItem("token");
        return Promise.resolve({
          api: result.api,
          customRoutes: [
            <Route
              path="/"
              render={() =>
                localStorage.getItem("token") ? (
                  window.location.reload()
                ) : (
                  <Redirect to="/login"/>
                )
              }
            />,
          ],
        });
      }
      return Promise.reject(result);
    }
  );
const dataProvider = baseHydraDataProvider(
  entrypoint,
  fetchHydra,
  apiDocumentationParser
);

export default () => (
  <HydraAdmin
    entrypoint={entrypoint}
    dataProvider={dataProvider}
    authProvider={authProvider}
    loginPage={Login}
  >
    {users}
    {people}
    {tags}
    {educational_levels}
    <ResourceGuesser name={"address_regions"} />
    {projects}
    {organizations}
    {organization_unconfirmed_members}
    {creative_works}
    {questions}
    {answers}
    {banks}
    {bank_accounts}
    {reviews}
    <ResourceGuesser name={"contacts"} />
    {media_objects}
    {video_objects}
    {document_objects}
    {archive_objects}
    {image_objects}
    <ResourceGuesser name={"locales"} />
    <ResourceGuesser name={"lessons"} />
    <ResourceGuesser name={"lesson_questions"} />
    <ResourceGuesser name={"lesson_answers"} />
    <ResourceGuesser name={"themes"} />
    <ResourceGuesser name={"subthemes"} />
  </HydraAdmin>
);


