import {
  CreateGuesser,
  EditGuesser,
  ListGuesser,
  ShowGuesser,
} from "@api-platform/admin";
import React from "react";
import {
  TextField,
  TextInput,
  ChipField,
  Resource,
  SelectInput,
  ReferenceInput
} from 'react-admin';
import {v4 as uuidv4} from 'uuid';
import FieldGuesser from "@api-platform/admin/lib/FieldGuesser";

const BankAccountList = props => (
  <ListGuesser {...props}>
    {/*<TextField source={"id"} />*/}
    <TextField source={"holder"} />
    <TextField source={"accountNumber"} />
    <TextField source={"additionalInfo"} />
    <FieldGuesser label="Bank" reference="banks" source="bank" addLabel={true} link={'show'}>
      <ChipField optionText={'name'} link={'show'} />
    </FieldGuesser>
    <FieldGuesser label="User" reference="users" source="owner" addLabel={true} link={'show'}>
      <ChipField optionText={'username'} link={'show'} />
    </FieldGuesser>
    {/*<FieldGuesser source={"createdAt"} />*/}
    {/*<FieldGuesser source={"updatedAt"} />*/}
  </ListGuesser>
);


const BankAccountShow = props => (
  <ShowGuesser {...props}>
    <TextField source={"id"} />
    <FieldGuesser source={"holder"} addLabel={true} />
    <FieldGuesser source={"accountNumber"} addLabel={true} />
    <FieldGuesser source={"additionalInfo"} addLabel={true} />
    <FieldGuesser label="Bank" reference="banks" source="bank" addLabel={true} link={'show'}>
      <ChipField optionText={'name'} link={'show'} />
    </FieldGuesser>
    <FieldGuesser label="User" reference="users" source="owner" addLabel={true} link={'show'}>
      <ChipField optionText={'username'} link={'show'} />
    </FieldGuesser>
    <FieldGuesser source={"createdAt"} addLabel={true} />
    <FieldGuesser source={"updatedAt"} addLabel={true} />
  </ShowGuesser>
);
const BankAccountEdit = props => (
  <EditGuesser {...props}>
    <TextInput disabled source="id" />
    <TextInput source={"holder"} />
    <TextInput source={"accountNumber"} />
    <TextInput source={"additionalInfo"} />
    <ReferenceInput label="Bank" reference="banks" source="bank">
      <SelectInput optionText={'name'} link={'show'} />
    </ReferenceInput>
    <ReferenceInput label="User" reference="users" source="owner">
      <SelectInput optionText={'username'} link={'show'} />
    </ReferenceInput>
  </EditGuesser>
);

const BankAccountCreate = props => (
  <CreateGuesser {...props}>
    <TextInput source={"id"} defaultValue={uuidv4()} />
    <TextInput source={"holder"} />
    <TextInput source={"accountNumber"} />
    <TextInput source={"additionalInfo"} />
    <ReferenceInput label="Bank" reference="banks" source="bank">
      <SelectInput optionText={'name'} link={'show'} />
    </ReferenceInput>
    <ReferenceInput label="User" reference="users" source="owner">
      <SelectInput optionText={'username'} link={'show'} />
    </ReferenceInput>
  </CreateGuesser>
);

export let bank_accounts = <Resource
  name="bank_accounts"
  list={BankAccountList}
  show={BankAccountShow}
  edit={BankAccountEdit}
  create={BankAccountCreate}
/>;
