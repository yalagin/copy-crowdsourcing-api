import {
  CreateGuesser,
  EditGuesser,
  FieldGuesser,
  ListGuesser,
  ShowGuesser
} from "@api-platform/admin";
import React from "react";
import {
  SelectInput,
  ReferenceArrayInput,
  ReferenceInput,
  SelectArrayInput,
  TextInput,
  SingleFieldList,
  ChipField,
  ReferenceArrayField,
  ReferenceField,
  DateField,
  NumberInput,
  DateTimeInput,
  Resource,
  TextField,
  AutocompleteInput, NumberField,BooleanField,BooleanInput
} from 'react-admin';
import { v4 as uuidv4 } from 'uuid';

const ProjectList = props => (
  <ListGuesser {...props}>
    <TextField source={"theme"} />
    <TextField source={"objective"} />
    <TextField source={"activity"} />
    <TextField source={"assessment"} />
    <TextField source={"award"} />
    <ReferenceArrayField  source="tags" reference="tags" link={'show'} >
      <SingleFieldList>
        <ChipField source="tag" link={'show'} />
      </SingleFieldList>
    </ReferenceArrayField>
    <ReferenceField source="educationalLevel" reference="educational_levels" label="Educational Level">
      <ChipField source="level" link={'show'} />
    </ReferenceField>
    <NumberField source={"minimumNumberOfQuestions"} />
    <DateField source={"startTime"} />
    <DateField source={"endTime"} />
  </ListGuesser>
);

const ProjectCreate = props => (
  <CreateGuesser {...props}>
    <TextInput source={"id"} defaultValue={uuidv4()} />
    <TextInput source={"theme"} multiline />
    <TextInput source={"objective"} multiline  />
    <TextInput source={"activity"} multiline/>
    <TextInput source={"assessment"} multiline />
    <TextInput source={"award"} multiline />
    <ReferenceArrayInput label="Tags" reference="tags" source="tags">
      <SelectArrayInput optionText={'tag'} />
    </ReferenceArrayInput>
    <ReferenceInput label="Educational Level" reference="educational_levels" source="educationalLevel">
      <SelectInput optionText={'level'} />
    </ReferenceInput>
    <NumberInput source={"minimumNumberOfQuestions"} />
    <DateTimeInput source={"startTime"} />
    <DateTimeInput source={"endTime"} />
  </CreateGuesser>
);
const ProjectEdit = props => (
  <EditGuesser {...props}>
    <TextInput disabled source="id" />
    {/*<InputGuesser source={"winner"} addLabel={true} />*/}
    <ReferenceInput
      source="winner"
      reference="creative_works"
      label="Winner creative work id"
      filterToQuery={searchText => ({ title: searchText })}
    >
      <AutocompleteInput optionText="id" />
    </ReferenceInput>
    <BooleanInput source={"emailSent"} label="email already send ? if true emails will not be sent on change, if false emails will be sent" />
    <TextInput source={"theme"} multiline />
    <TextInput source={"objective"} multiline  />
    <TextInput source={"activity"} multiline/>
    <TextInput source={"assessment"} multiline />
    <TextInput source={"award"} multiline />
    <ReferenceArrayInput label="Tags" reference="tags" source="tags">
      <SelectArrayInput optionText={'tag'} />
    </ReferenceArrayInput>
    <ReferenceInput label="Educational Level" reference="educational_levels" source="educationalLevel">
      <SelectInput optionText={'level'} />
    </ReferenceInput>
    <NumberInput source={"minimumNumberOfQuestions"} />
    <DateTimeInput source={"startTime"} />
    <DateTimeInput source={"endTime"} />
  </EditGuesser>
);

const ProjectShow = props => (
  <ShowGuesser {...props}>
    <TextField source={"id"} addLabel={true} />
    <TextField source={"theme"} addLabel={true} />
    <TextField source={"objective"} addLabel={true} />
    <TextField source={"activity"} addLabel={true} />
    <TextField source={"assessment"} addLabel={true} />
    <TextField source={"award"} addLabel={true} />
    <ReferenceField source="winner" reference="creative_works" label="Winner" link={'show'} >
      <TextField source={"id"} link={'show'} />
    </ReferenceField>
    <BooleanField source={"emailSent"} addLabel={true} />
    <ReferenceArrayField  source="tags" reference="tags" >
      <SingleFieldList>
        <ChipField source="tag" link={'show'} />
      </SingleFieldList>
    </ReferenceArrayField>
    <ReferenceField source="educationalLevel" reference="educational_levels" label="Educational Level" >
        <ChipField source="level" link={false} />
    </ReferenceField>
    <ReferenceArrayField source="organizations" reference="organizations" label="Organizations" >
      <SingleFieldList>
        <ChipField source="name" link={'show'} />
      </SingleFieldList>
    </ReferenceArrayField>
    <FieldGuesser source={"creativeWorks"} addLabel={true}  link={'show'} />
    <NumberField source={"minimumNumberOfQuestions"} addLabel={true} />
    <DateField source={"startTime"} addLabel={true}  showTime={true}/>
    <DateField source={"endTime"} addLabel={true}  showTime={true} />
    <DateField source={"createdAt"} addLabel={true}  showTime={true}/>
    <DateField source={"updatedAt"} addLabel={true} showTime={true} />
  </ShowGuesser>
);

export  let projects = <Resource
  name={"projects"}
  list={ProjectList}
  create={ProjectCreate}
  edit={ProjectEdit}
  show={ProjectShow}
/>
