import {
  CreateGuesser,
  EditGuesser,
  FieldGuesser,
  InputGuesser,
  ListGuesser,
  ShowGuesser
} from "@api-platform/admin";
import React from "react";
import {
  TextField,
  TextInput,
  SelectInput,
  ChipField,
  Resource
} from 'react-admin';
import {v4 as uuidv4} from 'uuid';

const ReviewList = props => (
  <ListGuesser {...props}>
    <FieldGuesser source={"author"}/>
    <FieldGuesser source={"itemReviewed"}/>
    <FieldGuesser source={"reviewBody"}/>
    <FieldGuesser source={"reviewRating"}/>
    <FieldGuesser label="organization" reference="organizations" source="organization" addLabel={true} link={'show'}>
      <ChipField optionText={'name'} link={'show'}/>
    </FieldGuesser>
    <FieldGuesser source={"tags"}/>
    <FieldGuesser source={"isSubmitted"}/>
  </ListGuesser>
);

const ReviewShow = props => (
  <ShowGuesser {...props}>
    <FieldGuesser source={"author"} addLabel={true}/>
    <FieldGuesser source={"itemReviewed"} addLabel={true}/>
    <FieldGuesser source={"reviewBody"} addLabel={true}/>
    <FieldGuesser source={"reviewRating"} addLabel={true}/>
    <FieldGuesser label="organization" reference="organizations" source="organization" addLabel={true} link={'show'}>
      <ChipField optionText={'name'} link={'show'}/>
    </FieldGuesser>
    <FieldGuesser source={"tags"} addLabel={true}/>
    <FieldGuesser source={"isSubmitted"} addLabel={true}/>
  </ShowGuesser>
);


const ReviewEdit = props => (
  <EditGuesser {...props}>
  </EditGuesser>
);

const ReviewCreate = props => (
  <CreateGuesser {...props}>
  </CreateGuesser>
);

export let reviews = <Resource
  name="reviews"
  list={ReviewList}
  show={ReviewShow}
  // edit={ReviewEdit}
  // create={ReviewCreate}
/>;
