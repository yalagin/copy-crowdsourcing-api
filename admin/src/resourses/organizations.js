import {
  CreateGuesser,
  EditGuesser,
  InputGuesser,
  ListGuesser,
  ShowGuesser,
  ResourceGuesser
} from "@api-platform/admin";
import React from "react";
import {
  Resource,
  TextInput,
  DateField,
  BooleanField,
  SelectInput,
  ReferenceInput,
  ChipField,
  ReferenceField,
  TextField,
  SingleFieldList,
  ReferenceArrayField,
  BooleanInput,
  Datagrid,
  ShowButton
} from 'react-admin';
import { v4 as uuidv4 } from 'uuid';


const OrganizationList = props => (
  <ListGuesser {...props}>
    <TextField source={"id"} />
    <TextField source={"name"} />
    <BooleanField source={"openForJoin"} />
    <BooleanField source={"locked"} />
    <TextField source={"status"} />
    <ReferenceArrayField  source="members" reference="users" >
      <SingleFieldList>
        <ChipField source="username" link={'show'} />
      </SingleFieldList>
    </ReferenceArrayField>
    <ReferenceField source="founder" reference="users" label="Founder" >
      <ChipField source="username" link={false} />
    </ReferenceField>
    <ReferenceField source="project" reference="projects" label="Project" >
      <ChipField source="theme" link={false} />
    </ReferenceField>
    {/*<ReferenceArrayField  source="organizationUnconfirmedMembers" reference="organization_unconfirmed_members" label="Unconfirmed members">*/}
    {/*  <SingleFieldList>*/}
    {/*    <ChipField source="id" link={'show'} />*/}
    {/*  </SingleFieldList>*/}
    {/*</ReferenceArrayField>*/}
    <ReferenceField source="creativeWork" reference="creative_works" label="work"  >
      <ChipField source="id" link={'show'} />
    </ReferenceField>
  </ListGuesser>
);

const OrganizationCreate = props => (
  <CreateGuesser {...props}>
    <TextInput source={"id"} defaultValue={uuidv4()} />
    <TextInput source={"name"} />
    {/*<InputGuesser source={"members"} />*/}
    <ReferenceInput label="User" reference="users" source="founder">
      <SelectInput optionText={'username'} />
    </ReferenceInput>
    <InputGuesser source={"image"} />
    <BooleanInput source={"openForJoin"} />
    <BooleanInput source={"locked"} />
    {/*<InputGuesser source={"creativeWork"} />*/}
    <ReferenceInput label="Work" reference="creative_works" source="creativeWork">
      <SelectInput optionText={'id'} />
    </ReferenceInput>
  </CreateGuesser>
);

const OrganizationEdit = props => (
  <EditGuesser {...props}>
    <TextInput disabled source="id" />
    <TextInput source={"name"} />
    {/*<InputGuesser source={"members"} />*/}
    <ReferenceInput label="founder" reference="users" source="founder">
      <SelectInput optionText={'username'} />
    </ReferenceInput>
    <ReferenceInput label="image" reference="media_objects" source="image">
      <SelectInput optionText={'contentUrl'} />
    </ReferenceInput>
    <BooleanInput source={"openForJoin"} />
    <BooleanInput source={"locked"} />
    <ReferenceInput label="Work" reference="creative_works" source="creativeWork">
      <SelectInput optionText={'id'} />
    </ReferenceInput>
  </EditGuesser>
);

const OrganizationShow = props => (
  <ShowGuesser {...props}>
    <TextField source={"id"} />
    <TextField source={"name"} />
    <TextField source={"status"} />
    <BooleanField source={"openForJoin"} />
    <BooleanField source={"locked"} />
    <ReferenceField source="founder" reference="users" label="Founder" >
      <ChipField source="username" link={false} />
    </ReferenceField>
    <ReferenceField source="project" reference="projects" label="Project" >
      <ChipField source="theme" link={false} />
    </ReferenceField>
    <ReferenceArrayField  source="members" reference="users" >
      <SingleFieldList>
        <ChipField source="username" link={'show'} />
      </SingleFieldList>
    </ReferenceArrayField>
    <ReferenceArrayField  source="organizationUnconfirmedMembers" reference="organization_unconfirmed_members" label="Unconfirmed members">
      <Datagrid>
        <TextField source="id" />
        <ReferenceField label="member" reference="users" source="member">
          <ChipField source={'username'} link={'show'} />
        </ReferenceField>
        <TextField source={"status"} />
        <ShowButton />
      </Datagrid>
    </ReferenceArrayField>
    <ReferenceField source="creativeWork" reference="creative_works" label="work"  >
      <ChipField source="id" link={'show'} />
    </ReferenceField>
    <ReferenceField source="image" reference="media_objects" label="image"  >
      <ChipField source="contentUrl" link={'show'} />
    </ReferenceField>
    <DateField source={"createdAt"} addLabel={true}  showTime={true}/>
    <DateField source={"updatedAt"} addLabel={true} showTime={true} />
  </ShowGuesser>
);

export  let organizations =
  <ResourceGuesser
    name={"organizations"}
    list={OrganizationList}
    edit={OrganizationEdit}
    create ={OrganizationCreate}
    show={OrganizationShow}
  />
