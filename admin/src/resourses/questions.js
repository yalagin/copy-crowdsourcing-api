import {
  FieldGuesser,
  ListGuesser,
  ShowGuesser
} from "@api-platform/admin";
import React from "react";
import {
  Resource,
  DateField,
} from 'react-admin';

const QuestionList = props => (
  <ListGuesser {...props}>
    <FieldGuesser source={"id"} />
    <FieldGuesser source={"creator"} />
    <FieldGuesser source={"isPartOf"} link={'show'} />
    <FieldGuesser source={"connectedAnswers"} link={'show'} />
    <FieldGuesser source={"description"} />
    <FieldGuesser source={"image"} />
  </ListGuesser>
);

const QuestionShow = props => (
  <ShowGuesser {...props}>
    <FieldGuesser source={"id"} addLabel={true} />
    <FieldGuesser source={"creator"} addLabel={true} />
    <FieldGuesser source={"isPartOf"} addLabel={true} link={'show'} />
    <FieldGuesser source={"connectedAnswers"} addLabel={true} link={'show'} />
    <FieldGuesser source={"description"} addLabel={true} />
    <FieldGuesser source={"image"} addLabel={true} />
    <DateField source={"createdAt"} addLabel={true}  showTime={true}/>
    <DateField source={"updatedAt"} addLabel={true} showTime={true} />
  </ShowGuesser>
);

// const QuestionEdit = props => (
//   <EditGuesser {...props}>
//     <InputGuesser source={"id"} />
//     <InputGuesser source={"creator"} />
//     <InputGuesser source={"isPartOf"} />
//     <InputGuesser source={"connectedAnswers"} />
//     <InputGuesser source={"description"} />
//     <InputGuesser source={"image"} />
//   </EditGuesser>
// );
// const QuestionCreate = props => (
//   <CreateGuesser {...props}>
//     <InputGuesser source={"id"} />
//     <InputGuesser source={"creator"} />
//     <InputGuesser source={"isPartOf"} />
//     <InputGuesser source={"connectedAnswers"} />
//     <InputGuesser source={"description"} />
//     <InputGuesser source={"image"} />
//   </CreateGuesser>
// );
export let questions = <Resource
  name="questions"
  list={QuestionList}
  edit={false}
  show={QuestionShow}
  create={false}
/>;
