import {
  ListGuesser,
  ShowGuesser
} from "@api-platform/admin";
import React from "react";
import {
  TextField,
  DateField,
  Resource,
  ReferenceField,
  ChipField
  } from 'react-admin';
import SimpleChipField from "../UI/SimpleChipField";
import { v4 as uuidv4 } from 'uuid';
import FieldGuesser from "@api-platform/admin/lib/FieldGuesser";
import ResourceGuesser from "@api-platform/admin/lib/ResourceGuesser";

const OrganizationMembersList = props => (
  <ListGuesser {...props}>
    <TextField source={"id"} />
    <ReferenceField label="organization" reference="organizations" source="organization">
      <ChipField source={'name'} link={'show'} />
    </ReferenceField>
    <ReferenceField label="member" reference="users" source="member">
      <ChipField source={'username'} link={'show'} />
    </ReferenceField>
    <TextField source={"status"} />
  </ListGuesser>
);

const OrganizationMembersShow = props => (
  <ShowGuesser {...props}>
    <TextField source={"id"} addLabel={true}/>
    <ReferenceField label="organization" reference="organizations" source="organization">
      <ChipField source={'name'} link={'show'} />
    </ReferenceField>
    <ReferenceField label="member" reference="users" source="member">
      <ChipField source={'username'} link={'show'} />
    </ReferenceField>
    <TextField source={"status"} />
    <DateField source={"createdAt"} addLabel={true}  showTime={true}/>
    <DateField source={"updatedAt"} addLabel={true} showTime={true} />
  </ShowGuesser>
);

export  let organization_unconfirmed_members = <Resource
  name={"organization_unconfirmed_members"}
  list={OrganizationMembersList}
  show={OrganizationMembersShow}
/>
