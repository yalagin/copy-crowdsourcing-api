import {
  CreateGuesser,
  EditGuesser,
  FieldGuesser,
  InputGuesser,
  ListGuesser,
  ShowGuesser
} from "@api-platform/admin";
import React from "react";
import {
  TextField,
  TextInput,
  Resource,
  DateField,
  SelectInput,
  ReferenceInput,
  ReferenceField,
  ChipField
} from 'react-admin';
import {v4 as uuidv4} from 'uuid';

const PersonList = props => (
  <ListGuesser {...props}>
    <ReferenceField label="User" reference="users" source="user">
      <ChipField source={'username'} link={'show'} />
    </ReferenceField>
    <ReferenceField source="addressRegion" reference="address_regions" label="Address Region" >
      <ChipField source="name" link={false} />
    </ReferenceField>
    <TextField source="aggregateRating"  />
    <TextField source={"worksFor"} />
    <DateField source={"createdAt"} />
    <DateField source={"updatedAt"} />
  </ListGuesser>
);
const PersonShow = props => (
  <ShowGuesser {...props}>
    <ReferenceField label="User" reference="users" source="user">
      <ChipField source={'username'} link={'show'} />
    </ReferenceField>
    <ReferenceField source="addressRegion" reference="address_regions" label="Address Region" >
      <ChipField source="name" link={false} />
    </ReferenceField>
    <FieldGuesser source={"worksFor"} addLabel={true} />
    <FieldGuesser source={"image"} addLabel={true} />
    <TextField source="aggregateRating"  />
    <DateField source={"createdAt"} addLabel={true}  showTime={true}/>
    <DateField source={"updatedAt"} addLabel={true} showTime={true} />
  </ShowGuesser>
);
const PersonCreate = props => (
  <CreateGuesser {...props}>
    <TextInput source={"id"} defaultValue={uuidv4()}/>
    <InputGuesser source={"addressRegion"} />
    <InputGuesser source={"worksFor"} />
    <ReferenceInput label="User" reference="users" source="user">
      <SelectInput optionText={'username'} />
    </ReferenceInput>
    <ReferenceInput label="image" reference="media_objects" source="image">
      <SelectInput optionText={'contentUrl'} />
    </ReferenceInput>
  </CreateGuesser>
);
const PersonEdit = props => (
  <EditGuesser {...props}>
    <InputGuesser source={"addressRegion"} />
    <InputGuesser source={"worksFor"} />
    <ReferenceInput label="User" reference="users" source="user">
      <SelectInput optionText={'username'} />
    </ReferenceInput>
    <ReferenceInput label="image" reference="media_objects" source="image">
      <SelectInput optionText={'contentUrl'} />
    </ReferenceInput>
  </EditGuesser>
);
export let people = <Resource
  name="people"
  list={PersonList}
  edit={PersonEdit}
  show={PersonShow}
  create={PersonCreate}
/>;
