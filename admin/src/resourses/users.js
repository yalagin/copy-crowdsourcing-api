import {
  CreateGuesser,
  EditGuesser,
  ListGuesser,
  ShowGuesser
} from "@api-platform/admin";
import React from "react";
import {
  SelectInput,
  SingleFieldList,
  ArrayField,
  PasswordInput,
  ReferenceArrayInput,
  BooleanField,
  ReferenceArrayField,
  ChipField,
  TextField,
  SelectArrayInput,
  TextInput,
  Resource,
  Datagrid,
  ReferenceField,
  Button,
  List,
  FunctionField,
  ShowButton,
  DeleteButton} from 'react-admin';
import SimpleChipField from "../UI/SimpleChipField";
import { v4 as uuidv4 } from 'uuid';

const UserList = props => {



  const renderButton = (data) => {
    const imitateUser = () => {
      console.log(data);
      const win = window.open(process.env.REACT_APP_FRONTEND_ENTRYPOINT+'/imitate/'+data.email, '_blank');
      win.focus();
    };
    return <Button
      label={'Imitate'}
      onClick={imitateUser}
      source={"id"}
    />;
  }


  return (
    <List {...props}>
      <Datagrid>
        <TextField source={"id"}/>
        <TextField source={"email"}/>
        <TextField source={"username"}/>
        <BooleanField source={"enabled"}/>
        <ReferenceArrayField source="tags" reference="tags">
          <SingleFieldList>
            <ChipField source="tag" link={'show'}/>
          </SingleFieldList>
        </ReferenceArrayField>
        <ArrayField source={"roles"} addLabel={true}>
          <SingleFieldList>
            <SimpleChipField/>
          </SingleFieldList>
        </ArrayField>
        <FunctionField source={"id"} render={renderButton}/>
        <ShowButton />
        <DeleteButton />
      </Datagrid>
    </List>
  );
};
const UserShow = props => (
  <ShowGuesser {...props}>
    <TextField source={"id"} addLabel={true} />
    <TextField source={"email"} addLabel={true} />
    <TextField source={"username"} addLabel={true} />
    <BooleanField source={"enabled"} />
    <ReferenceArrayField  source="tags" reference="tags" >
      <SingleFieldList>
        <ChipField source="tag" link={'show'} />
      </SingleFieldList>
    </ReferenceArrayField>
    <ArrayField source={"roles"} addLabel={true} >
      <SingleFieldList>
        <SimpleChipField />
      </SingleFieldList>
    </ArrayField>
    <ReferenceArrayField  source="organizations" reference="organizations" label="member in organizations">
      <Datagrid>
        <TextField source="id" />
        <TextField source={"name"} />
        <ReferenceField source="founder" reference="users" label="Founder" >
          <ChipField source="username" link={false} />
        </ReferenceField>
        <ReferenceField source="creativeWork" reference="creative_works" label="work"  >
          <ChipField source="id" link={'show'} />
        </ReferenceField>
        <ShowButton />
      </Datagrid>
    </ReferenceArrayField>
    <ReferenceArrayField  source="organizationUnconfirmedMembers" reference="organization_unconfirmed_members" label="Unconfirmed member in organization">
      <Datagrid>
        <TextField source="id" />
        <ReferenceField label="organization" reference="organizations" source="organization">
          <ChipField source={'name'} link={'show'} />
        </ReferenceField>
        <TextField source={"status"} />
        <ShowButton />
      </Datagrid>
    </ReferenceArrayField>
    <ReferenceArrayField  source="bankAccounts" reference="bank_accounts" label="Bank Accounts">
      <Datagrid>
        <TextField source={"holder"} />
        <TextField source={"accountNumber"} />
        <TextField source={"additionalInfo"} />
        <ShowButton />
      </Datagrid>
    </ReferenceArrayField>

  </ShowGuesser>
);
const UserEdit = props => (
  <EditGuesser {...props}>
    <TextInput disabled source="id" />
    <TextInput source={"username"} />
    <TextInput source={"email"} />
    <ReferenceArrayInput label="Tags" reference="tags" source="tags">
      <SelectArrayInput optionText={'tag'} />
    </ReferenceArrayInput>
    <SelectInput source="role" choices={[
      { id: 'ROLE_TEACHER', name: 'Teacher' },
      { id: 'ROLE_DESIGNER', name: 'Designer' },
    ]} />
    <ArrayField source={"roles"} addLabel={true} >
      <SingleFieldList>
        <SimpleChipField />
      </SingleFieldList>
    </ArrayField>

  </EditGuesser>
);
const UserCreate = props => (
  <CreateGuesser {...props}>
    <TextInput source={"id"} defaultValue={uuidv4()}/>
    <TextInput source={"email"} />
    <TextInput source={"username"} />
    <PasswordInput source={"password"} />
    <SelectInput source="role" choices={[
      { id: 'ROLE_TEACHER', name: 'Teacher' },
      { id: 'ROLE_DESIGNER', name: 'Designer' },
    ]} />
    <ReferenceArrayInput label="Tags" reference="tags" source="tags">
      <SelectArrayInput optionText={'tag'} />
    </ReferenceArrayInput>
  </CreateGuesser>
);

export  let users = <Resource
  name="users"
  list={UserList}
  show={UserShow}
  edit={UserEdit}
  create={UserCreate}
/>;
