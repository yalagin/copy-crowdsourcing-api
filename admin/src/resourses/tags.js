import {
  CreateGuesser,
  EditGuesser,
  FieldGuesser,
  InputGuesser,
  ListGuesser,
  ShowGuesser
} from "@api-platform/admin";
import React from "react";
import {
  TextField,
  TextInput,
  SelectInput,
  DateField,
  Resource
} from 'react-admin';
import {v4 as uuidv4} from 'uuid';

const TagsList = props => (
  <ListGuesser {...props}>
    <TextField source={"id"}/>
    <TextField source={"role"}/>
    <FieldGuesser source={"tag"}/>
    <FieldGuesser source={"name"}/>
    <FieldGuesser source={"value"}/>
    <DateField source={"createdAt"} addLabel={true}  />
    <DateField source={"updatedAt"} addLabel={true}  />
  </ListGuesser>
);
const TagsShow = props => (
  <ShowGuesser {...props}>
    <TextField source={"id"} addLabel={true}/>
    <TextField source={"role"} addLabel={true}/>
    <FieldGuesser source={"tag"} addLabel={true}/>
    <FieldGuesser source={"name"}/>
    <FieldGuesser source={"value"}/>
    <DateField source={"createdAt"} addLabel={true}  showTime={true}/>
    <DateField source={"updatedAt"} addLabel={true} showTime={true} />
  </ShowGuesser>
);
const TagsEdit = props => (
  <EditGuesser {...props}>
    <TextInput disabled source="id"/>
    <SelectInput source="role" choices={[
      {id: 'ROLE_TEACHER', name: 'Teacher'},
      {id: 'ROLE_DESIGNER', name: 'Designer'},
    ]}/>
    <InputGuesser source={"tag"}/>
    <InputGuesser source={"name"}/>
    <InputGuesser source={"value"}/>
  </EditGuesser>
);
const TagsCreate = props => (
  <CreateGuesser {...props}>
    <TextInput source={"id"} defaultValue={uuidv4()}/>
    <SelectInput source="role" choices={[
      {id: 'ROLE_TEACHER', name: 'Teacher'},
      {id: 'ROLE_DESIGNER', name: 'Designer'},
    ]}/>
    <InputGuesser source={"tag"}/>
    <InputGuesser source={"name"}/>
    <InputGuesser source={"value"}/>
  </CreateGuesser>
);

export let tags = <Resource
  name="tags"
  list={TagsList}
  show={TagsShow}
  edit={TagsEdit}
  create={TagsCreate}
/>;
