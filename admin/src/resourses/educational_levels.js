import {
  CreateGuesser,
  EditGuesser,
  FieldGuesser,
  ListGuesser,
  ShowGuesser
} from "@api-platform/admin";
import React from "react";
import {
  TextField,
  TextInput,
  Resource,
  DateField,
  NumberInput
} from 'react-admin';
import {v4 as uuidv4} from 'uuid';


const EducationalLevelList = props => (
  <ListGuesser {...props}>
    <TextField source={"id"}/>
    <FieldGuesser source={"level"} />
    <FieldGuesser source={"ordering"} />
    <DateField source={"createdAt"} addLabel={true}  />
    <DateField source={"updatedAt"} addLabel={true}  />
  </ListGuesser>
);

const EducationalLevelEdit = props => (
  <EditGuesser {...props}>
    <TextInput disabled source="id"/>
    <TextInput source={"level"} />
    <NumberInput source={"ordering"} />
  </EditGuesser>
);

const EducationalLevelShow = props => (
  <ShowGuesser {...props}>
    <TextField source={"id"}/>
    <FieldGuesser source={"level"} addLabel={true} />
    <FieldGuesser source={"ordering"} addLabel={true} />
    <DateField source={"createdAt"} addLabel={true}  showTime={true}/>
    <DateField source={"updatedAt"} addLabel={true} showTime={true} />
  </ShowGuesser>
);

const EducationalLevelCreate = props => (
  <CreateGuesser {...props}>
    <TextInput source={"id"} defaultValue={uuidv4()}/>
    <TextInput source={"level"} />
    <NumberInput source={"ordering"} />
  </CreateGuesser>
);


export let educational_levels = <Resource
  name="educational_levels"
  list={EducationalLevelList}
  edit={EducationalLevelEdit}
  show={EducationalLevelShow}
  create={EducationalLevelCreate}
/>;
