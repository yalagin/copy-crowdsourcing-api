import {
  ListGuesser,
  ShowGuesser
} from "@api-platform/admin";
import React from "react";
import {
  FileInput,
  Resource,
  Create,
  SimpleForm,
  FileField,
  DeleteButton,
  TextField
} from 'react-admin';


const MediaObjectList = props => (
  <ListGuesser {...props}>
    <TextField source={"contentUrl"} />
    <DeleteButton/>
  </ListGuesser>
);


const MediaObjectCreate = props => (
  <Create {...props}>
    <SimpleForm>
      <FileInput source="media_objects" label="files" >
        <FileField source="file" title="file" />
      </FileInput>
    </SimpleForm>
  </Create>
);

const MediaObjectShow = props => (
  <ShowGuesser {...props}>
    <TextField source={"contentUrl"} />
    <DeleteButton/>
  </ShowGuesser>
);

export let document_objects = <Resource
  name="document_objects"
  list={MediaObjectList}
  show={MediaObjectShow}
  edit={false}
  create={false}
/>;
