import {
  EditGuesser,
  FieldGuesser,
  InputGuesser,
  ListGuesser,
  ShowGuesser
} from "@api-platform/admin";
import React from "react";
import {
  Resource,
  DateField,
  ChipField
} from 'react-admin';

const CreativeWorkList = props => (
  <ListGuesser {...props}>
    <FieldGuesser source={"id"} />
    <FieldGuesser label="organization that created work" reference="organizations" source="creator" addLabel={true} link={'show'}>
      <ChipField optionText={'name'} link={'show'} />
    </FieldGuesser>
    <FieldGuesser source={"createdFor"} link={'show'}/>
    <FieldGuesser source={"hasParts"} link={'show'} />
    <FieldGuesser source={"isSubmitted"} />
    <FieldGuesser source={"rank"} />
  </ListGuesser>
);
const CreativeWorkShow = props => (
  <ShowGuesser {...props}>
    <FieldGuesser source={"id"} addLabel={true} />
    <FieldGuesser label="organization that created work" reference="organizations" source="creator" addLabel={true} link={'show'}>
      <ChipField optionText={'name'} link={'show'} />
    </FieldGuesser>
    <FieldGuesser source={"createdFor"} addLabel={true} link={'show'}/>
    <FieldGuesser source={"hasParts"} addLabel={true} link={'show'}/>
    <FieldGuesser source={"isSubmitted"} addLabel={true} />
    <FieldGuesser source={"rank"} addLabel={true} />
    <FieldGuesser source={"mediaObjects"} addLabel={true} />
    <DateField source={"createdAt"} addLabel={true}  showTime={true}/>
    <DateField source={"updatedAt"} addLabel={true} showTime={true} />
  </ShowGuesser>
);
const CreativeWorkEdit = props => (
  <EditGuesser {...props}>
    <InputGuesser source={"rank"} />
  </EditGuesser>
);

//
// const CreativeWorkCreate = props => (
//   <CreateGuesser {...props}>
//     <InputGuesser source={"id"} />
//     <InputGuesser source={"creator"} />
//     <InputGuesser source={"about"} />
//     <InputGuesser source={"hasParts"} />
//     <InputGuesser source={"isSubmitted"} />
//     <InputGuesser source={"rank"} />
//   </CreateGuesser>
// );
export let creative_works = <Resource
  name="creative_works"
  list={CreativeWorkList}
  edit={CreativeWorkEdit}
  show={CreativeWorkShow}
  create={false}
/>;
