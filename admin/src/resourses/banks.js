import {
  CreateGuesser,
  EditGuesser,
  ListGuesser,
  ShowGuesser
} from "@api-platform/admin";
import React from "react";
import {
  TextField,
  TextInput,
  DateField,
  Resource,
  ReferenceArrayField,
  SingleFieldList,
  ChipField
} from 'react-admin';
import {v4 as uuidv4} from 'uuid';

const BanksList = props => (
  <ListGuesser {...props}>
    <TextField source={"id"}/>
    <TextField source={"name"}/>
    <DateField source={"createdAt"} addLabel={true}  />
    <DateField source={"updatedAt"} addLabel={true}  />
  </ListGuesser>
);
const BanksShow = props => (
  <ShowGuesser {...props}>
    <TextField source={"id"} addLabel={true}/>
    <TextField source={"name"} addLabel={true}/>
    <DateField source={"createdAt"} addLabel={true}  showTime={true}/>
    <DateField source={"updatedAt"} addLabel={true} showTime={true} />
    <ReferenceArrayField  source="bankAccounts" reference="bank_accounts" >
      <SingleFieldList>
        <ChipField source="holder" link={'show'} />
      </SingleFieldList>
    </ReferenceArrayField>
  </ShowGuesser>
);
const BanksEdit = props => (
  <EditGuesser {...props}>
    <TextInput disabled source="id"/>
    <TextInput source={"name"}/>
  </EditGuesser>
);
const BanksCreate = props => (
  <CreateGuesser {...props}>
    <TextInput source={"id"} defaultValue={uuidv4()}/>
    <TextInput source={"name"}/>
  </CreateGuesser>
);

export let banks = <Resource
  name="banks"
  list={BanksList}
  show={BanksShow}
  edit={BanksEdit}
  create={BanksCreate}
/>;
