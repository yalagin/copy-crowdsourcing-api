import {
  FieldGuesser,
  ListGuesser,
  ShowGuesser
} from "@api-platform/admin";
import React from "react";
import {
  BooleanField,
  Resource,
  DateField,
} from 'react-admin';

const AnswerList = props => (
  <ListGuesser {...props}>
    <FieldGuesser source={"id"} />
    <FieldGuesser source={"creator"}  link={'show'}/>
    <FieldGuesser source={"parentItem"}  link={'show'}/>
    <FieldGuesser source={"description"} />
    <BooleanField source={"correct"} />
  </ListGuesser>
);
const AnswerShow = props => (
  <ShowGuesser {...props}>
    <FieldGuesser source={"id"} addLabel={true} />
    <FieldGuesser source={"creator"} addLabel={true}  link={'show'}/>
    <FieldGuesser source={"parentItem"} addLabel={true}  link={'show'}/>
    <FieldGuesser source={"description"} addLabel={true} />
    <BooleanField source={"correct"} addLabel={true} />
    <DateField source={"createdAt"} addLabel={true}  showTime={true}/>
    <DateField source={"updatedAt"} addLabel={true} showTime={true} />
  </ShowGuesser>
);

export let answers = <Resource
  name="answers"
  list={AnswerList}
  edit={false}
  show={AnswerShow}
  create={false}
/>;
