# Titik Pintar Content Crowdsourcing API

How to run a local instance
---------------------------

### Prerequisites

Install [Docker](https://www.docker.com/) and [Docker Compose](https://docs.docker.com/compose/install).

#### OSX

Use [Docker for Mac](https://www.docker.com/docker-mac) which will provide you both `docker` and `docker-compose`.

#### Windows

Use [Docker for Windows](https://www.docker.com/docker-windows) which will provide you both `docker` and `docker-compose`.
Depending on your platform, Docker could be installed as Native or you have to install Docker toolbox which use VirtualBox instead of Hyper-V causing a lot a differences in implementations.
If you have the luck to have a CPU that supports native Docker you can [share your hard disk as a virtual volume for your appliances](https://blogs.msdn.microsoft.com/stevelasker/2016/06/14/configuring-docker-for-windows-volumes/).
 
Also, disabling WSL 2 engine, on docker, might help.
 
#### Linux

Follow [the instructions for your distribution](https://docs.docker.com/install/). `docker-compose` binary is to be installed independently.
Make sure:
- to install `docker-compose` [following instructions](https://docs.docker.com/compose/install/) to get the **latest version**.
- to follow the [post-installation steps](https://docs.docker.com/install/linux/linux-postinstall/).

### Run the application

#### Start the Docker containers

```
docker-compose up
```

#### Open the platform in your browser
```
open http://localhost
```

Navigate to https://localhost:8443/

#### Migration

Run `docker-compose exec php bin/console doctrine:migrations:migrate` to execute migration. You may want to create an alias to make your life easier.

#### Load Fixtures
Run `docker-compose exec php bin/console hautelook:fixtures:load` to load data fixtures into database.

#### Running tests

Run `docker-compose exec php vendor/bin/simple-phpunit` to execute the unit tests.

#### Documentation

The official project documentation is available **[on the API Platform website](https://api-platform.com)**.

#### Troubleshoot JWT 

```shell script
docker-compose exec php sh -c '
    set -e
    apk add openssl
    mkdir -p config/jwt
    jwt_passphrase=${JWT_PASSPHRASE:-$(grep ''^JWT_PASSPHRASE='' .env | cut -f 2 -d ''='')}
    echo "$jwt_passphrase" | openssl genpkey -out config/jwt/private.pem -pass stdin -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
    echo "$jwt_passphrase" | openssl pkey -in config/jwt/private.pem -passin stdin -out config/jwt/public.pem -pubout
    setfacl -R -m u:www-data:rX -m u:"$(whoami)":rwX config/jwt
    setfacl -dR -m u:www-data:rX -m u:"$(whoami)":rwX config/jwt
'
```

#### fix permissions problems 
```
docker-compose run --rm php chown -R $(id -u):$(id -g) .
```
###### if you are pulling updated version form git and got errors please run that will update composer and rerun migrations with fixtures
```
docker-compose up --build --force-recreate
```

#### Varnish 

Make sure varnish runs at port 80, and nginx at port 8080

```
sudo service php7.4-fpm restart && sudo service nginx restart && sudo /etc/init.d/varnish restart \
&& sudo varnishadm param.set http_req_hdr_len 655360 \
&& sudo varnishadm param.set http_req_size 655360 \
&& sudo varnishadm param.set http_resp_hdr_len 655360 \
&& sudo varnishadm param.set http_resp_size 983040 \
&& sudo varnishadm param.set workspace_backend 256k \
&& sudo varnishadm param.set workspace_client 256k \
&& sudo varnishadm param.set http_max_hdr 256
```
and in nginx conf something like
```
server {
    listen 443 ssl; # managed by Certbot
    listen [::]:443 ssl;
    server_name dev.api.sahabatpintar.id;
    port_in_redirect off;

    ssl_certificate /etc/letsencrypt/live/dev.api.sahabatpintar.id/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/dev.api.sahabatpintar.id/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-XSS-Protection "1; mode=block";
    add_header X-Content-Type-Options "nosniff";

    location / {
        proxy_pass http://127.0.0.1:80;
        proxy_set_header Host $http_host;
        proxy_set_header X-Forwarded-Host $http_host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto https;
        proxy_set_header HTTPS "on";

        proxy_redirect      http://localhost:80 https://dev.api.sahabatpintar.id;

        fastcgi_buffering on;
        fastcgi_buffer_size 512k;
        fastcgi_buffers 16 512k;
        fastcgi_busy_buffers_size 512k;

        proxy_busy_buffers_size   512k;
        proxy_buffers   16 512k;
        proxy_buffer_size   512k;
    }
}

server {
    listen 8080;
    listen [::]:8080;
    server_name dev.api.sahabatpintar.id;
    root /var/www/dev.api.sahabatpintar.id/api/public;
    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }
 
    index index.html index.htm index.php;
    port_in_redirect off;

    charset utf-8;

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location ~ \.php$ {
        fastcgi_pass unix:/var/run/php/php7.4-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        include fastcgi_params;

        fastcgi_buffering on;
        fastcgi_buffer_size 512k;
        fastcgi_buffers 16 512k;
        fastcgi_busy_buffers_size 512k;

        proxy_busy_buffers_size   512k;
        proxy_buffers   16 512k;
        proxy_buffer_size   512k;

        }

}
```
