<?php


namespace App\Tests\Functional;


use App\Entity\EducationalLevel;
use App\Entity\Organization;
use App\Entity\OrganizationUnconfirmedMembers;
use App\Entity\Project;
use App\Entity\Tags;
use App\Entity\User;
use App\Test\CustomApiTestCase;
use Faker\Provider\Uuid;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;

class OrganizationResourceTest extends CustomApiTestCase
{
    use ReloadDatabaseTrait;

    public function testCreateOrganization()
    {
        $client = self::createClient();
        list($user, $token) = $this->createUserAndLogIn($client, 'sdafsdfasvth@example.com', 'fooBar12345');
        list($user2, $token2) = $this->createUserAndLogIn($client, 'sdfsdfa@example.com', 'fooBar12345');
        $em = $this->getEntityManager();
        $project = $em->getRepository(Project::class)->findOneBy([]);
        $client->request('POST', '/organizations', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [
                'id' => Uuid::uuid(),
                'name' => 'Penguins',
                'project' => '/projects/' . $project->getId(),
            ]
        ]);
        $this->assertResponseIsSuccessful();


        $this->assertJsonContains([
            'name' => 'Penguins',
            'founder' => '/users/' . $user->getId()
        ]);

        $client->request('POST', '/organizations', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [
                'id' => Uuid::uuid(),
                'name' => 'Penguins2',
                'members' => [
                    '/users/' . $user->getId(),
                    '/users/' . $user2->getId(),
                ],
                'project' => '/projects/' . $project->getId(),
            ],
         ]);
        $this->assertResponseStatusCodeSame(400);

        $client->request('GET', '/organizations', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ]
        ]);

        $this->assertJsonContains([
            'hydra:totalItems' => 31,
        ]);
    }

    public function testApplyToOrganization()
    {
        $client = self::createClient();
        list($user, $token) = $this->createUserAndLogIn($client, 'cheeseplease@example.com', 'fooBar12345');
        list($user2, $token2) = $this->createUserAndLogIn($client, 'cheeseplease2@example.com', 'fooBar12345');
        $project = $this->getEntityManager()->getRepository(Project::class)->findOneBy([]);
        $organizationUuid = Uuid::uuid();
        $client->request('POST', '/organizations', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [
                'id' => $organizationUuid,
                'name' => 'Penguins',
                'members' => [
                    '/users/' . $user->getId(),
                ],
                'project' => '/projects/' . $project->getId(),
            ]
        ]);
        $this->assertResponseIsSuccessful();
        $uuid = Uuid::uuid();
        $client->request('POST', '/organization_unconfirmed_members/join', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token2
            ],
            'json' => [
                'id' => $uuid,
                'organization' => '/organizations/' . $organizationUuid,
                'member' => '/users/' . $user2->getId(),
                'status' => OrganizationUnconfirmedMembers::STATUS_INVITED,
            ]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'status' => OrganizationUnconfirmedMembers::STATUS_WANTED_TO_JOIN,
        ]);
        $this->assertEquals(
            OrganizationUnconfirmedMembers::STATUS_WANTED_TO_JOIN,
            $this->getEntityManager()->getRepository(OrganizationUnconfirmedMembers::class)->find($uuid)->getStatus()
        );
    }

    public function testInviteToOrganization()
    {
        $client = self::createClient();
        list($user, $token) = $this->createUserAndLogIn($client, 'cheeseplease3@example.com', 'fooBar12345');
        $project = $this->getEntityManager()->getRepository(Project::class)->findOneBy([]);
        $organizationUuid = Uuid::uuid();
        $client->request('POST', '/organizations', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [
                'id' => $organizationUuid,
                'name' => 'Penguins',
                'members' => [
                    '/users/' . $user->getId(),
                ],
                'project' => '/projects/' . $project->getId(),
            ]
        ]);
        $this->assertResponseIsSuccessful();

        list($user2, $token2) = $this->createUserAndLogIn($client, 'cheeseplease6@example.com', 'fooBar12345');
        $client->request('POST', '/organization_unconfirmed_members/invite', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token2
            ],
            'json' => [
                'id' => Uuid::uuid(),
                'organization' => '/organizations/' . $organizationUuid,
                'member' => '/users/' . $user2->getId()
            ]
        ]);
        $this->assertResponseStatusCodeSame(400);
        $invitationId = Uuid::uuid();
        $client->request('POST', '/organization_unconfirmed_members/invite', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [
                'id' => $invitationId,
                'organization' => '/organizations/' . $organizationUuid,
                'member' => '/users/' . $user2->getId()
            ]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'status' => OrganizationUnconfirmedMembers::STATUS_INVITED,
        ]);

        $client->request('PUT', "/organization_unconfirmed_members/$invitationId/accept-invitation", [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token2
            ],
            'json' => [

            ]
        ]);
        $this->assertResponseIsSuccessful();

        //double invitation should not be possible
        $client->request('POST', '/organization_unconfirmed_members/invite', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [
                'id' => Uuid::uuid(),
                'organization' => '/organizations/' . $organizationUuid,
                'member' => '/users/' . $user2->getId()
            ]
        ]);
        $this->assertResponseStatusCodeSame(400);
        $this->assertJsonContains([
            'hydra:description' => 'You are already member of organization for this project'
        ]);

        $client->request('POST', '/organization_unconfirmed_members/invite', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [
                'id' => Uuid::uuid(),
                'organization' => '/organizations/' . $organizationUuid,
                'member' => '/users/' . $user2->getId()
            ]
        ]);
        $this->assertResponseStatusCodeSame(400);
    }

    public function testCloningOrganization()
    {
        //create team and add to project
        $client = self::createClient();
        list($user, $founderToken) = $this->createUserAndLogIn($client, 'cheeseplease@example.com', 'fooBar12345');
        $em = $this->getEntityManager();
        list($user2, $memberToken) = $this->createUserAndLogIn($client, 'cheesepleaseanotherone@example.com', 'fooBar12312');
        list($userNotInOrganization, $tokenNotInOrganization) = $this->createUserAndLogIn($client, 'cheeseplease3@example.com', 'fooBar12345');
        $project = $em->getRepository(Project::class)->findOneBy([]);

        $organizationsUuid = Uuid::uuid();
        $client->request('POST', '/organizations', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $founderToken
            ],
            'json' => [
                'id' => $organizationsUuid,
                'name' => 'Penguins',
                'project' => '/projects/' . $project->getId(),

            ]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'name' => 'Penguins',
            'founder'=>'/users/'.$user->getId()
        ]);
        // add second user to organization
        $user2 = $this->getEntityManager()->getRepository(User::class)->find($user2->getId());
        $this->getEntityManager()->getRepository(Organization::class)->find($organizationsUuid)->addMember($user2)->setIsLocked(true);
        $this->getEntityManager()->flush();

        $creativeWorkUuid = Uuid::uuid();
        $client->request('POST', '/creative_works', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $founderToken
            ],
            'json' => [
                'id' => $creativeWorkUuid,
                'creator' => '/organizations/' . $organizationsUuid,
                'createdFor' => '/projects/' . $project->getId(),
            ]
        ]);
        $this->assertResponseIsSuccessful();

        //create new project by admin
        list($adminUser, $adminToken)  = $this->createAdminAndLogIn($client, 'cheesepleasefromadmin@example.com', 'Admin123testing');

        $em = $this->getEntityManager();
        $tag = $em->getRepository(Tags::class)->findOneBy(['role'=>User::ROLE_TEACHER]);
        $educationalLevel = $em->getRepository(EducationalLevel::class)->findOneBy([]);


        $endDate = (string)date('Y-m-d H:i:s', time() + 604800);
        $newProjectId = Uuid::uuid();
        $startDate = (string)date('Y-m-d H:i:s');
        $client->request('POST', '/projects', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer '.$adminToken
            ],
            'json' => [
                'id' => $newProjectId,
                "theme"=> "Arithmetic with blank Numbers | Rounding Off and Approximation",
                "objective"=> "Demonstrate rounding and approximation from addition, subtraction, multiplication and division of two whole numbers and/or fractions",
                "activity"=> "Learning about rounding whole numbers and solve mathematical questions",
                "assessment"=> "Understand rounding and approximation from addition, subtraction, multiplication and division of two whole numbers and/or fractions",
                "tags"=> [
                    'tags/'.$tag->getId()
                ],
                "educationalLevel"=> 'educational_levels/'.$educationalLevel->getId(),
                "startTime"=> $startDate,
                "endTime"=> $endDate,
            ]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            "objective"=> "Demonstrate rounding and approximation from addition, subtraction, multiplication and division of two whole numbers and/or fractions",
            'id' => $newProjectId,
        ]);

        $newOrganizationUuid = Uuid::uuid();
        $client->request('POST', "/organizations/$organizationsUuid/clone", [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $founderToken
            ],
            'json' => [
                'cloneForProject' => $newProjectId,
                'newOrganizationId' => $newOrganizationUuid
            ]
        ]);
        $this->assertResponseIsSuccessful();
        $organization = $em->getRepository(Organization::class)->find($newOrganizationUuid);
        $this->assertEquals($organization->getId(),$newOrganizationUuid);


        $invitedMembers = $em->getRepository(OrganizationUnconfirmedMembers::class)->findby(['organization'=>$newOrganizationUuid]);
        $this->assertEquals(1,count($invitedMembers));
        $this->assertEquals($user2->getId(),$invitedMembers[0]->getMember()->getId());
        $this->assertEquals(OrganizationUnconfirmedMembers::STATUS_INVITED,$invitedMembers[0]->getStatus());

    }

    public function testUserCanNotInviteToOrganizationOrExitIfOrganizationIsLockedAndWhenUserExitsOrganizationOtherMemberBecomesFounder()
    {
        $client = self::createClient();
        list($user, $token) = $this->createUserAndLogIn($client, 'cheeseplease3@example.com', 'fooBar12345');
        $project = $this->getEntityManager()->getRepository(Project::class)->findOneBy([]);
        $organizationUuid = Uuid::uuid();
        $client->request('POST', '/organizations', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [
                'id' => $organizationUuid,
                'name' => 'Penguins',
                'project' => '/projects/' . $project->getId(),
                'members' => [
                    '/users/' . $user->getId(),
                ]
            ]
        ]);
        $this->assertResponseIsSuccessful();

        list($user2, $token2) = $this->createUserAndLogIn($client, 'cheeseplease6@example.com', 'fooBar12345');

        $invitationId = Uuid::uuid();
        $client->request('POST', '/organization_unconfirmed_members/invite', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [
                'id' => $invitationId,
                'organization' => '/organizations/' . $organizationUuid,
                'member' => '/users/' . $user2->getId()
            ]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'status' => OrganizationUnconfirmedMembers::STATUS_INVITED,
        ]);

        /** @var Organization $organization */
        $organization = $this->getEntityManager()->getRepository(Organization::class)->find($organizationUuid);
        $organization->setIsLocked(true);
        $this->getEntityManager()->flush();

        $client->request('PUT', "/organization_unconfirmed_members/$invitationId/accept-invitation", [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token2
            ],
            'json' => [

            ]
        ]);
        $this->assertResponseStatusCodeSame(400);
        $badinvitationId = Uuid::uuid();
        $client->request('POST', '/organization_unconfirmed_members/invite', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [
                'id' => $badinvitationId,
                'organization' => '/organizations/' . $organizationUuid,
                'member' => '/users/' . $user2->getId()
            ]
        ]);
        $this->assertResponseStatusCodeSame(400);
        $this->assertNull($this->getEntityManager()->getRepository(OrganizationUnconfirmedMembers::class)
            ->find($badinvitationId));


        $client->request('PUT', "/organizations/$organizationUuid/exit", [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [

            ]
        ]);
        $this->assertResponseStatusCodeSame(400);

        /** @var Organization $organization */
        $organization = $this->getEntityManager()->getRepository(Organization::class)->find($organizationUuid);
        $organization->setIsLocked(false);
        $this->getEntityManager()->flush();

        $client->request('PUT', "/organization_unconfirmed_members/$invitationId/accept-invitation", [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token2
            ],
            'json' => [

            ]
        ]);
        $this->assertResponseStatusCodeSame(200);

        $client->request('PUT', "/organizations/$organizationUuid/exit", [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [

            ]
        ]);
        $this->assertResponseStatusCodeSame(200);

        $client->request('GET', "/organizations/$organizationUuid",[
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
        ]);
        $this->assertResponseStatusCodeSame(200);

        $this->assertJsonContains([
            'name' => 'Penguins',
            'founder'=>'/users/'.$user2->getId()
        ]);
    }

    public function testOwnerExitTeamWithNoMembersAndDeletesOrganizationOnExit()
    {
        $client = self::createClient();
        list($user, $token) = $this->createUserAndLogIn($client, 'cheeseplease3@example.com', 'fooBar12345');
        $project = $this->getEntityManager()->getRepository(Project::class)->findOneBy([]);
        $organizationUuid = Uuid::uuid();
        $client->request('POST', '/organizations', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [
                'id' => $organizationUuid,
                'name' => 'Penguins',
                'project' => '/projects/' . $project->getId(),
                'members' => [
                    '/users/' . $user->getId(),
                ]
            ]
        ]);
        $this->assertResponseIsSuccessful();


        $client->request('PUT', "/organizations/$organizationUuid/exit", [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [

            ]
        ]);
        $this->assertResponseStatusCodeSame(200);
        $client->request('GET', "/organizations/$organizationUuid",[
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
        ]);
        $this->assertResponseStatusCodeSame(404);
    }

    public function testOwnerExitTeamWithMembersAndAnotherMemberWillBecomeOwner()
    {
        $client = self::createClient();
        list($user, $token) = $this->createUserAndLogIn($client, 'cheeseplease3@example.com', 'fooBar12345');
        $project = $this->getEntityManager()->getRepository(Project::class)->findOneBy([]);
        $organizationUuid = Uuid::uuid();
        $client->request('POST', '/organizations', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [
                'id' => $organizationUuid,
                'name' => 'Penguins',
                'project' => '/projects/' . $project->getId(),
                'members' => [
                    '/users/' . $user->getId(),
                ]
            ]
        ]);
        $this->assertResponseIsSuccessful();
        // add second user to organization
        list($user2, $token2) = $this->createUserAndLogIn($client, 'cheeseplease2@example.com', 'fooBar12345');
        $user2 = $this->getEntityManager()->getRepository(User::class)->find($user2->getId());
        $this->getEntityManager()->getRepository(Organization::class)->find($organizationUuid)->addMember($user2);
        $this->getEntityManager()->flush();


        $client->request('PUT', "/organizations/$organizationUuid/exit", [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [

            ]
        ]);
        $this->assertResponseStatusCodeSame(200);
        $client->request('GET', "/organizations/$organizationUuid",[
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
        ]);
        $this->assertResponseStatusCodeSame(200);
        $this->assertJsonContains([
            'name' => 'Penguins',
            'founder'=>'/users/'.$user2->getId()
        ]);
    }

    public function testUserCanNotUnlockTheTeam()
    {
        $client = self::createClient();
        list($user, $token) = $this->createUserAndLogIn($client, 'cheeseplease3@example.com', 'fooBar12345');
        $project = $this->getEntityManager()->getRepository(Project::class)->findOneBy([]);
        $organizationUuid = Uuid::uuid();
        $client->request('POST', '/organizations', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [
                'id' => $organizationUuid,
                'name' => 'Penguins2',
                'project' => '/projects/' . $project->getId(),
                'members' => [
                    '/users/' . $user->getId(),
                ]
            ]
        ]);
        $this->assertResponseIsSuccessful();

        $client->request('PUT', "/organizations/$organizationUuid/lock", [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [
            ]
        ]);
        $this->assertResponseIsSuccessful();

        $client->request('PUT', "/organizations/$organizationUuid", [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [
                "locked"=> false
            ]
        ]);

        $this->assertResponseStatusCodeSame(200);

        $client->request('GET', "/organizations/$organizationUuid", [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ]
        ]);
        $this->assertJsonContains([
            "locked"=> true
        ]);
    }

    public function testCreateOrganizationAsAdminImitation()
    {
        $client = self::createClient();
        list($user, $token) = $this->createAdminAndLogIn($client, 'admin@example.com', 'fooBar12345');
        $em = $this->getEntityManager();
        /** @var User $user2 */
        list($user2, $token2) = $this->createUserAndLogIn($client, 'sdfsafwehyk@example.com', 'fooBar12345');
        $project = $em->getRepository(Project::class)->findOneBy([]);
        $client->request('POST', '/organizations?imitate='.$user2->getEmail(), [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [
                'id' => Uuid::uuid(),
                'name' => 'Penguins',
                'members' => [
                    '/users/' . $user->getId(),
                    '/users/' . $user2->getId(),
                ],
                'project' => '/projects/' . $project->getId(),
            ]
        ]);
        $this->assertResponseIsSuccessful();

        $this->assertJsonContains([
            'name' => 'Penguins',
            'founder' => '/users/' . $user2->getId()
        ]);

        $client->request('POST', '/organizations', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token,
                'imitate' => $user2->getEmail()
            ],
            'json' => [
                'id' => Uuid::uuid(),
                'name' => 'Penguins2',
                'members' => [
                    '/users/' . $user->getId(),
                    '/users/' . $user2->getId(),
                ],
                'project' => '/projects/' . $project->getId(),
            ]
        ]);
        $this->assertResponseStatusCodeSame(400);

        $client->request('GET', '/organizations', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token,
                'imitate' => $user2->getEmail()
            ]
        ]);
        $this->assertResponseIsSuccessful();
    }


    public function testCancelInvitationOnOrgCreation()
    {
        $client = self::createClient();
        list($user, $token) = $this->createUserAndLogIn($client, 'cheeseplease33@example.com', 'fooBar12345');
        list($user2, $token2) = $this->createUserAndLogIn($client, 'cheeseplease233@example.com', 'fooBar12345');
        $project = $this->getEntityManager()->getRepository(Project::class)->findOneBy([]);
        $organizationUuid = Uuid::uuid();
        $client->request('POST', '/organizations', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [
                'id' => $organizationUuid,
                'name' => 'Penguins',
                'members' => [
                    '/users/' . $user->getId(),
                ],
                'project' => '/projects/' . $project->getId(),
            ]
        ]);
        $this->assertResponseIsSuccessful();
        $uuid = Uuid::uuid();
        $client->request('POST', '/organization_unconfirmed_members/join', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token2
            ],
            'json' => [
                'id' => $uuid,
                'organization' => '/organizations/' . $organizationUuid,
                'member' => '/users/' . $user2->getId(),
                'status' => OrganizationUnconfirmedMembers::STATUS_INVITED,
            ]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'status' => OrganizationUnconfirmedMembers::STATUS_WANTED_TO_JOIN,
        ]);
        $this->assertEquals(
            OrganizationUnconfirmedMembers::STATUS_WANTED_TO_JOIN,
            $this->getEntityManager()->getRepository(OrganizationUnconfirmedMembers::class)->find($uuid)->getStatus()
        );

        $client->request('POST', '/organizations', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token2
            ],
            'json' => [
                'id' => Uuid::uuid(),
                'name' => 'Penguins another',
                'members' => [
                    '/users/' . $user2->getId(),
                ],
                'project' => '/projects/' . $project->getId(),
            ]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertEquals(
            OrganizationUnconfirmedMembers::STATUS_REJECTED,
            $this->getEntityManager()->getRepository(OrganizationUnconfirmedMembers::class)->find($uuid)->getStatus()
        );
    }

    public function testMakeInvitationsInvalid()
    {
        $client = self::createClient();
        list($user, $token) = $this->createUserAndLogIn($client, 'cheesepleas5e3@example.com', 'fooBar12345');
        $project = $this->getEntityManager()->getRepository(Project::class)->findOneBy([]);
        $organizationUuid = Uuid::uuid();
        $client->request('POST', '/organizations', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [
                'id' => $organizationUuid,
                'name' => 'Penguins',
                'project' => '/projects/' . $project->getId(),
            ]
        ]);
        $this->assertResponseIsSuccessful();

        list($invitedUser, $invitedToken) = $this->createUserAndLogIn($client, 'cheeseplease36@example.com', 'fooBar12345');
        list($user3, $token3) = $this->createUserAndLogIn($client, 'cheeseplease336@example.com', 'fooBar123345');
        list($user4, $token4) = $this->createUserAndLogIn($client, 'cheesep2lease336@example.com', 'f2ooBar123345');
        $organizationUuid2 = Uuid::uuid();
        $client->request('POST', '/organizations', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token3
            ],
            'json' => [
                'id' => $organizationUuid2,
                'name' => 'Penguins',
                'project' => '/projects/' . $project->getId(),
            ]
        ]);
        $this->assertResponseIsSuccessful();

        $invitationId = Uuid::uuid();
        $client->request('POST', '/organization_unconfirmed_members/invite', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [
                'id' => $invitationId,
                'organization' => '/organizations/' . $organizationUuid,
                'member' => '/users/' . $invitedUser->getId()
            ]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'status' => OrganizationUnconfirmedMembers::STATUS_INVITED,
        ]);

        $invitationId2 = Uuid::uuid();
        $client->request('POST', '/organization_unconfirmed_members/invite', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token3
            ],
            'json' => [
                'id' => $invitationId2,
                'organization' => '/organizations/' . $organizationUuid2,
                'member' => '/users/' . $invitedUser->getId()
            ]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'status' => OrganizationUnconfirmedMembers::STATUS_INVITED,
        ]);

        $client->request('PUT', "/organization_unconfirmed_members/$invitationId/accept-invitation", [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $invitedToken
            ],
            'json' => [

            ]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertSame(
            $this->getEntityManager()
            ->getRepository(OrganizationUnconfirmedMembers::class)->find($invitationId2)->getStatus(),
        OrganizationUnconfirmedMembers::STATUS_REJECTED);

        //invited user can not create org for current project
        $organizationUuid3 = Uuid::uuid();
        $client->request('POST', '/organizations', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $invitedToken
            ],
            'json' => [
                'id' => $organizationUuid3,
                'name' => 'Penguins',
                'project' => '/projects/' . $project->getId(),
            ]
        ]);
        $this->assertResponseStatusCodeSame(400);

        //he already has team so he can not create new org for current project
        $client->request('POST', '/organizations', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [
                'id' => $organizationUuid3,
                'name' => 'Penguins',
                'project' => '/projects/' . $project->getId(),
            ]
        ]);
        $this->assertResponseStatusCodeSame(400);

        // validation for checking if user already participate in proj
        $client->request('POST', '/organization_unconfirmed_members/invite', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token3
            ],
            'json' => [
                'id' =>  Uuid::uuid(),
                'organization' => '/organizations/' . $organizationUuid2,
                'member' => '/users/' . $invitedUser->getId()
            ]
        ]);
        $this->assertResponseStatusCodeSame(400);
        $this->assertJsonContains([
            'hydra:description' => 'You are already member of organization for this project'
        ]);
    }
}
