<?php


namespace App\Tests\Functional;


use App\Entity\AddressRegion;
use App\Entity\Person;
use App\Entity\Project;
use App\Entity\User;
use App\Test\CustomApiTestCase;
use Faker\Provider\Uuid;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;

class PersonResourceTest extends CustomApiTestCase
{
    use ReloadDatabaseTrait;

    public function testCreatePersonWithAddressLocality()
    {
        $client = self::createClient();
        list($user, $token)  = $this->createUserAndLogIn($client, 'cheeseplease@example.com', 'fooBar123',false);
        /** @var AddressRegion $address */
        $address = $this->getEntityManager()->getRepository(AddressRegion::class)->findOneBy([]);

        $client->request('POST', '/people', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer '.$token
            ],
            'json' => [
                'id' => Uuid::uuid(),
                'addressRegion' => '/address_regions/'.$address->getId(),
            ]
        ]);
        $this->assertResponseIsSuccessful();

        $this->assertJsonContains([
            'addressRegion' => '/address_regions/'.$address->getId(),
            'user'=>'/users/'.$user->getId()
        ]);
    }


    public function testCreatePersonWithWorksFor()
    {
        $client = self::createClient();
        list($user, $token)  = $this->createUserAndLogIn($client, 'cheeseplease@example.com', 'fooBar1234',false);

        $client->request('POST', '/people', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer '.$token
            ],
            'json' => [
                'id' => Uuid::uuid(),
                'worksFor' => 'Microsoft',
            ]
        ]);
        $this->assertResponseIsSuccessful();

        $this->assertJsonContains([
            'worksFor' => 'Microsoft',
            'user'=>'/users/'.$user->getId()
        ]);
    }


    public function testUserCanChangePerson()
    {
        $client = self::createClient();
        list($user, $token)  = $this->createUserAndLogIn($client, 'cheeseplease@example.com', 'foo',false);
        /** @var AddressRegion $address */
        $address = $this->getEntityManager()->getRepository(AddressRegion::class)->findOneBy([]);

        $id = Uuid::uuid();
        $client->request('POST', '/people', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer '.$token
            ],
            'json' => [
                'id' => $id,
                'addressRegion' => '/address_regions/'.$address->getId(),
            ]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'addressRegion' => '/address_regions/'.$address->getId(),
            'user'=>'/users/'.$user->getId()
        ]);
        /** @var AddressRegion $address */
        $address2 = $this->getEntityManager()->getRepository(AddressRegion::class)->findOneBy([]);

        $client->request('PATCH', '/people/'.$id, [
            'headers' => [
                'Content-Type' => 'application/merge-patch+json',
                'Authorization' => 'Bearer '.$token
            ],
            'json' => [
                'addressRegion' => '/address_regions/'.$address2->getId(),
            ]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'addressRegion' => '/address_regions/'.$address2->getId(),
            'user'=>'/users/'.$user->getId()
        ]);
    }

    public function testUserCanNotChangeID()
    {
        $client = self::createClient();
        list($user, $token)  = $this->createUserAndLogIn($client, 'cheeseplease@example.com', 'FooBar1234',false);

        /** @var AddressRegion $address */
        $address = $this->getEntityManager()->getRepository(AddressRegion::class)->findOneBy([]);
        $originalId = Uuid::uuid();
        $client->request('POST', '/people', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer '.$token
            ],
            'json' => [
                'id' => $originalId,
                'addressRegion' => '/address_regions/'.$address->getId(),
            ]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'addressRegion' => '/address_regions/'.$address->getId(),
            'user'=>'/users/'.$user->getId()
        ]);

        $newId = "2d47bee4-a7fb-4764-999b-43298dd13e28";
        $client->request('PATCH', '/people/'.$originalId, [
            'headers' => [
                'Content-Type' => 'application/merge-patch+json',
                'Authorization' => 'Bearer '.$token
            ],
            'json' => [
                "id"=> $newId
            ]
        ]);
        $this->assertResponseIsSuccessful();

        $client->request('GET', '/people/'.$newId, [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer '.$token
            ]
        ]);
        $this->assertResponseStatusCodeSame(404);
    }

    public function testUserIsNotEnabled()
    {
        $client = static::createClient();
        $em = $this->getEntityManager();
        $id = Uuid::uuid();

        $client->request('POST', 'users', [
            'headers' => ['Content-Type' => 'application/ld+json'],
            'json' => [
                'id' => $id,
                'email' => 'cheeseplease@example.com',
                'username' => 'cheeseplease',
                'password' => 'brie1Aaaaa',
                'role' => User::ROLE_TEACHER
            ]
        ]);

        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->enableUser($id);
        $token = $this->logIn($client, 'cheeseplease@example.com', 'brie1Aaaaa');
        /** @var AddressRegion $address */
        $address = $this->getEntityManager()->getRepository(AddressRegion::class)->findOneBy([]);
        $people = $this->getEntityManager()->getRepository(Person::class)->findOneBy(['user'=>$id]);

        $client->request('PATCH', '/people/'.$people->getId(), [
            'headers' => [
                'Content-Type' => 'application/merge-patch+json',
                'Authorization' => 'Bearer '.$token
            ],
            'json' => [
                'id' => Uuid::uuid(),
                'addressRegion' => '/address_regions/'.$address->getId(),
            ]
        ]);
        $this->assertResponseIsSuccessful();
    }

    public function testCreatePersonWithBlankWorksFor()
    {
        $client = self::createClient();
        list($user, $token)  = $this->createUserAndLogIn($client, 'cheeseplease@example.com', 'foo',false);
        /** @var AddressRegion $address */
        $address = $this->getEntityManager()->getRepository(AddressRegion::class)->findOneBy([]);
        $client->request('POST', '/people', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer '.$token
            ],
            'json' => [
                'id' => Uuid::uuid(),
                'addressRegion' => '/address_regions/'.$address->getId(),
                'worksFor' => '',
            ]
        ]);
        $this->assertResponseIsSuccessful();
    }

    public function testFailCreatePersonWithInvalidWorksFor()
    {
        $client = self::createClient();
        list($user, $token)  = $this->createUserAndLogIn($client, 'cheeseplease@example.com', 'foo');

        // Min length
        $client->request('POST', '/people', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer '.$token
            ],
            'json' => [
                'id' => Uuid::uuid(),
                'worksFor' => 'M',
            ]
        ]);
        $this->assertResponseStatusCodeSame(400);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');

        // Max length
        $client->request('POST', '/people', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer '.$token
            ],
            'json' => [
                'id' => Uuid::uuid(),
                'worksFor' => 'QZpnHiPxc4CaY8RDHyZSY3ZOrGay6MFcQF9xAiT714pAgpZSpKVfUxFFpHTXGX46nqOjv3Gk3voqoE2YpdnctcZKjticv2LSybu2aPYI2M80rkzKyXyOZrASHz5gvFLHvFC87L2ZY1JcxIlocVCbU9jULqrB1wondEJt2Ny8j3AjdKSxaHDKZsd9zbcZFPKkJuH2wLsgs',
            ]
        ]);
        $this->assertResponseStatusCodeSame(400);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
    }

    public function testCreatePersonWithBlankAddressRegion()
    {
        $client = self::createClient();
        list($user, $token)  = $this->createUserAndLogIn($client, 'cheeseplease@example.com', 'foo');

        $client->request('POST', '/people', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer '.$token
            ],
            'json' => [
                'id' => Uuid::uuid(),
                'addressRegion' => '',
                'worksFor' => 'SDN 1 Sumber',
            ]
        ]);
        $this->assertResponseStatusCodeSame(400);
    }

    public function testFailCreatePersonWithInvalidAddressLocality()
    {
        $client = self::createClient();
        list($user, $token)  = $this->createUserAndLogIn($client, 'cheeseplease@example.com', 'foo');

        // Min length
        $client->request('POST', '/people', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer '.$token
            ],
            'json' => [
                'id' => Uuid::uuid(),
                'addressRegion' => '/address_regions/0491e9a0-b0d0-4258-9815-68fe011892ea',
            ]
        ]);
        $this->assertResponseStatusCodeSame(400);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');

        // Max length
        $client->request('POST', '/people', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer '.$token
            ],
            'json' => [
                'id' => Uuid::uuid(),
                'addressRegion' => 'QZpnHiPxc4CaY8RDHyZSY3ZOrGay6MFcQF9xAiT714pAgpZSpKVfUxFFpHTXGX46nqOjv3Gk3voqoE2YpdnctcZKjticv2LSybu2aPYI2M80rkzKyXyOZrASHz5gvFLHvFC87L2ZY1JcxIlocVCbU9jULqrB1wondEJt2Ny8j3AjdKSxaHDKZsd9zbcZFPKkJuH2wLsgs',
            ]
        ]);
        $this->assertResponseStatusCodeSame(400);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
    }
}
