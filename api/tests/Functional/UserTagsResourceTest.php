<?php


namespace App\Tests\Functional;


use App\Entity\User;
use App\Test\CustomApiTestCase;
use Faker\Provider\Uuid;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;

class UserTagsResourceTest extends CustomApiTestCase
{
    use ReloadDatabaseTrait;

    public function testCreateUserTagsByAdmin()
    {
        $client = self::createClient();
        list($user, $token)  = $this->createAdminAndLogIn($client, 'admin@example.com', 'admin');

        $uuid = Uuid::uuid();
        $client->request('POST', 'tags', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer '.$token
            ],
            'json' => [
                'id' => $uuid,
                'tag' => 'UX/UI',
                'name' => "test",
                'value' => 'test',
                'role' => User::ROLE_DESIGNER,
            ]
        ]);
        $this->assertResponseIsSuccessful();

        $this->assertJsonContains([
            'role' => User::ROLE_DESIGNER,
            'tag' => 'UX/UI',
            'id' => $uuid,
        ]);
    }

    public function testCreateUserTagsByUser()
    {
        $client = self::createClient();
        list($user, $token)  = $this->createUserAndLogIn($client, 'user@example.com', 'user');

        $client->request('POST', 'tags', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer '.$token
            ],
            'json' => [
                'id' => Uuid::uuid(),
                'tag' => 'UX/UI',
                'role' => User::ROLE_DESIGNER,
            ]
        ]);
        $this->assertResponseStatusCodeSame(403);
    }

    public function testGetAllUserTagsAndSortForDesigner()
    {
        $client = self::createClient();
        list($user, $token)  = $this->createUserAndLogIn($client, 'user@example.com', 'user');

        $client->request('GET', 'tags', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer '.$token
            ]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            "hydra:totalItems"=> 11
        ]);

        $client->request('GET', 'tags?role='.User::ROLE_DESIGNER, [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer '.$token
            ]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            "hydra:totalItems"=> 2
        ]);
    }
}
