<?php


namespace App\Tests\Functional;

use ApiPlatform\Core\Api\OperationType;
use App\Entity\AnimationObject;
use App\Entity\ArchiveObject;
use App\Entity\ImageObject;
use App\Entity\Project;
use App\Test\CustomApiTestCase;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;

class MediaObjectsTest extends CustomApiTestCase
{
    use ReloadDatabaseTrait;

    public function testGetAwsLink()
    {
        $client = self::createClient();
        list($user, $token) = $this->createUserAndLogIn($client, 'cheesepleasemedia@example.com', 'fooBar12345');
        $client->request('POST', '/archive_objects/give-me-put-link', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [
                'fileExtension' =>'Penguins',
            ]
        ]);
        $this->assertResponseStatusCodeSame(201);
        //in fronted we check only violations not errors code!
        $this->assertJsonContains([
            'hydra:title' => 'An error occurred',
        ]);
        $client->request('POST', '/archive_objects/give-me-put-link', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [
                'fileExtension' =>'zip',
            ]
        ]);
        $this->assertResponseStatusCodeSame(200);
        $this->assertMatchesResourceItemJsonSchema( ArchiveObject::class, OperationType::ITEM);
    }

    public function testSavingAwsLinkToAllMediaObjects()
    {
        $client = self::createClient();
        list($user, $token) = $this->createUserAndLogIn($client, 'cheesepleasemedia2@example.com', 'fooBar12345');
        $client->request('POST', '/archive_objects', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [
                'url' =>"https://xpics.s3.amazonaws.com/c20b77c1-665e-4f1f-ac37-bda2972a835d.zip?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=not-a-real-key%2F20201002%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20201002T132220Z&X-Amz-SignedHeaders=host&X-Amz-Expires=1800&X-Amz-Signature=22459a633110c5823dea78ad9fe1aee08cae1795e069b3ed3dc31b120335a685",
                'name' => "Penguins"
            ]
        ]);
        $this->assertResponseStatusCodeSame(201);
        $this->assertMatchesResourceItemJsonSchema( ArchiveObject::class, OperationType::ITEM);
        $this->assertJsonContains([
            'name' => "Penguins"
        ]);

        $client->request('POST', '/image_objects', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [
                'url' =>"https://xpics.s3.amazonaws.com/c20b77c1-665e-4f1f-ac37-bda2972a835d.zip?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=not-a-real-key%2F20201002%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20201002T132220Z&X-Amz-SignedHeaders=host&X-Amz-Expires=1800&X-Amz-Signature=22459a633110c5823dea78ad9fe1aee08cae1795e069b3ed3dc31b120335a685",
                'name' => "Penguins.jpg"
            ]
        ]);
        $this->assertResponseStatusCodeSame(201);
        $this->assertMatchesResourceCollectionJsonSchema( ImageObject::class);
        $this->assertJsonContains([
            'name' => "Penguins.jpg"
        ]);

        $client->request('POST', '/video_objects', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [
                'url' =>"https://xpics.s3.amazonaws.com/c20b77c1-665e-4f1f-ac37-bda2972a835d.zip?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=not-a-real-key%2F20201002%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20201002T132220Z&X-Amz-SignedHeaders=host&X-Amz-Expires=1800&X-Amz-Signature=22459a633110c5823dea78ad9fe1aee08cae1795e069b3ed3dc31b120335a685",
                'name' => "Penguins"
            ]
        ]);
        $this->assertResponseStatusCodeSame(201);
        $this->assertJsonContains([
            'name' => "Penguins"
        ]);

        $client->request('POST', '/document_objects', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [
                'url' =>"https://xpics.s3.amazonaws.com/c20b77c1-665e-4f1f-ac37-bda2972a835d.zip?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=not-a-real-key%2F20201002%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20201002T132220Z&X-Amz-SignedHeaders=host&X-Amz-Expires=1800&X-Amz-Signature=22459a633110c5823dea78ad9fe1aee08cae1795e069b3ed3dc31b120335a685",
                'name' => "Penguins"
            ]
        ]);
        $this->assertResponseStatusCodeSame(201);
        $this->assertJsonContains([
            'name' => "Penguins"
        ]);
    }


    public function testSaveVideoObject()
    {
        $client = self::createClient();
        list($user, $token) = $this->createUserAndLogIn($client, 'cheesepleasemedia2@example.com', 'fooBar12345');

        $client->request('POST', '/animation_objects', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [
                'url' =>"https://xpics.s3.amazonaws.com/c20b77c1-665e-4f1f-ac37-bda2972a835d.gif?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=not-a-real-key%2F20201002%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20201002T132220Z&X-Amz-SignedHeaders=host&X-Amz-Expires=1800&X-Amz-Signature=22459a633110c5823dea78ad9fe1aee08cae1795e069b3ed3dc31b120335a685",
                'name' => "Penguins.gif"
            ]
        ]);
        $this->assertResponseStatusCodeSame(201);

        $em = $this->getEntityManager();
        $animation = $em->getRepository(AnimationObject::class)->findOneBy([]);
        $image = $em->getRepository(ImageObject::class)->findOneBy([]);

        $client->request('POST', '/video_objects', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [
                'url' =>"https://xpics.s3.amazonaws.com/c20b77c1-665e-4f1f-ac37-bda2972a835d.avi?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=not-a-real-key%2F20201002%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20201002T132220Z&X-Amz-SignedHeaders=host&X-Amz-Expires=1800&X-Amz-Signature=22459a633110c5823dea78ad9fe1aee08cae1795e069b3ed3dc31b120335a685",
                'name' => "Penguins.avi",
                'image' => "/image_objects/". $image->getId(),
                'animation' => "/animation_objects/". $animation->getId()
            ]
        ]);
        $this->assertResponseStatusCodeSame(201);
        $this->assertJsonContains([
            'name' => "Penguins.avi",
            'image' => "/image_objects/". $image->getId(),
            'animation' => "/animation_objects/". $animation->getId()
        ]);

        $videoObjectId = $client->getResponse()->toArray()['@id'];

        $client->request('POST', '/animation_objects', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [
                'url' =>"https://xpics.s3.amazonaws.com/c20b77c1-665e-4f1f-ac37-bda2972a835d.gif?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=not-a-real-key%2F20201002%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20201002T132220Z&X-Amz-SignedHeaders=host&X-Amz-Expires=1800&X-Amz-Signature=22459a633110c5823dea78ad9fe1aee08cae1795e069b3ed3dc31b120335a685",
                'name' => "Penguins3.gif"
            ]
        ]);
        $this->assertResponseIsSuccessful(201);
        $anotherAnimationId = $client->getResponse()->toArray()['@id'];

        $client->request('PATCH', $videoObjectId, [
            'headers' => [
                'Content-Type' => 'application/merge-patch+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [
                'animation' => $anotherAnimationId
            ]
        ]);
        $this->assertResponseStatusCodeSame(200);

        $this->assertJsonContains([
            'name' => "Penguins.avi",
            'image' => "/image_objects/". $image->getId(),
            'animation' => $anotherAnimationId
        ]);
    }

    public function testMediaObjectTranslations()
    {
        $client = self::createClient();
        list($user, $token) = $this->createUserAndLogIn($client, 'cheesepleasemedia2@example.com', 'fooBar12345');

        $client->request('POST', '/animation_objects', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token,
                'X-LOCALE' => 'en'
            ],
            'json' => [
                'url' =>"https://xpics.s3.amazonaws.com/c20b77c1-665e-4f1f-ac37-bda2972a835d.gif?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=not-a-real-key%2F20201002%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20201002T132220Z&X-Amz-SignedHeaders=host&X-Amz-Expires=1800&X-Amz-Signature=22459a633110c5823dea78ad9fe1aee08cae1795e069b3ed3dc31b120335a685",
                'name' => "PenguinsEn.gif"
            ]
        ]);
        $this->assertResponseIsSuccessful(201);
        $this->assertJsonContains([
            'name' => "PenguinsEn.gif",
        ]);
        $anotherAnimationId = $client->getResponse()->toArray()['@id'];

        $client->request('PATCH', $anotherAnimationId, [
            'headers' => [
                'Content-Type' => 'application/merge-patch+json',
                'Authorization' => 'Bearer ' . $token,
            ],
            'json' => [
                'name' => "PenguinsId.gif",
                'url' =>"https://xpics.s3.amazonaws.com/blabla.gif?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=not-a-real-key%2F20201002%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20201002T132220Z&X-Amz-SignedHeaders=host&X-Amz-Expires=1800&X-Amz-Signature=22459a633110c5823dea78ad9fe1aee08cae1795e069b3ed3dc31b120335a685",
            ]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'name' => "PenguinsId.gif",
        ]);

        $client->request('GET', $anotherAnimationId, [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token,
                'X-LOCALE' => 'id'
            ]
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'name' => "PenguinsId.gif",
        ]);

        $client->request('GET', $anotherAnimationId, [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token,
                'X-LOCALE' => 'en'
            ]
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'name' => "PenguinsEn.gif",
        ]);
    }
}
