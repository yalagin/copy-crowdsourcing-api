<?php


namespace App\Tests\Functional;


use App\Entity\User;
use App\Test\CustomApiTestCase;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;

class CustomActionsTest extends CustomApiTestCase
{
    use ReloadDatabaseTrait;

    public function testSendApplicationLinkToEmail(){
        $client = self::createClient();
        /** @var User $user */
        list($user, $token) = $this->createUserAndLogIn($client, 'cheesepleasemedia@example.com', 'fooBar12345');
        $client->request('POST', '/user/send-mobile-application-link-to-email', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
//                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [
                'email' =>$user->getEmail(),
            ]
        ]);
        $this->assertResponseIsSuccessful();

        $client->request('POST', '/user/send-activation-link-to-email', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
//                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [
                'email' =>$user->getEmail(),
            ]
        ]);
        $this->assertResponseIsSuccessful();
        $client->request('POST', '/user/send-token-to-email', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
//                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [
                'email' =>$user->getEmail(),
            ]
        ]);
        $this->assertResponseIsSuccessful();
    }
}
