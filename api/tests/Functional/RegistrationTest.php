<?php


namespace App\Tests\Functional;


use App\Entity\User;
use App\Entity\Tags;
use App\Exception\UnauthorizedRoleException;
use App\Test\CustomApiTestCase;
use Faker\Provider\Uuid;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class RegistrationTest extends CustomApiTestCase
{
    use ReloadDatabaseTrait;


    /**
     * @throws UnauthorizedRoleException
     */
    public function testRegisterUserWithRoleTeacher()
    {
        $client = static::createClient();
        $em = $this->getEntityManager();
        $id = Uuid::uuid();

        $client->request('POST', 'users', [
            'headers' => ['Content-Type' => 'application/ld+json'],
            'json' => [
                'id' => $id,
                'email' => 'cheeseplease@example.com',
                'username' => 'cheeseplease',
                'password' => 'brie1Aaaaa',
                'role' => User::ROLE_TEACHER
            ]
        ]);

        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        /** @var User $user */
        $user = $em->getRepository(User::class)->find($id);
        $this->assertEquals([User::ROLE_TEACHER,User::ROLE_USER], $user->getRoles());
        $this->enableUser($id);

        $token = $this->logIn($client, 'cheeseplease@example.com', 'brie1Aaaaa');
        /** @var Tags $tag */

        $tag = $em
            ->getRepository(Tags::class)->findOneBy(['role'=>User::ROLE_TEACHER]);

        //check if client can use it's id and add tags
        $client->request('PATCH', '/users/'.$id, [
            'headers' => [
                'Content-Type' => 'application/merge-patch+json',
                'Authorization' => 'Bearer '.$token
            ],
            'json' => [
                "tags"=> [
                    'tags/'.$tag->getId()
                ]
            ]
        ]);
        $this->assertResponseIsSuccessful();
        $user = $em->getRepository(User::class)->find($id);
        $this->assertEquals($tag, $user->getTags()->first());


        $tag = $em
            ->getRepository(Tags::class)->findOneBy(['role'=>User::ROLE_DESIGNER]);
        $client->request('PATCH', '/users/' . $id, [
            'headers' => [
                'Content-Type' => 'application/merge-patch+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [
                "tags" => [
                    'tags/' . $tag->getId()
                ]
            ]
        ]);

        $this->assertResponseStatusCodeSame(400);
    }

    public function testAddTeacherRoleToUser()
    {
        $client = static::createClient();
        list($user, $token)  = $this->createUserAndLogIn($client, 'cheeseplease@example.com', 'foo');
        $em = $this->getEntityManager();

        /** @var User $user */
        $user = $em->getRepository(User::class)->find($user->getId());
        $this->assertEquals([User::ROLE_USER], $user->getRoles());


        $client->request('PATCH', '/users/'.$user->getId(), [
            'headers' => [
                'Content-Type' => 'application/merge-patch+json',
                'Authorization' => 'Bearer '.$token
            ],
            'json' => [
                'role' => User::ROLE_TEACHER
            ]
        ]);
        $this->assertResponseIsSuccessful();
        /** @var User $user */
        $user = $em->getRepository(User::class)->find($user->getId());
        $this->assertEquals([User::ROLE_TEACHER,User::ROLE_USER], $user->getRoles());


        /** @var Tags $tag */
        $tag = $this->getEntityManager()
            ->getRepository(Tags::class)->findOneBy(['role'=>User::ROLE_TEACHER]);
        $client->request('PATCH', '/users/'.$user->getId(), [
            'headers' => [
                'Content-Type' => 'application/merge-patch+json',
                'Authorization' => 'Bearer '.$token
            ],
            'json' => [
                "tags"=> [
                    'tags/'.$tag->getId()
                ]
            ]
        ]);
        $this->assertResponseIsSuccessful();
    }

    /**
     * @throws UnauthorizedRoleException
     */
    public function testRegisterUserWithRoleDesigner()
    {
        $client = static::createClient();
        $em = $this->getEntityManager();
        $id = Uuid::uuid();

        $client->request('POST', 'users', [
            'headers' => ['Content-Type' => 'application/ld+json'],
            'json' => [
                'id' => $id,
                'email' => 'cheeseplease@example.com',
                'username' => 'cheeseplease',
                'password' => 'brie1Aaaaa',
                'role' => User::ROLE_DESIGNER
            ]
        ]);

        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        /** @var User $user */
        $user = $em->getRepository(User::class)->find($id);
        $this->assertEquals([User::ROLE_DESIGNER,User::ROLE_USER], $user->getRoles());
        $this->enableUser($id);

        $token = $this->logIn($client, 'cheeseplease@example.com', 'brie1Aaaaa');
        /** @var Tags $tag */

        $tag = $em
            ->getRepository(Tags::class)->findOneBy(['role'=>User::ROLE_DESIGNER]);

        //check if client can use it's id and add tags
        $client->request('PATCH', '/users/'.$id, [
            'headers' => [
                'Content-Type' => 'application/merge-patch+json',
                'Authorization' => 'Bearer '.$token
            ],
            'json' => [
                "tags"=> [
                    'tags/'.$tag->getId()
                ]
            ]
        ]);
        $this->assertResponseIsSuccessful();


        $tag = $em
            ->getRepository(Tags::class)->findOneBy(['role'=>User::ROLE_TEACHER]);
        $client->request('PATCH', '/users/' . $id, [
            'headers' => [
                'Content-Type' => 'application/merge-patch+json',
                'Authorization' => 'Bearer ' . $token
            ],
            'json' => [
                "tags" => [
                    'tags/' . $tag->getId()
                ]
            ]
        ]);

        $this->assertResponseStatusCodeSame(400);
    }

    public function testAddTeacherWithTagsInOneRequest()
    {
        $client = static::createClient();
        $em = $this->getEntityManager();
        $id = Uuid::uuid();
        $tag = $em
            ->getRepository(Tags::class)->findOneBy(['role'=>User::ROLE_TEACHER]);

        $client->request('POST', 'users', [
            'headers' => ['Content-Type' => 'application/ld+json'],
            'json' => [
                'id' => $id,
                'email' => 'cheeseplease@example.com',
                'username' => 'cheeseplease',
                'password' => 'brie1Aaaaa',
                'role' => User::ROLE_TEACHER,
                "tags"=> [
                    'tags/'.$tag->getId()
                ]
            ]
        ]);

        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        /** @var User $user */
        $user = $em->getRepository(User::class)->find($id);
        $this->assertEquals([User::ROLE_TEACHER,User::ROLE_USER], $user->getRoles());
        $this->assertEquals($tag->getId(), $user->getTags()->first()->getId());
    }

    public function testFailAddTeacherWithTagsInOneRequestWithWrongGroup()
    {
        $client = static::createClient();
        $em = $this->getEntityManager();
        $id = Uuid::uuid();
        $tag = $em
            ->getRepository(Tags::class)->findOneBy(['role'=>User::ROLE_DESIGNER]);

        $client->request('POST', 'users', [
            'headers' => ['Content-Type' => 'application/ld+json'],
            'json' => [
                'id' => $id,
                'email' => 'cheeseplease@example.com',
                'username' => 'cheeseplease',
                'password' => 'brie1Aaaaa',
                'role' => User::ROLE_TEACHER,
                "tags"=> [
                    'tags/'.$tag->getId()
                ]
            ]
        ]);

        $this->assertResponseStatusCodeSame(400);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        /** @var User $user */
        $user = $em->getRepository(User::class)->find($id);
        $this->assertEquals(null, $user);
    }


    public function testFailAddTeacherWithTagsInOneRequestWithOutRole()
    {
        $client = static::createClient();
        $em = $this->getEntityManager();
        $id = Uuid::uuid();
        $tag = $em
            ->getRepository(Tags::class)->findOneBy(['role'=>User::ROLE_DESIGNER]);

        $client->request('POST', 'users', [
            'headers' => ['Content-Type' => 'application/ld+json'],
            'json' => [
                'id' => $id,
                'email' => 'cheeseplease@example.com',
                'username' => 'cheeseplease',
                'password' => 'brie1Aaaaa',
                "tags"=> [
                    'tags/'.$tag->getId()
                ]
            ]
        ]);

        $this->assertResponseStatusCodeSame(400);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        /** @var User $user */
        $user = $em->getRepository(User::class)->find($id);
        $this->assertEquals(null, $user);
    }

    public function testActivateUserSuccessfully()
    {
        $client = static::createClient();
        $em = $this->getEntityManager();
        $id = Uuid::uuid();

        $client->request('POST', 'users', [
            'headers' => ['Content-Type' => 'application/ld+json'],
            'json' => [
                'id' => $id,
                'email' => 'cheeseplease@example.com',
                'username' => 'cheeseplease',
                'password' => 'brie1Aaaaa',
            ]
        ]);

        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        /** @var User $user */
        $user = $em->getRepository(User::class)->find($id);
        $this->assertEquals(false, $user->getEnabled());
        $confirmationToken = $user->getConfirmationToken();
        $client->request('GET', '/confirm-user/'.$confirmationToken );
        $this->assertResponseStatusCodeSame(302);
        $user = $em->getRepository(User::class)->find($id);
        $this->assertEquals(true, $user->getEnabled());
    }
    public function testActivateUserSuccessfullyViaAPIEndpoint()
    {
        $client = static::createClient();
        $em = $this->getEntityManager();
        $id = Uuid::uuid();

        $client->request('POST', 'users', [
            'headers' => ['Content-Type' => 'application/ld+json'],
            'json' => [
                'id' => $id,
                'email' => 'cheeseplease@example.com',
                'username' => 'cheeseplease',
                'password' => 'brie1Aaaaa',
            ]
        ]);

        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        /** @var User $user */
        $user = $em->getRepository(User::class)->find($id);
        $this->assertEquals(false, $user->getEnabled());

        $confirmationToken = $user->getConfirmationToken();
        $client->request('POST', 'users/confirm', [
            'headers' => ['Content-Type' => 'application/ld+json'],
            'json' => [
                'confirmationToken' => $confirmationToken,
            ]
        ]);
        $this->assertResponseIsSuccessful();
        $user = $em->getRepository(User::class)->find($id);
        $this->assertEquals(true, $user->getEnabled());
    }

    public function testFailRegisterUserWithInvalidEmail()
    {
        $client = static::createClient();

        $client->request('POST', 'users', [
            'headers' => ['Content-Type' => 'application/ld+json'],
            'json' => [
                'id' => '4ae959fa-da18-4dc1-a5ac-9690a1afe30a',
                'email' => 'cheeseplease$example.com',
                'username' => 'cheeseplease',
                'password' => 'brie1Aaaaa'
            ]
        ]);

        $this->assertResponseStatusCodeSame(400);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
    }

    public function testFailRegisterUserWithBadPassword()
    {
        $client = static::createClient();

        $client->request('POST', 'users', [
            'headers' => ['Content-Type' => 'application/ld+json'],
            'json' => [
                'id' => '4ae959fa-da18-4dc1-a5ac-9690a1afe30a',
                'email' => 'cheeseplease@example.com',
                'username' => 'cheeseplease',
                'password' => '123456'
            ]
        ]);

        $this->assertResponseStatusCodeSame(400);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
    }

    public function testFailRegisterUserWithInvalidRole()
    {
        $client = static::createClient();

        $client->request('POST', 'users', [
            'headers' => ['Content-Type' => 'application/ld+json'],
            'json' => [
                'id' => '4ae959fa-da18-4dc1-a5ac-9690a1afe30a',
                'email' => 'cheeseplease@example.com',
                'username' => 'cheeseplease',
                'password' => 'brie1Aaaaa',
                'role' => 'ROLE_INVALID'
            ]
        ]);

        $this->assertResponseStatusCodeSame(400);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
    }

    public function testUserCanNotLoginWhenNotEnabled()
    {
        $client = static::createClient();
        $em = $this->getEntityManager();
        $id = Uuid::uuid();

        $client->request('POST', 'users', [
            'headers' => ['Content-Type' => 'application/ld+json'],
            'json' => [
                'id' => $id,
                'email' => 'cheeseplease@example.com',
                'username' => 'cheeseplease',
                'password' => 'brie1Aaaaa',
                'role' => User::ROLE_TEACHER
            ]
        ]);

        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $client->request('POST', '/authentication_token', [
            'headers' => ['Content-Type' => 'application/ld+json'],
            'json' => [
                'email' => "cheeseplease@example.com",
                'password' => "brie1Aaaaa"
            ],
        ]);
        $this->assertResponseStatusCodeSame(401);
    }
}
