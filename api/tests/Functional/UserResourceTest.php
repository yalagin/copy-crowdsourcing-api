<?php

namespace App\Tests\Functional;

use App\Entity\Person;
use App\Entity\User;
use App\Test\CustomApiTestCase;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;

class UserResourceTest extends CustomApiTestCase
{
    use ReloadDatabaseTrait;

    public function testCreateUser()
    {
        $client = static::createClient();

        $client->request('POST', 'users', [
            'headers' => ['Content-Type' => 'application/ld+json'],
            'json' => [
                'id' => '4ae959fa-da18-4dc1-a5ac-9690a1afe30a',
                'email' => 'cheeseplease@example.com',
                'username' => 'cheeseplease',
                'password' => 'brie1Aaaaa'
            ]
        ]);


        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');

        $this->logIn($client, 'cheeseplease@example.com', 'brie1Aaaaa',false);
        $this->assertResponseStatusCodeSame(401);
    }

    public function testUserTriesBecomingAnAdmin()
    {
        $client = self::createClient();
        list($user, $token)  = $this->createUserAndLogIn($client, 'cheeseplease@example.com', 'foo');

        $client->request('PATCH', '/users/'.$user->getId(), [
            'headers' => [
                'Content-Type' => 'application/merge-patch+json',
                'Authorization' => 'Bearer '.$token
            ],
            'json' => [
                'username' => 'newusername',
                'roles' => ['ROLE_ADMIN'], // will be ignored
                'role' => 'ROLE_ADMIN' // will be ignored
            ]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'username' => 'newusername'
        ]);

        $em = $this->getEntityManager();
        /** @var User $user */
        $user = $em->getRepository(User::class)->find($user->getId());
        $this->assertEquals(['ROLE_USER'], $user->getRoles());
    }

    public function testGetUser()
    {
        $client = self::createClient();
        $user = $this->createUser('cheeseplease@example.com', 'foo');
        list($signedInUser, $token)= $this->createUserAndLogIn($client, 'authenticated@example.com', 'foo');

        $client->request('GET', '/users/'.$user->getId(),[
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer '.$token
            ],
        ]);
        $this->assertJsonContains([
            'username' => 'cheeseplease'
        ]);

        $data = $client->getResponse()->toArray();
        $this->assertArrayNotHasKey('password', $data);
    }


    public function testChangeUserRole()
    {
        $client = self::createClient();
        list($user, $token)  = $this->createUserAndLogIn($client, 'cheeseplease@example.com', 'foo');

        $client->request('PATCH', '/users/'.$user->getId(), [
            'headers' => [
                'Content-Type' => 'application/merge-patch+json',
                'Authorization' => 'Bearer '.$token
            ],
            'json' => [
                'role' => User::ROLE_TEACHER // will not be ignored!
            ]
        ]);
        $this->assertResponseIsSuccessful();

        $em = $this->getEntityManager();
        /** @var User $user */
        $user = $em->getRepository(User::class)->find($user->getId());
        $this->assertEquals([User::ROLE_TEACHER,User::ROLE_USER], $user->getRoles());


        //user tring to change role second time
        $client->request('PATCH', '/users/'.$user->getId(), [
            'headers' => [
                'Content-Type' => 'application/merge-patch+json',
                'Authorization' => 'Bearer '.$token
            ],
            'json' => [
                'role' => User::ROLE_DESIGNER // will be ignored!
            ]
        ]);

        $this->assertResponseIsSuccessful();

        /** @var User $user */
        $user = $em->getRepository(User::class)->find($user->getId());
        $this->assertEquals([User::ROLE_TEACHER,User::ROLE_USER], $user->getRoles());
    }



    public function testChangingUserNameProperties()
    {
        $client = static::createClient();


        $id = '4ae959fa-da18-4dc1-a5ac-9690a1afe30a';
        $email = 'cheeseplease@example.com';
        $userName = 'cheeseplease';
        $password = 'Bribe2020forewer1234test';
        $client->request('POST', 'users', [
            'headers' => ['Content-Type' => 'application/ld+json'],
            'json' => [
                'id' => '' . $id,
                'email' => '' . $email,
                'username' => '' . $userName,
                'password' => '' . $password
            ]
        ]);


        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        /** @var User $user */
        $user = $this->enableUser($id);

        $token = $this->logIn($client, $email, $password);

        //user tring to change role second time
        $client->request('PATCH', '/users/'.$id, [
            'headers' => [
                'Content-Type' => 'application/merge-patch+json',
                'Authorization' => 'Bearer '.$token
            ],
            'json' => [
                'username'=> 'tes' // will not be ignored!
            ]
        ]);

        $em = $this->getEntityManager();
        /** @var User $user */
        $user = $em->getRepository(User::class)->find($id);
        $this->assertNotEquals($userName, $user->getUsername());
        $this->assertEquals('tes', $user->getUsername());
    }

    public function testTokenExpiration()
    {
        $client = self::createClient();
        list($user, $token)  = $this->createUserAndLogIn($client, 'cheeseplease@example.com', 'FooBar123');

        $client->request('PUT', '/users/'.$user->getId().'/reset-password', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer '.$token
            ],
            'json' => [
                'oldPassword' => "FooBar123",
                'newRetypedPassword' => "FooBar1234",
                'newPassword' => "FooBar1234",
            ]
        ]);
        $this->assertResponseIsSuccessful();
//        sleep(1)
        //imitate time passing 2 sec I don't want to make test slow by sleep(1);
        $em = $this->getEntityManager();
        /** @var User $user */
        $user = $em->getRepository(User::class)->find($user->getId());
        $user->setPasswordChangeDate($user->getPasswordChangeDate()+2);
        $em->flush();
        $client->request('GET', '/users/'.$user->getId(), [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer '.$token
            ]
        ]);
        $this->assertResponseStatusCodeSame(401);


        $client->request('POST', 'authentication_token', [
            'headers' => ['Content-Type' => 'application/ld+json'],
            'json' => [
                'email' => 'cheeseplease@example.com',
                'password' => "FooBar1234",
            ]
        ]);
        $this->assertResponseIsSuccessful();

        $client->request('POST', 'authentication_token', [
            'headers' => ['Content-Type' => 'application/ld+json'],
            'json' => [
                'email' => 'cheeseplease@example.com',
                'password' => "FooBar123",
            ]
        ]);
        $this->assertResponseStatusCodeSame(401);
    }

    public function testResetPasswordViaToken()
    {
        $client = static::createClient();
        $em = $this->getEntityManager();
        $email = "ilovevobla@gmail.com";
        $this->createUser($email,'VoblaForeverWIthM3');
        $user = $em->getRepository(User::class)->findOneBy(['email'=>$email]);
        $this->assertEmpty( $user->getPasswordResetToken());

        $client->request('POST', 'user/send-token-to-email', [
            'headers' => ['Content-Type' => 'application/ld+json'],
            'json' => [
                'email' => $email,
            ]
        ]);
        $this->assertResponseIsSuccessful();
        $user = $em->getRepository(User::class)->findOneBy(['email'=>$email]);
        $this->assertNotEmpty($user->getPasswordResetToken());


        $client->request('PUT', 'users/'.$user->getId().'/reset-password-with-token', [
            'headers' => ['Content-Type' => 'application/ld+json'],
            'json' => [
                'token' => $user->getPasswordResetToken(),
                'newPassword' => "Idis1ikeVobla",
                'newRetypedPassword' => "Idis1ikeVobla"
            ]
        ]);

        $this->assertResponseIsSuccessful();

        $client->request('POST', 'authentication_token', [
            'headers' => ['Content-Type' => 'application/ld+json'],
            'json' => [
                'email' => $email,
                'password' => "Idis1ikeVobla",
            ]
        ]);
        $this->assertResponseIsSuccessful();
    }

    //user know admin id and email but don't have password and access to email account
    public function testUserTryingToGetAdminPassword()
    {
        $client = static::createClient();
        $em = $this->getEntityManager();
        $email = 'admin@example.com';
        list($user, $token)  = $this->createAdminAndLogIn($client, $email, 'Admin12345');

        $user = $em->getRepository(User::class)->findOneBy(['email'=>$email]);


        $client->request('PUT', 'users/'.$user->getId().'/reset-password-with-token', [
            'headers' => ['Content-Type' => 'application/ld+json'],
            'json' => [
                'token' => "I want to hack admin",
                'newPassword' => "Idis1ikeVobla",
                'newRetypedPassword' => "Idis1ikeVobla"
            ]
        ]);

        $this->assertResponseStatusCodeSame(400);

        $client->request('PUT', 'users/'.$user->getId().'/reset-password-with-token', [
            'headers' => ['Content-Type' => 'application/ld+json'],
            'json' => [
                'newPassword' => "Idis1ikeVobla",
                'newRetypedPassword' => "Idis1ikeVobla"
            ]
        ]);

        $this->assertResponseStatusCodeSame(401);

        $client->request('POST', 'authentication_token', [
            'headers' => ['Content-Type' => 'application/ld+json'],
            'json' => [
                'email' => $email,
                'password' => "Idis1ikeVobla",
            ]
        ]);

        $this->assertResponseStatusCodeSame(401);
    }

    public function testUserWithProfile(){
        $client = static::createClient();
        $email = "i124lov4evobla@gmail.com";
        $client->request('POST', 'users', [
            'headers' => ['Content-Type' => 'application/ld+json'],
            'json' => [
                'id' => '4ae959fa-da18-4dc1-a5ac-9690a1afe30a',
                'email' => $email,
                'username' => 'cheeseplease',
                'password' => 'brie1Aaaaa'
            ]
        ]);
        $this->assertResponseStatusCodeSame(201);

        $em = $this->getEntityManager();
        $user = $em->getRepository(User::class)->findOneBy(['email'=>$email]);
        $this->assertNotEmpty($user);
        $person = $em->getRepository(Person::class)->findOneBy(['user'=>$user->getId()]);
        $this->assertNotEmpty($person->getImage());
    }

}
