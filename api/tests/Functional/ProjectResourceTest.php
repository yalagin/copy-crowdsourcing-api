<?php


namespace App\Tests\Functional;


use App\Entity\Award;
use App\Entity\EducationalLevel;
use App\Entity\Project;
use App\Entity\Tags;
use App\Entity\User;
use App\Test\CustomApiTestCase;
use DateTime;
use Faker\Provider\Uuid;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;

class ProjectResourceTest extends CustomApiTestCase
{
    use ReloadDatabaseTrait;

    public function testUnauthenticatedUserCanGetListOfProjects()
    {
        $client = self::createClient();

        $client->request('GET', '/projects?theme=Arithmetic%20with%20Whole%20Numbers%20%7C%20Rounding%20Off%20and%20Approximation', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
            ]
        ]);
        $this->assertResponseIsSuccessful();
    }

    public function testUserTriesToCreateAndModifyProject()
    {
        $client = self::createClient();
        list($user, $token)  = $this->createUserAndLogIn($client, 'cheeseplease@example.com', 'fooBar12345');

        $em = $this->getEntityManager();
        $tag = $em->getRepository(Tags::class)->findOneBy(['role'=>User::ROLE_TEACHER]);
        $educationalLevel = $em->getRepository(EducationalLevel::class)->findOneBy([]);


        $endDate = (string)date('Y-m-d H:i:s', time() + 604800);
        $uuid = Uuid::uuid();
        $startDate = (string)date('Y-m-d H:i:s');
        $client->request('POST', '/projects', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer '.$token
            ],
            'json' => [
                'id' => $uuid,
                "title"=> "Arithmetic with blank Numbers | Rounding Off and Approximation",
                "objective"=> "Demonstrate rounding and approximation from addition, subtraction, multiplication and division of two whole numbers and/or fractions",
                "activity"=> "Learning about rounding whole numbers and solve mathematical questions",
                "assessment"=> "Understand rounding and approximation from addition, subtraction, multiplication and division of two whole numbers and/or fractions",
                "tags"=> [
                    'tags/'.$tag->getId()
                ],
                "educationalLevel"=> 'educational_levels/'.$educationalLevel->getId(),
                "startTime"=> $startDate,
                "endTime"=> $endDate,
            ]
        ]);
        $this->assertResponseStatusCodeSame(403);

        $project = $em->getRepository(Project::class)->findOneBy([]);
        $client->request('PATCH', '/projects/'.$project->getId(), [
            'headers' => [
                'Content-Type' => 'application/merge-patch+json',
                'Authorization' => 'Bearer '.$token
            ],
            'json' => [
                "theme"=> "Arithmetic with me!",
                "endTime"=> $startDate,
            ]
        ]);
        $this->assertResponseStatusCodeSame(403);
    }

    public function testAdminTriesToCreateAndModifyProject()
    {
        $client = self::createClient();
        list($user, $token)  = $this->createAdminAndLogIn($client, 'cheesepleasefromadmin@example.com', 'Admin123testing');

        $em = $this->getEntityManager();
        $tag = $em->getRepository(Tags::class)->findOneBy(['role'=>User::ROLE_TEACHER]);
        $educationalLevel = $em->getRepository(EducationalLevel::class)->findOneBy([]);
        $award = $em->getRepository(Award::class)->findOneBy([]);


        $endDate = (string)date('Y-m-d H:i:s', time() + 604800);
        $uuid = Uuid::uuid();
        $startDate = (string)date('Y-m-d H:i:s');
        $client->request('POST', '/projects', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer '.$token
            ],
            'json' => [
                'id' => $uuid,
                "title"=> "Arithmetic with blank Numbers | Rounding Off and Approximation",
                "objective"=> "Demonstrate rounding and approximation from addition, subtraction, multiplication and division of two whole numbers and/or fractions",
                "activity"=> "Learning about rounding whole numbers and solve mathematical questions",
                "assessment"=> "Understand rounding and approximation from addition, subtraction, multiplication and division of two whole numbers and/or fractions",
                "tags"=> [
                    'tags/'.$tag->getId()
                ],
                "awards" => [
                    '/awards/'.$award->getId()
                ],
                "educationalLevel"=> 'educational_levels/'.$educationalLevel->getId(),
                "startTime"=> $startDate,
                "endTime"=> $endDate,
            ]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            "objective"=> "Demonstrate rounding and approximation from addition, subtraction, multiplication and division of two whole numbers and/or fractions",
            "awards" => [
                '/awards/'.$award->getId()
            ],
        ]);

        $client->request('PATCH', '/projects/'.$uuid, [
            'headers' => [
                'Content-Type' => 'application/merge-patch+json',
                'Authorization' => 'Bearer '.$token
            ],
            'json' => [
                "objective"=> "Arithmetic with me!",
                "endTime"=> $startDate,
                "awards" => [
                    '/awards/'.$award->getId()
                ],
            ]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            "id"=> $uuid,
            "objective"=> "Arithmetic with me!",
            "awards" => [
                 '/awards/'.$award->getId()
            ],
        ]);
    }
}
