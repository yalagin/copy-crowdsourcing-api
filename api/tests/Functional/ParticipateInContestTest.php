<?php


namespace App\Tests\Functional;


use App\Entity\CreativeWork;
use App\Entity\ImageObject;
use App\Entity\Lesson;
use App\Entity\LessonAnswer;
use App\Entity\LessonQuestion;
use App\Entity\Organization;
use App\Entity\Project;
use App\Entity\Review;
use App\Entity\Tags;
use App\Entity\User;
use App\Test\CustomApiTestCase;
use Doctrine\Common\Collections\ArrayCollection;
use Faker\Provider\Uuid;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;

class ParticipateInContestTest extends CustomApiTestCase
{
    use ReloadDatabaseTrait;

    public function testCreateCreativeWork()
    {
        $client = self::createClient();
        list($user, $founderToken) = $this
            ->createUserAndLogIn($client, 'useremail@example.com', 'fooBar12312');
        $em = $this->getEntityManager();
        /** @var User $user2 */
        list($user2, $memberToken) = $this
            ->createUserAndLogIn($client, 'cheesepleaseanotheroneemail@example.com', 'fooBar12312');
        list($userNotInOrganization, $tokenNotInOrganization) =
            $this->createUserAndLogIn($client, 'cheeseplease3@example.com', 'fooBar12312');
        $project = $em->getRepository(Project::class)->findOneBy([]);

        $organizationsUuid = Uuid::uuid();
        $client->request('POST', '/organizations', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $founderToken
            ],
            'json' => [
                'id' => $organizationsUuid,
                'name' => 'Penguins',
                'project' => '/projects/' . $project->getId(),
            ]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'name' => 'Penguins',
            'founder'=>'/users/'.$user->getId()
        ]);
        // add second user to organization
        $user2 = $this->getEntityManager()->getRepository(User::class)->find($user2->getId());
        $this->getEntityManager()->getRepository(Organization::class)
            ->find($organizationsUuid)->addMember($user2)->setIsLocked(true);
        $this->getEntityManager()->flush();


        $client->request('POST', '/video_objects', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $founderToken
            ],
            'json' => [
                'url' =>"https://xpics.s3.amazonaws.com/c20b77c1-665e-4f1f-ac37-bda2972a835d.avi?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=not-a-real-key%2F20201002%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20201002T132220Z&X-Amz-SignedHeaders=host&X-Amz-Expires=1800&X-Amz-Signature=22459a633110c5823dea78ad9fe1aee08cae1795e069b3ed3dc31b120335a685",
                'name' => "Penguins"
            ]
        ]);
        $this->assertResponseStatusCodeSame(201);
        $vidoeId =$client->getResponse()->toArray()['@id'];


        $creativeWorkUuid = Uuid::uuid();
        $client->request('POST', '/creative_works', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $founderToken
            ],
            'json' => [
                'id' => $creativeWorkUuid,
                'creator' => '/organizations/' . $organizationsUuid,
                'createdFor' => '/projects/' . $project->getId(),
                'video'=> $vidoeId
            ]
        ]);
        $this->assertResponseIsSuccessful();

        $this->assertJsonContains([
            'id' => $creativeWorkUuid,
            'creator' => '/organizations/' . $organizationsUuid,
            'createdFor' => '/projects/' . $project->getId(),
            'video'=> $vidoeId
        ]);

        //test that we can upload the acrchive to server and connect it to creative work
        $client->request('POST', '/archive_objects', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $founderToken
            ],
            'json' => [
                'url' =>"https://xpics.s3.amazonaws.com/c20b77c1-665e-4f1f-ac37-bda2972a835d.zip?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=not-a-real-key%2F20201002%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20201002T132220Z&X-Amz-SignedHeaders=host&X-Amz-Expires=1800&X-Amz-Signature=22459a633110c5823dea78ad9fe1aee08cae1795e069b3ed3dc31b120335a685",
                'name' => "Penguins2"
            ]
        ]);
        $this->assertResponseStatusCodeSame(201);
        $achiveId =$client->getResponse()->toArray()['@id'];

        $imageObject = $this->getEntityManager()->getRepository(ImageObject::class)->findOneBy([]);

        $client->request('PATCH', '/creative_works/'.$creativeWorkUuid, [
            'headers' => [
                'Content-Type' => 'application/merge-patch+json',
                'Authorization' => 'Bearer ' . $founderToken
            ],
            'json' => [
                'archive'=> $achiveId,
                'images' => [
                    '/image_objects/'.$imageObject->getId()
                ]
            ]
        ]);

        $this->assertResponseIsSuccessful();

        $this->assertJsonContains([
            'id' => $creativeWorkUuid,
            'creator' => '/organizations/' . $organizationsUuid,
            'createdFor' => '/projects/' . $project->getId(),
            'video'=> $vidoeId,
            'archive'=> $achiveId,
            'images' => [
                '/image_objects/'.$imageObject->getId()
            ]
        ]);

        $client->request('POST', '/creative_works', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $tokenNotInOrganization
            ],
            'json' => [
                'id' => Uuid::uuid(),
                'creator' => '/organizations/' . $organizationsUuid,
                'createdFor' => '/projects/' . $project->getId(),
            ]
        ]);
        $this->assertResponseStatusCodeSame(400);

        $questionsUuid = Uuid::uuid();
        $client->request('POST', '/questions', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $founderToken
            ],
            'json' => [
                'id' => $questionsUuid,
                'creator' => '/organizations/' . $organizationsUuid,
                'isPartOf'=> '/creative_works/'.$creativeWorkUuid,
                'description' => "Does earth flat?",
                'type'=>'TYPE_MULTIPLE_CHOICE',
                'answerKey' =>"its round like a ball",
                'visualBrief' =>"a round earth, like a pancake"
            ]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'id' => $questionsUuid,
            'creator' => '/organizations/' . $organizationsUuid,
            'isPartOf'=> '/creative_works/'.$creativeWorkUuid,
            'description' => "Does earth flat?",
            'type'=>'TYPE_MULTIPLE_CHOICE',
            'answerKey' =>"its round like a ball",
            'visualBrief' =>"a round earth, like a pancake"
        ]);


        $client->request('POST', '/questions', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $tokenNotInOrganization
            ],
            'json' => [
                'id' => Uuid::uuid(),
                'creator' => '/organizations/' . $organizationsUuid,
                'isPartOf'=> '/creative_works/'.$creativeWorkUuid,
                'description' => "Does earth flat?"
            ]
        ]);
        $this->assertResponseStatusCodeSame(400);

        $client->request('POST', '/answers', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $founderToken
            ],
            'json' => [
                'id' => Uuid::uuid(),
                'creator' => '/organizations/' . $organizationsUuid,
                'parentItem'=> '/questions/'.$questionsUuid,
                'description' => "True"
            ]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'creator' => '/organizations/' . $organizationsUuid,
            'parentItem'=> '/questions/'.$questionsUuid,
            'description' => "True",
            'correct'=>false
        ]);

        $client->request('POST', '/answers', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $founderToken
            ],
            'json' => [
                'id' => Uuid::uuid(),
                'creator' => '/organizations/' . $organizationsUuid,
                'parentItem'=> '/questions/'.$questionsUuid,
                'description' => "False",
                'correct'=>true
            ]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'creator' => '/organizations/' . $organizationsUuid,
            'parentItem'=> '/questions/'.$questionsUuid,
            'description' => "False",
            'correct'=>true
        ]);

        $answerUuid = Uuid::uuid();
        $client->request('POST', '/answers', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $memberToken
            ],
            'json' => [
                'id' => $answerUuid,
                'creator' => '/organizations/' . $organizationsUuid,
                'parentItem'=> '/questions/'.$questionsUuid,
                'description' => "I dont know",
            ]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'creator' => '/organizations/' . $organizationsUuid,
            'parentItem'=> '/questions/'.$questionsUuid,
            'description' => "I dont know",
            'correct'=>false
        ]);

        $client->request('PATCH', '/answers/'.$answerUuid, [
            'headers' => [
                'Content-Type' => 'application/merge-patch+json',
                'Authorization' => 'Bearer ' . $founderToken
            ],
            'json' => [
                'description' => "I dont know2",
            ]
        ]);
        $this->assertResponseIsSuccessful();

        $client->request('PATCH', '/answers/'.$answerUuid, [
            'headers' => [
                'Content-Type' => 'application/merge-patch+json',
                'Authorization' => 'Bearer ' . $tokenNotInOrganization
            ],
            'json' => [
                'description' => "I dont know3",
            ]
        ]);
        $this->assertResponseStatusCodeSame(403);


        $client->request('POST', '/answers', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $tokenNotInOrganization
            ],
            'json' => [
                'id' => Uuid::uuid(),
                'creator' => '/organizations/' . $organizationsUuid,
                'parentItem'=> '/questions/'.$questionsUuid,
                'description' => "I dont know2",
            ]
        ]);
        $this->assertResponseStatusCodeSame(400);

        $client->request('PUT', '/creative_works/'.$creativeWorkUuid.'/submit', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $tokenNotInOrganization
            ]
        ]);
        $this->assertResponseStatusCodeSame(403);

        $client->request('PUT', '/creative_works/'.$creativeWorkUuid.'/submit', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $memberToken
            ]
        ]);
        $this->assertResponseStatusCodeSame(403);

        $client->request('PUT', '/creative_works/'.$creativeWorkUuid.'/submit', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $founderToken
            ],'json' => [

            ]
        ]);
        $this->assertResponseStatusCodeSame(400);
        $this->assertJsonContains([
            'hydra:description'=>"Minimal jumlah pertanyaan adalah 15"
        ]);

        //post another question without correct answer
        $mathQuestionUuid = Uuid::uuid();
        $client->request('POST', '/questions', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $founderToken
            ],
            'json' => [
                'id' => $mathQuestionUuid,
                'creator' => '/organizations/' . $organizationsUuid,
                'isPartOf'=> '/creative_works/'.$creativeWorkUuid,
                'description' => "2+2",
                'type' => "test"
            ]
        ]);
        $this->assertResponseStatusCodeSame(201);
        $client->request('PUT', '/creative_works/'.$creativeWorkUuid.'/submit', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $founderToken
            ],'json' => [

            ]
        ]);
        $this->assertResponseStatusCodeSame(400);
        $this->assertJsonContains([
            'hydra:description'=>"Minimal jumlah pertanyaan adalah 15"
        ]);
        $project = $em->getRepository(Project::class)->find($project->getId());

        /** @var Project $project */
        $project->setMinimumNumberOfQuestions(1);
        $em->flush();
        $client->request('PUT', '/creative_works/'.$creativeWorkUuid.'/submit', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $founderToken
            ],'json' => [

            ]
        ]);
        $this->assertResponseStatusCodeSame(400);
        $this->assertJsonContains([
            'hydra:description'=>'2+2 pertanyaan harus memiliki setidaknya satu jawaban yang benar'
        ]);

        $client->request('POST', '/answers', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $founderToken
            ],
            'json' => [
                'id' => Uuid::uuid(),
                'creator' => '/organizations/' . $organizationsUuid,
                'parentItem'=> '/questions/'.$mathQuestionUuid,
                'description' => "5"
            ]
        ]);
        $this->assertJsonContains([
            'correct'=> false
        ]);
        $client->request('PUT', '/creative_works/'.$creativeWorkUuid.'/submit', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $founderToken
            ],'json' => [

            ]
        ]);
        $this->assertResponseStatusCodeSame(400);
        $this->assertJsonContains([
            'hydra:description'=>'2+2 pertanyaan harus memiliki setidaknya satu jawaban yang benar'
        ]);


        $client->request('POST', '/answers', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $founderToken
            ],
            'json' => [
                'id' => Uuid::uuid(),
                'creator' => '/organizations/' . $organizationsUuid,
                'parentItem'=> '/questions/'.$mathQuestionUuid,
                'description' => "4",
                'correct'=> true
            ]
        ]);
        $this->assertJsonContains([
            'correct'=> true
        ]);
        $client->request('PUT', '/creative_works/'.$creativeWorkUuid.'/submit', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $founderToken
            ],'json' => [

            ]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'isSubmitted'=> true
        ]);

        /** @var ArrayCollection $review */
        $reviews= $em->getRepository(Review::class)->findBy(['organization'=> $organizationsUuid]);
        $this->assertSame(2,count($reviews));
        $review= $em->getRepository(Review::class)->findBy([
            'organization'=> $organizationsUuid,
            'author' => $user2->getId()
        ]);
        $this->assertSame(1,count($review));
        $reviewId =$review[0]->getId();
        $tag = $em
            ->getRepository(Tags::class)->findOneBy(['role'=>Tags::REVIEW_TAG]);

        $client->request('PUT', '/reviews/'.$reviewId, [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $memberToken
            ],'json' => [
                "reviewBody"=> "string",
                "reviewRating"=> 5,
                "tags"=> [
                    "/tags/".$tag->getId()
                ]
            ]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'isSubmitted'=> true
        ]);

        $this->creativeWorkId= $creativeWorkUuid;
    }

    public function  testImportCreativeWorkToLesson()
    {
        $client = self::createClient();

        /** @var CreativeWork $creativeWork */
        $creativeWork = $this->getEntityManager()->getRepository(CreativeWork::class)->findOneBy([]);
        $this->assertNotSame(null , $creativeWork);

        $connection = $this->getEntityManager()->getConnection();
        $dbPlatform = $connection->getDatabasePlatform();
        $q = $dbPlatform->getTruncateTableSql('lesson',true);
        $q2 = $dbPlatform->getTruncateTableSql('lesson_answer',true);
        $q3 = $dbPlatform->getTruncateTableSql('lesson_question',true);
        $connection->executeUpdate($q2);
        $connection->executeUpdate($q3);
        $connection->executeUpdate($q);

        $lesson = $this->getEntityManager()->getRepository(Lesson::class)->findOneBy([]);
        $question = $this->getEntityManager()->getRepository(LessonQuestion::class)->findOneBy([]);
        $answer = $this->getEntityManager()->getRepository(LessonAnswer::class)->findOneBy([]);
        $this->assertSame(false , $lesson && $question && $answer);

        list($user, $token) = $this
            ->createUserAndLogIn($client, 'cheeseAnother@example.com', 'fooBar12312');

        $client->request('PUT', '/creative_works/'.$creativeWork->getId().'/import-to-lesson', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $token
            ],'json' => [

            ]
        ]);
        $this->assertResponseStatusCodeSame(403);

        $lesson = $this->getEntityManager()->getRepository(Lesson::class)->findOneBy([]);
        $question = $this->getEntityManager()->getRepository(LessonQuestion::class)->findOneBy([]);
        $answer = $this->getEntityManager()->getRepository(LessonAnswer::class)->findOneBy([]);
        $this->assertSame(false , $lesson && $question && $answer);

        list($user, $adminToken) = $this
            ->createAdminAndLogIn($client, 'cheeseAdmin@example.com', 'fooBar12312');

        $client->request('PUT', '/creative_works/'.$creativeWork->getId().'/import-to-lesson', [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $adminToken
            ],'json' => [

            ]
        ]);
        $this->assertResponseIsSuccessful();

        $lesson = $this->getEntityManager()->getRepository(Lesson::class)->findOneBy([]);
        $question = $this->getEntityManager()->getRepository(LessonQuestion::class)->findOneBy([]);
        $answer = $this->getEntityManager()->getRepository(LessonAnswer::class)->findOneBy([]);
        $this->assertNotSame(null , $lesson);
        $this->assertNotSame(null , $question);
        $this->assertNotSame(null , $answer);

        $client->request('PATCH', '/lessons/'.$lesson->getId(), [
            'headers' => [
                'Content-Type' => 'application/merge-patch+json',
                'Authorization' => 'Bearer ' . $adminToken
            ],'json' => [
                'transcript' => 'транскрипция '
            ]
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'transcript'=> 'транскрипция '
        ]);

        $client->request('PATCH', '/lessons/'.$lesson->getId(), [
            'headers' => [
                'Content-Type' => 'application/merge-patch+json',
                'Authorization' => 'Bearer ' . $adminToken,
                'X-LOCALE' => 'en'
            ],'json' => [
                'transcript' => 'transcript my transcript'
            ]
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'transcript'=> 'transcript my transcript'
        ]);

        $client->request('GET', '/lessons/'.$lesson->getId(), [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $adminToken
            ],'json' => [

            ]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'transcript'=> 'транскрипция '
        ]);

        $client->request('GET', '/lessons/'.$lesson->getId(), [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $adminToken,
                'X-LOCALE' => 'en'
            ],'json' => [

            ]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'transcript'=> 'transcript my transcript'
        ]);
        $client->request('GET', '/lessons/'.$lesson->getId(), [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $adminToken,
                'X-LOCALE' => 'id'
            ],'json' => [

            ]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'transcript'=> 'транскрипция '
        ]);
        $client->request('GET', '/lessons/'.$lesson->getId(), [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $adminToken,
                'X-LOCALE' => 'ru'
            ],'json' => [

            ]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'transcript'=> 'транскрипция '
        ]);
        $client->request('GET', '/lessons/'.$lesson->getId(), [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Authorization' => 'Bearer ' . $adminToken,
                'X-LOCALE' => 'th'
            ],'json' => [

            ]
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'transcript'=> 'транскрипция '
        ]);
    }
}
