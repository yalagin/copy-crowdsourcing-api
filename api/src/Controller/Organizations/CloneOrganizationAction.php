<?php


namespace App\Controller\Organizations;


use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Entity\CreativeWork;
use App\Entity\Organization;
use App\Entity\OrganizationUnconfirmedMembers;
use App\Entity\Project;
use App\Entity\User;
use App\Exception\UnauthorizedUserException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Uid\Uuid;

class CloneOrganizationAction
{
    public function __invoke(Organization $data,EntityManagerInterface $entityManager, ValidatorInterface $validator,TokenStorageInterface $tokenStorage)
    {
        $validator->validate($data);
        /** @var Project $project */
        $project = $entityManager->getRepository(Project::class)->find($data->getCloneForProject());
        if(!$project){
            throw new NotFoundHttpException();
        }
        $token = $tokenStorage->getToken();

        if (null === $token) {
            throw new UnauthorizedUserException();
        }

        /** @var UserInterface $user */
        $user = $token->getUser();

        if (!$user instanceof User) {
            throw new UnauthorizedUserException();
        }

        $newOrganization = New Organization();
        $newOrganization->setId($data->getNewOrganizationId());
        $newOrganization->setFounder($data->getFounder());
        $newOrganization->addMember($data->getFounder());
        $newOrganization->setName($data->getName());
        $newOrganization->setImage($data->getImage());
        $newOrganization->setIsOpenForJoin($data->isOpenForJoin());
        $newOrganization->setCloneForProject($data->getCloneForProject());
        $newOrganization->setNewOrganizationId($data->getNewOrganizationId());
        $newOrganization->setProject($project);

        $entityManager->persist($newOrganization);

        $members = $data->getMembers();
        foreach($members as $member){
            //skip yourself
            if($member->getId() == $user->getId()){
                continue;
            }
            $organizationMember = new OrganizationUnconfirmedMembers();
            $organizationMember->setId(Uuid::v4());
            $organizationMember->setOrganization($newOrganization);
            $organizationMember->setMember($member);
            $organizationMember->setStatus(OrganizationUnconfirmedMembers::STATUS_INVITED);

            $entityManager->persist($organizationMember);
        }
        // creativeWork only created when lock status is true
//        $creativeWork = new CreativeWork();
//        $creativeWork->setId(Uuid::v4());
//        $creativeWork->setCreatedFor($project);
//        $creativeWork->setCreator($newOrganization);

//        $entityManager->persist($creativeWork);
        $entityManager->flush();

        return $newOrganization;
//        return new JsonResponse([]);
    }
}
