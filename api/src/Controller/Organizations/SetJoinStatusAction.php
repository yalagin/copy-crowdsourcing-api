<?php


namespace App\Controller\Organizations;


use App\Entity\OrganizationUnconfirmedMembers;
use App\Exception\OrganizationException;
use App\Service\OrganizationService;
use InvalidArgumentException;

class SetJoinStatusAction
{
    public function __invoke(OrganizationUnconfirmedMembers $data, OrganizationService $organizationService)
    {
        $user = $data->getMember();
        $organization = $data->getOrganization();
        if (!$user || !$organization) {
            throw new InvalidArgumentException('invalid invitation');
        }

        $organizationService->checkIfUserAlreadyMemberInTeamForCurrentProject($user, $organization->getProject());

        if (!$organization->isLocked() && $organization->isOpenForJoin()) {
            return $data->setStatus(OrganizationUnconfirmedMembers::STATUS_WANTED_TO_JOIN);
        }
        throw new OrganizationException('Tim sudah terkunci atau tidak terbuka untuk umum');

    }
}
