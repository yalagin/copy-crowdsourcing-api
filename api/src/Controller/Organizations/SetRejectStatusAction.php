<?php


namespace App\Controller\Organizations;


use App\Entity\OrganizationUnconfirmedMembers;
use App\Exception\OrganizationException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SetRejectStatusAction  extends AbstractController
{
    public function __invoke(OrganizationUnconfirmedMembers $data)
    {
//        if($data->getOrganization() && !$data->getOrganization()->isLocked()) {
            if($this->getUser() === $data->getMember()){
                $data->setRejectedReason('User declined invitation');
            }else{
                $data->setRejectedReason('Founder canceled invitation');
            }
            return $data->setStatus(OrganizationUnconfirmedMembers::STATUS_REJECTED);
//        }
//        throw new OrganizationLockedException('Organization is already locked');
    }
}
