<?php


namespace App\Controller\Organizations;

use App\Entity\Organization;
use App\Entity\User;
use App\Exception\OrganizationException;
use App\Exception\UnauthorizedUserException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;


class RemoveMemberFromOrganizationAction
{
    public function __invoke(Organization $data, EntityManagerInterface $entityManager,TokenStorageInterface $tokenStorage)
    {
        if(!$data->isLocked()) {
            $token = $tokenStorage->getToken();
            if (null === $token) {
                throw new UnauthorizedUserException();
            }
            /** @var User $user */
            $user = $token->getUser();

            if ($data->getMemberForRemoval() === $user && $data->getMembers()->count() <= 1) {
                $entityManager->remove($data);
            } elseif ($data->getMemberForRemoval() === $user) {
                $data->removeMember($user);
                $data->setFounder($data->getMembers()->first());
                $entityManager->persist($data);
            } else {
                $data->removeMember($data->getMemberForRemoval());
                $entityManager->persist($data);
            }
            $entityManager->flush();
            return $data;
        }
        throw new OrganizationException();
    }
}
