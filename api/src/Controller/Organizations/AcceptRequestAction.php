<?php


namespace App\Controller\Organizations;


use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Entity\OrganizationUnconfirmedMembers;
use App\Entity\Project;
use App\Entity\User;
use App\Exception\OrganizationException;
use App\Service\OrganizationService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;

class AcceptRequestAction
{
    public function __invoke(
        OrganizationUnconfirmedMembers $data,
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        OrganizationService $organizationService
    )
    {
        $validator->validate($data);
        if($data->getOrganization() && !$data->getOrganization()->isLocked()) {
            if($data->getOrganization()->getMembers()->count() >= 4){
                throw new OrganizationException('Sudah ada 4 anggota tim, kamu tidak bisa mengundang lagi');
            }

            // Add member to organization
            $organization = $data->getOrganization();
            $organization->addMember($data->getMember());
            $entityManager->persist($organization);
            $entityManager->remove($data);

            $organizationService->declineAllUserInvitationsForCurrentProject($data->getMember(),$organization->getProject());

            $entityManager->flush();

            return new JsonResponse([]);
        }
        throw new OrganizationException();
    }
}
