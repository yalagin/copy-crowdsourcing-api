<?php


namespace App\Controller\Organizations;


use App\Entity\OrganizationUnconfirmedMembers;
use App\Exception\OrganizationException;
use App\Service\OrganizationService;
use InvalidArgumentException;

class SetInviteStatusAction
{
    public function __invoke(OrganizationUnconfirmedMembers $data, OrganizationService $organizationService)
    {
        $user = $data->getMember();
        $organization = $data->getOrganization();
        if (!$user || !$organization) {
            throw new InvalidArgumentException('invalid invitation');
        }

        if ($organization->isLocked()) {
            throw new OrganizationException('organization already locked!');
        }

        $organizationService->checkIfUserAlreadyMemberInTeamForCurrentProject($user, $organization->getProject());

        return $data->setStatus(OrganizationUnconfirmedMembers::STATUS_INVITED);
    }
}
