<?php


namespace App\Controller\Organizations;

use App\Entity\Organization;
use App\Entity\OrganizationUnconfirmedMembers;
use App\Entity\User;
use App\Exception\UnauthorizedUserException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;


class LockOrganizationAction
{
    public function __invoke(Organization $data,EntityManagerInterface $entityManager)
    {
        $invitations = $entityManager
            ->getRepository(OrganizationUnconfirmedMembers::class)
            ->findBy(['organization' => $data->getId()]);

        foreach($invitations as $invitation){
            $invitation->setStatus(OrganizationUnconfirmedMembers::STATUS_REJECTED);
            $invitation->setRejectedReason('Team is locked');
            $entityManager->persist($invitation);
        }
        return $data->setIsLocked(true);
    }
}
