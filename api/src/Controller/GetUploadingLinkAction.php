<?php


namespace App\Controller;


use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Entity\MediaObject;
use App\Security\TokenGenerator;
use App\Service\AwsS3Service;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Uid\Uuid;

class GetUploadingLinkAction
{
    /**
     * The __invoke method is called when a script tries to call an object as a function.
     *
     * @param MediaObject $data
     * @param AwsS3Service $awsS3Service
     * @param ValidatorInterface $validator
     * @return mixed
     * @link https://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
     */
    public function __invoke(MediaObject $data, AwsS3Service $awsS3Service, ValidatorInterface $validator)
    {
        $violationList = $validator->validate($data, null, ['media_object:items:get_link']);
        if($violationList->count()){
            return $violationList;
        }


        $presignedUrl = $awsS3Service->getSignedUrl(Uuid::v4().'.'.$data->getFileExtension());
        return new JsonResponse([
            "url" => $presignedUrl
        ]);
    }
}
