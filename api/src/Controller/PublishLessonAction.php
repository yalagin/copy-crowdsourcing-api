<?php


namespace App\Controller;


use App\Entity\Lesson;

class PublishLessonAction
{
    public function __invoke(Lesson $data)
    {
        $data->setPublishedTime(new \DateTime());
        return $data->setPublished(true);
    }
}
