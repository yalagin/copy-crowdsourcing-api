<?php

namespace App\Controller;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Email\Mailer;
use App\Entity\User;
use App\Entity\UserCustomActions;
use App\Exception\InvalidConfirmationTokenException;
use App\Repository\UserRepository;
use App\Security\TokenGenerator;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SendConfirmationTokenToEmailAction
{
    public function __invoke(UserCustomActions $data, UserRepository $userRepository, LoggerInterface $logger, Mailer $mailer, ValidatorInterface $validator, EntityManagerInterface $entityManager,TokenGenerator $tokenGenerator)
    {
        $validator->validate($data);

        $logger->debug('Fetching user by email');

        $user = $userRepository->findOneBy(
            ['email' => $data->getEmail()]
        );

        // User was NOT found by confirmation token
        if (!$user) {
            $logger->debug('User by email not found');
            throw new NotFoundHttpException();
        }
        $logger->debug('Send confirmation email for User #'.$user->getId());

        if(!$user->getEnabled()) {
            $mailer->sendConfirmationEmail($user);
        }

        return new JsonResponse(['send'=>true]);
    }
}
