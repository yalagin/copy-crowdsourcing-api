<?php

namespace App\Controller;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Email\Mailer;
use App\Entity\User;
use App\Entity\UserCustomActions;
use App\Exception\InvalidConfirmationTokenException;
use App\Repository\UserRepository;
use App\Security\TokenGenerator;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SendResetPasswordLinkToEmailAction
{


    public function __invoke(UserCustomActions $data, UserRepository $userRepository, LoggerInterface $logger, Mailer $mailer, ValidatorInterface $validator, EntityManagerInterface $entityManager,TokenGenerator $tokenGenerator)
    {
        $validator->validate($data);

        $logger->debug('Fetching user by email');

        $user = $userRepository->findOneBy(
            ['email' => $data->getEmail()]
        );

        // User was NOT found by confirmation token
        if (!$user) {
            $logger->debug('User by email not found');
            throw new NotFoundHttpException();
        }
        $logger->debug('Set user confirmation token for User #'.$user->getId());
        $passwordResetToken = $tokenGenerator->getRandomSecureToken();
        $user->setPasswordResetToken(
            $passwordResetToken
        );
        $user->setPasswordResetTokenTimeStamp(new DateTime(date('Y-m-d H:i:s',strtotime("+1 week"))));
        $entityManager->flush();

        $logger->debug('Sending user resetting password for User #'.$user->getId());
        // Send e-mail here...
        $mailer->sendResetPasswordEmail($user);

        return new JsonResponse(['send'=>true]);
    }
}
