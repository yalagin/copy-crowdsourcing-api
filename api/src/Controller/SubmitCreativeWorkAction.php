<?php


namespace App\Controller;


use App\Entity\CreativeWork;
use App\Exception\InvalidCreativeWorkException;
use App\Service\CreativeWorkService;

class SubmitCreativeWorkAction
{

    /**
     * @param CreativeWork $data
     * @param CreativeWorkService $creativeWorkHandler
     * @return CreativeWork
     * @throws InvalidCreativeWorkException
     */
    public function __invoke(CreativeWork $data, CreativeWorkService $creativeWorkHandler)
    {
        return $creativeWorkHandler->handle($data);
    }
}
