<?php


namespace App\Controller;


use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Email\Mailer;
use App\Entity\User;
use App\Entity\UserCustomActions;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class SendMobileApplicationLinkToEmailAction extends AbstractController
{
    /**
     * from the frontend hit an endpoint passing the email address
     * @param UserCustomActions $data
     * @param LoggerInterface $logger
     * @param Mailer $mailer
     * @param ValidatorInterface $validator
     */
    public function __invoke(UserCustomActions $data, LoggerInterface $logger, Mailer $mailer, ValidatorInterface $validator)
    {
        $validator->validate($data);

    //    /** @var User $user */
    //    $user = $this->getUser();
    //    $logger->debug('Send mobile app link for user '.$user->getId().' to email '. $data->getEmail());

        $mailer->sendLinkToMobileAppInEmail($data->getEmail());

        return new JsonResponse(['send'=>true]);
    }
}
