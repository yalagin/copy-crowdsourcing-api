<?php


namespace App\Controller;


use App\Entity\CreativeWork;
use App\Exception\InvalidCreativeWorkException;
use App\Service\CreativeWorkService;

class ImportCreativeWork
{
    /**
     * @param CreativeWork $data
     * @param CreativeWorkService $creativeWorkHandler
     * @throws InvalidCreativeWorkException
     */
    public function __invoke(CreativeWork $data, CreativeWorkService $creativeWorkHandler)
    {
        return $creativeWorkHandler->importToLesson($data);
//        return $data;
    }
}
