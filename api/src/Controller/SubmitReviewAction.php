<?php


namespace App\Controller;


use App\Entity\Review;

class SubmitReviewAction
{
    /**
     * @param Review $data
     * @return Review
     */
    public function __invoke(Review $data)
    {
        return $data->setIsSubmitted(true);
    }
}
