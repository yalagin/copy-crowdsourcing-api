<?php
// api/src/Controller/CreateMediaObjectAction.php

namespace App\Controller;

use App\Entity\AnimationObject;
use App\Entity\ImageObject;
use App\Entity\VideoObject;
use App\Service\MediaObjectService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Entity\MediaObject;
use Symfony\Component\HttpFoundation\Request;

class CreateMediaObjectAction
{
    public function __invoke(MediaObject $data, ValidatorInterface $validator,Request $request,EntityManagerInterface $entityManager,MediaObjectService $mediaObjectService)
    {
        $data = $mediaObjectService->processMediaObject($data, $request, $entityManager);

        $violationList = $validator->validate($data, null, ['media_object:items:create']);
        if($violationList->count()){
            return $violationList;
        }
        return $data;
    }
}
