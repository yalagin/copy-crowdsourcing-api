<?php


namespace App\DataPersister;


use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use App\Entity\CreativeWork;
use App\Exception\OrganizationException;
use Doctrine\ORM\EntityManagerInterface;

class CreativeWorkDataPersister implements DataPersisterInterface
{

    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @inheritDoc
     */
    public function supports($data): bool
    {
        return $data instanceof CreativeWork;
    }

    /**
     * @param CreativeWork $data
     * @inheritDoc
     * @throws OrganizationException
     */
    public function persist($data)
    {
        if($data->getCreator()&&$data->getCreator()->isLocked()){
            $this->entityManager->persist($data);
            $this->entityManager->flush();
            return $data;
        }
        throw new OrganizationException('Organisasi tidak dikunci');
    }

    /**
     * @inheritDoc
     */
    public function remove($data)
    {
        $this->entityManager->remove($data);
        $this->entityManager->flush();
    }
}
