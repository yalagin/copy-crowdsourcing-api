<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use App\Email\Mailer;
use App\Entity\ImageObject;
use App\Entity\Person;
use App\Entity\User;
use App\Exception\UnauthorizedRoleException;
use App\Security\TokenGenerator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Uid\Uuid;

class UserDataPersister implements DataPersisterInterface
{
    private EntityManagerInterface $entityManager;
    private UserPasswordEncoderInterface $userPasswordEncoder;
    private TokenGenerator $tokenGenerator;
    private Mailer $mailer;
    private RequestStack $requestStack;

    public function __construct(EntityManagerInterface $entityManager,
                                UserPasswordEncoderInterface $userPasswordEncoder,
                                TokenGenerator $tokenGenerator,
                                Mailer $mailer,
                                RequestStack $requestStack)
    {
        $this->entityManager = $entityManager;
        $this->userPasswordEncoder = $userPasswordEncoder;
        $this->tokenGenerator = $tokenGenerator;
        $this->mailer = $mailer;
        $this->requestStack = $requestStack;
    }

    public function supports($data): bool
    {
        return $data instanceof User;
    }

    /**
     * @param User $data
     * @return User
     */
    public function persist($data)
    {
        //saving hashing password here...
        if ($data->getPlainPassword()) {
            $data->setPassword(
                $this->userPasswordEncoder->encodePassword($data, $data->getPlainPassword())
            );
            $data->eraseCredentials();
            // After password change, old tokens are still valid
            $data->setPasswordChangeDate(time());
        }
        //assign Roles here...
        $roleAssigner = $data->getRoleAssigner();
        // only teacher and designer roles allowed
        if ($roleAssigner === User::ROLE_TEACHER || $roleAssigner === User::ROLE_DESIGNER) {
            //checking if teacher and designer roles already settled
            if (
                !in_array(User::ROLE_TEACHER, $data->getRoles(), true)
                && !in_array(User::ROLE_DESIGNER, $data->getRoles(), true)
                && !in_array(User::ROLE_ADMIN, $data->getRoles(), true)
            ) {
                $data->setRoles([$roleAssigner]);
            }
        }
        //check tags validation here...
        foreach ($data->getTags() as $tag) {
            if (!in_array($tag->getRole(), $data->getRoles())) {
                throw new UnauthorizedRoleException();
            }
        }

        // Create confirmation token...
        $data->setConfirmationToken(
            $this->tokenGenerator->getRandomSecureToken()
        );

        // Create person entity on user creation
        $request = $this->requestStack->getCurrentRequest();
        if($request->getMethod() == 'POST'){
            $person = new Person();
            $person->setId(Uuid::v4());
            $person->setAuthor($data);
            $rand = rand(1, 500);
            $image_name = 'avatar_'.$rand.'.png';
            $image = $this->entityManager->getRepository(ImageObject::class)->findOneBy(['name' => $image_name]);
            $person->setImage($image);
            $this->entityManager->persist($person);
        }


        $this->entityManager->persist($data);
        $this->entityManager->flush();

        if(!$data->getEnabled()) {
            $this->mailer->sendConfirmationEmail($data);
        }
        return $data;
    }

    public function remove($data)
    {
        $this->entityManager->remove($data);
        $this->entityManager->flush();
    }
}
