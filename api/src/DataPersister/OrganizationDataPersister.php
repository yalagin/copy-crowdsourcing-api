<?php


namespace App\DataPersister;


use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use App\Entity\Organization;
use App\Entity\OrganizationUnconfirmedMembers;
use App\Entity\User;
use App\Exception\OrganizationException;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class OrganizationDataPersister implements DataPersisterInterface
{

    private EntityManagerInterface $em;
    /**
     * @var RequestStack
     */
    private RequestStack $requestStack;

    public function __construct(EntityManagerInterface $entityManager,RequestStack $requestStack)
    {
        $this->em = $entityManager;
        $this->requestStack = $requestStack;
    }

    /**
     * Is the data supported by the persister?
     * @param $data
     * @return bool
     */
    public function supports($data): bool
    {
        return $data instanceof Organization;
    }

    /**
     * Persists the data.
     *
     * @param Organization $data
     * @return object|void Void will not be supported in API Platform 3, an object should always be returned
     * @throws OrganizationException
     */
    public function persist($data)
    {
        $this->throwExceptionIfUserAlreadyHasAnOrganizationForThatProject($data);

        /**
         * confirmed member of a different team → show error: “You are already part of “Robbert and team” for this lesson request”
         */
        $organizations =  $this->em->getRepository(Organization::class)->findBy(['project' => $data->getProject()]);
        $request = $this->requestStack->getCurrentRequest();
        foreach($organizations as $organization){
            if($data->getId() !== $organization->getId()) {
                /** @var Collection $members */
                $members = $organization->getMembers();
                /** @var User $member */
                if (!$members->filter(fn($member) => $member->getId() === $data->getFounder()->getId())->isEmpty()) {
                    throw new OrganizationException("Kamu sudah menjadi bagian dari {$organization->getName()} untuk permintaan pelajaran ini");
                }

                /**
                 * on creating organization all invitations for current project are declined
                 */
                if($request->getMethod() == 'POST'){
                    $organizationUnconfirmedMembers = $organization->getOrganizationUnconfirmedMembers();
                    $usersInvitations = $organizationUnconfirmedMembers
                        ->filter(fn($member) => $member->getMember()->getId() === $data->getFounder()->getId());
                    if (!$usersInvitations->isEmpty()) {
                        /** @var OrganizationUnconfirmedMembers $invitation */
                        foreach($usersInvitations as $invitation){
                            if($invitation->getStatus() != OrganizationUnconfirmedMembers::STATUS_REJECTED) {
                                $invitation->setStatus(OrganizationUnconfirmedMembers::STATUS_REJECTED);
                                $invitation->setRejectedReason('user created team by him\her self');
                                $this->em->persist($invitation);
                            }
                        }
                    }
                }
            }
        }

        $this->em->persist($data);
        $this->em->flush();
    }

    /**
     * Removes the data.
     */
    public function remove($data)
    {
        $this->em->remove($data);
        $this->em->flush();
    }

    /**
     * @param Organization $data
     * @return Organization[]|object[]
     * @throws OrganizationException
     */
    public function throwExceptionIfUserAlreadyHasAnOrganizationForThatProject(Organization $data)
    {
        $organizations = $this->em->getRepository(Organization::class)->findBy([
            'project' => $data->getProject(),
            'founder' => $data->getFounder()
        ]);
        if (count($organizations) > 1) {
            throw new OrganizationException('Kamu sudah mempunyai tim untuk permintaan pelajaran ini');
        }
        return $organizations;
    }
}
