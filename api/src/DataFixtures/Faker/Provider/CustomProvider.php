<?php

namespace App\DataFixtures\Faker\Provider;

class CustomProvider
{
    public static function subject()
    {
        $arrX = array("Math", "Science");
        // get random index from array $arrX
        $randIndex = array_rand($arrX);
        // output the value for the random index
        return $arrX[$randIndex];
    }

    public static function specialization()
    {
        $arrX = array("Illustration", "Animation");
        // get random index from array $arrX
        $randIndex = array_rand($arrX);
        // output the value for the random index
        return $arrX[$randIndex];
    }

    public static function subtheme()
    {
        $arrX = [
            "The Beauty of Togetherness",
            "Cultural Diversity of My Nation",
            "Togetherness in Diversity",
            "Source of energy",
            "Usage of energy",
            "Renewable Energy",
        ];
        // get random index from array $arrX
        $randIndex = array_rand($arrX);
        // output the value for the random index
        return $arrX[$randIndex];
    }



    public static function reviewTag($counter)
    {
        $arrX = ["Communication", "Functional Skill", "Organization", "Teamwork", "Leadership"];
        return $arrX[$counter];
    }

    public static function convertToConstant($value)
    {
        return str_replace(' ', '_',strtoupper($value));
    }
}
