<?php


namespace App\EventListener;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\DisabledException;


class JWTCreatedListener
{
    private UserRepository $userRepository;
    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @param UserRepository $userRepository
     * @param LoggerInterface $logger
     */
    public function __construct(UserRepository $userRepository, LoggerInterface $logger)
    {
        $this->userRepository = $userRepository;
        $this->logger = $logger;
    }

    /**
     * @param JWTCreatedEvent $event
     *
     * @return void
     */
    public function onJWTCreated(JWTCreatedEvent $event)
    {
        $payload       = $event->getData();
        /** @var User $user
         * I did not do it through security token because test UserResourceTest::testResetPasswordViaToken failing
         */
        $user = $this->userRepository->findOneBy(
            ['email' => $payload['email']]
        );
        if (!$user) {
            $this->logger->debug('User by email not found email#'.$payload['email']);
            throw new NotFoundHttpException();
        }
        if(!$user->isAdmin() && !$user->getEnabled()) {
            throw new DisabledException();
        }

        $payload['uuid'] = "/users/".$user->getId();
        $payload['@id'] = "/users/".$user->getId();
        $payload['username'] = $user->getUsername();

        $event->setData($payload);
    }
}
