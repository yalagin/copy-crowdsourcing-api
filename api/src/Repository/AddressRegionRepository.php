<?php

namespace App\Repository;

use App\Entity\AddressRegion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AddressRegion|null find($id, $lockMode = null, $lockVersion = null)
 * @method AddressRegion|null findOneBy(array $criteria, array $orderBy = null)
 * @method AddressRegion[]    findAll()
 * @method AddressRegion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AddressRegionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AddressRegion::class);
    }

    // /**
    //  * @return AddressRegion[] Returns an array of AddressRegion objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AddressRegion
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
