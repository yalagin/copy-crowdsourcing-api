<?php


namespace App\Repository;


use App\Entity\Project;
use App\Entity\User;
use DateInterval;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Project|null find($id, $lockMode = null, $lockVersion = null)
 * @method Project|null findOneBy(array $criteria, array $orderBy = null)
 * @method Project[]    findAll()
 * @method Project[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjectRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Project::class);
    }

    public function getProjectsThatWillExpireInOneTwoAndFiveDays()
    {
        $from5 = new \DateTime((new \DateTime())->sub(new DateInterval('P5D'))->format("Y-m-d")." 00:00:00");
        $from2 = new \DateTime((new \DateTime())->sub(new DateInterval('P2D'))->format("Y-m-d")." 00:00:00");
        $from1 = new \DateTime((new \DateTime())->sub(new DateInterval('P1D'))->format("Y-m-d")." 00:00:00");
        $to5   = new \DateTime((new \DateTime())->sub(new DateInterval('P5D'))->format("Y-m-d")." 23:59:59");
        $to2   = new \DateTime((new \DateTime())->sub(new DateInterval('P2D'))->format("Y-m-d")." 23:59:59");
        $to1   = new \DateTime((new \DateTime())->sub(new DateInterval('P1D'))->format("Y-m-d")." 23:59:59");

        $qb = $this->createQueryBuilder("p");
        $qb
            ->andWhere('p.endTime BETWEEN :from5 AND :to5')->setParameter('from5', $from5 )->setParameter('to5', $to5)
            ->andWhere('p.endTime BETWEEN :from2 AND :to2')->setParameter('from2', $from2 )->setParameter('to2', $to2)
            ->andWhere('p.endTime BETWEEN :from1 AND :to1')->setParameter('from1', $from1)->setParameter('to1', $to1)
        ;
        $result = $qb->getQuery()->getResult();

        return $result;
    }

}
