<?php

namespace App\Repository;

use App\Entity\Organization;
use App\Entity\Project;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Organization|null find($id, $lockMode = null, $lockVersion = null)
 * @method Organization|null findOneBy(array $criteria, array $orderBy = null)
 * @method Organization[]    findAll()
 * @method Organization[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrganizationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Organization::class);
    }

    /**
    * @return Organization[] Returns an array of Organization objects
    */
    public function getOrganizationsForUserAndProject(User $user, Project $project)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.project = :project')
            ->andWhere(':user MEMBER OF o.members ')
            ->setParameter('project', $project)
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult()
        ;
    }
}
