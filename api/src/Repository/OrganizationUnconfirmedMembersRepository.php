<?php

namespace App\Repository;

use App\Entity\OrganizationUnconfirmedMembers;
use App\Entity\Project;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrganizationUnconfirmedMembers|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrganizationUnconfirmedMembers|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrganizationUnconfirmedMembers[]    findAll()
 * @method OrganizationUnconfirmedMembers[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrganizationUnconfirmedMembersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrganizationUnconfirmedMembers::class);
    }


    public function findInvitationsForProject(User $user,Project $project)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.member = :user')
            ->andWhere('o.status <> :status')
            ->andWhere('o.organization IN (:organizationIds)')
            ->setParameter('user', $user->getId())
            ->setParameter('status', OrganizationUnconfirmedMembers::STATUS_REJECTED)
            ->setParameter(
                'organizationIds',
                $project->getOrganizations()->map(fn($org)=>$org->getId())->toArray(),
                \Doctrine\DBAL\Connection::PARAM_STR_ARRAY
            )
            ->getQuery()
            ->getResult()
            ;
    }

    //new invites for user

    public function getInvitedUserIDs()
    {
        return call_user_func_array('array_merge', $this->createQueryBuilder('o')
            ->groupBy('o.member')
            ->select('(o.member)')
            ->andWhere('o.status = :status')
            ->setParameter('status', OrganizationUnconfirmedMembers::STATUS_INVITED)
            ->getQuery()
            ->getArrayResult())
            ;
    }

    public function getNewInvitationsForUser(User $user)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.status = :status')
            ->andWhere('o.member = :member')
            ->andWhere('o.emailSentCounter = 0')
            ->setParameter('status', OrganizationUnconfirmedMembers::STATUS_INVITED)
            ->setParameter('member', $user->getId())
            ->getQuery()
            ->getResult();
    }


    public function getOldInvitationsForUser(User $user)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.status = :status')
            ->andWhere('o.member = :member')
            ->andWhere('o.emailSentCounter > 0')
            ->setParameter('status', OrganizationUnconfirmedMembers::STATUS_INVITED)
            ->setParameter('member', $user->getId())
            ->getQuery()
            ->getResult();
    }

    //end new invites for user

    //get new invites for owner
    public function getOrganizationsThatReceivedJoinRequests(User $user)
    {
        $result = $this->createQueryBuilder('o')
            ->groupBy('organization.id')
            ->select('(organization)')
            ->andWhere('o.status = :status')
            ->andWhere('organization.founder = :founder')
            ->setParameter('status', OrganizationUnconfirmedMembers::STATUS_WANTED_TO_JOIN)
            ->setParameter('founder', $user->getId())
            ->join('o.organization', 'organization')
            ->getQuery()
            ->getScalarResult()
            ;

        return array_map('current', $result);
    }

    public function getFoundersIdsThatReceivedInvitation()
    {
        return call_user_func_array('array_merge',$this->createQueryBuilder('o')
            ->groupBy('organization.founder')
            ->select('(organization.founder)')
            ->andWhere('o.status = :status')
            ->setParameter('status', OrganizationUnconfirmedMembers::STATUS_WANTED_TO_JOIN)
            ->join('o.organization', 'organization',)
            ->getQuery()
            ->getArrayResult())
            ;
    }
    // end new invites for owner

    public function incrementCounterByOne()
    {
        $this->getEntityManager()->createQuery('
                UPDATE App\Entity\OrganizationUnconfirmedMembers t
                SET t.emailSentCounter = t.emailSentCounter + 1
                WHERE t.status = \'STATUS_WANTED_TO_JOIN\'
                OR  t.status = \'STATUS_INVITED\'
            ')
            ->execute();

        return $this;
    }

    public function changeStatusToRejectedIfCounterMoreThanAmountFromSettings()
    {
        $this->getEntityManager()->createQuery('
                UPDATE App\Entity\OrganizationUnconfirmedMembers t
                SET t.emailSentCounter = 0, t.status = \'STATUS_REJECTED\', t.rejectedReason = \'invitation expired\'
                WHERE t.status = \'STATUS_WANTED_TO_JOIN\' AND t.emailSentCounter > '. $_ENV['INVITATIONS_SENT_MAX_COUNTER'].'
                OR  t.status = \'STATUS_INVITED\'  AND t.emailSentCounter > '. $_ENV['INVITATIONS_SENT_MAX_COUNTER'].'
            ')
            ->execute();

        return $this;
    }
}
