<?php

namespace App\Command;

use App\Email\Mailer;
use App\Entity\OrganizationUnconfirmedMembers;
use App\Entity\User;
use App\Service\InvitationService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class SendTeamInvitesCommand extends Command
{
    protected static $defaultName = 'app:sendTeamInvites';

    /**
     * @var InvitationService
     */
    private InvitationService $invitationService;

    public function __construct(InvitationService $invitationService)
    {

        parent::__construct();
        $this->invitationService = $invitationService;
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);


        $this->invitationService->sendReminders($io);

        $io->block('');
        $io->success('Emails sent!');

        return Command::SUCCESS;
    }

}
