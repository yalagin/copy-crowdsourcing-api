<?php

namespace App\Command;

use App\Email\Mailer;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AssignInternalCommand extends Command
{
    protected static $defaultName = 'app:assign-internal';
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this
            ->setDescription('Add to user roles ROLE_INTERNAL')
            ->addArgument('email', InputArgument::REQUIRED, 'email of user')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $email = $input->getArgument('email');

        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['email'=>$email]);

        if(!$user){
            $io->error("user with ${email} email does not exists");
            return Command::FAILURE;
        }

        $user->setRoles(array_merge($user->getRoles(),[User::ROLE_INTERNAL]));
        $this->entityManager->flush();

        $io->success($email.' got role '. User::ROLE_INTERNAL);

        return Command::SUCCESS;
    }
}
