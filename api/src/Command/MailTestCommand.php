<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Email\Mailer;

class MailTestCommand extends Command
{
    protected static $defaultName = 'app:mail-test';

    private Mailer $mailer;

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    public function __construct(Mailer $mailer)
    {
        parent::__construct(null);
        $this->mailer = $mailer;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $arg1 = $input->getArgument('arg1');

        if ($arg1) {
            $io->note(sprintf('You passed an argument: %s', $arg1));
            $this->mailer->sendTestEmail($arg1);
            $io->success('You have a new command! Now make it your own! Pass --help to see your options.');
        }
        else {
            $io->error('Please pass the mail address as the first argument');
        }



        return Command::SUCCESS;
    }
}
