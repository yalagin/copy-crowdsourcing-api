<?php

namespace App\Command;

use App\Email\Mailer;
use App\Entity\Organization;
use App\Entity\Project;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ProjectReminderCommand extends Command
{
    protected static $defaultName = 'app:projectDeadlineReminder';
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;
    /**
     * @var Mailer
     */
    private Mailer $mailer;

    public function __construct(EntityManagerInterface $entityManager,Mailer $mailer)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $p = $this->entityManager->getRepository(Project::class)
            ->getProjectsThatWillExpireInOneTwoAndFiveDays();

        /** @var Project $project */
        foreach ($p as $project){
            /** @var Organization $org */
            foreach($project->getOrganizations() as $org){
                if($org->getCreativeWork() ||$org->getCreativeWork()->getIsSubmitted()) {
                    foreach ($org->getMembers() as $user) {
                        $this->mailer->sendDeadlineReminderForTeams($user, $org, $project);
                    }
                }
            }
        }

        $io->success('Done!');

        return Command::SUCCESS;
    }
}
