<?php

namespace App\Command;

use App\Entity\MediaObject;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class TranslateMediaCommand extends Command
{
    protected static $defaultName = 'app:translate-media';

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->note('purging media_object_translation table');
        $connection = $this->entityManager->getConnection();
        $dbPlatform = $connection->getDatabasePlatform();
        $q = $dbPlatform->getTruncateTableSql('media_object_translations',true);
        $connection->executeUpdate($q);
        $io->note('done purging media_object_translation table');


        $sql = 'SELECT * FROM media_object';
        $stmt = $connection->prepare($sql);
        $stmt->execute();
        $all = $stmt->fetchAll();

        $io->progressStart(count($all));
        /** @var MediaObject $mediaObject */
        foreach ($all as $mediaObject){
            $id = $mediaObject['id'];
            $filePath = $mediaObject['file_path'];
            $name = $mediaObject['name'];
            $sql = "INSERT INTO media_object_translations (locale, object_class, field, foreign_key, content) VALUES ('id', 'App\Entity\MediaObject', 'filePath', '${id}', '${filePath}');";
            $sql2 = "INSERT INTO media_object_translations (locale, object_class, field, foreign_key, content) VALUES ('id', 'App\Entity\MediaObject', 'name', '${id}', '${name}');";
            $stmt = $connection->prepare($sql);
            $stmt2 = $connection->prepare($sql2);
            $stmt->execute();
            $stmt2->execute();
            $io->progressAdvance();
        }
        $io->block('');
        $io->success('Done all media objects are now translated to ID language!');

        return Command::SUCCESS;
    }
}
