<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Validator\Constraints\CustomExpression;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use App\Controller\SubmitCreativeWorkAction;
use App\Controller\ImportCreativeWork;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\ExistsFilter;
use ApiPlatform\Core\Annotation\ApiFilter;

/**
 * The most generic kind of creative work, including books, movies, photographs, software programs, etc.
 *
 * @see http://schema.org/CreativeWork Documentation on Schema.org
 *
 * @author Maxim Yalagin <yalagin@gmail.com>
 *
 * @ORM\Entity
 * @ApiResource(
 *     iri="http://schema.org/CreativeWork",
 *     security="is_granted('ROLE_USER')",
 *     collectionOperations={
 *          "get"={
 *              "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *          },
 *          "post"={
 *              "denormalization_context"={"groups"={"creative_work:items:post"}},
 *              "validation_groups"={"Deafult","creative_work:items:post"},
 *          }
 *     },
 *      itemOperations={
 *           "get",
 *           "put"={
 *              "security"="user in object.getCreator().getMembers().toArray() or is_granted('ROLE_ADMIN')",
 *              "denormalization_context"={"groups"={"creative_work:items:change"}},
 *          },
 *          "patch"={
 *              "security"="user in object.getCreator().getMembers().toArray()  or is_granted('ROLE_ADMIN')",
 *              "denormalization_context"={"groups"={"creative_work:items:change"}},
 *          },
 *          "delete"={
 *              "security"="user == object.getCreator().getFounder() or is_granted('ROLE_ADMIN')",
 *              "denormalization_context"={"groups"={"creative_work:items:change"}},
 *          },
 *          "put-submit-creative-work"={
 *             "security"="user == object.getCreator().getFounder()",
 *             "method"="PUT",
 *             "path"="/creative_works/{id}/submit",
 *             "controller"=SubmitCreativeWorkAction::class,
 *             "denormalization_context"={"groups"={"creative_work:item:put-submit-creative-work:write"}},
 *             "validation_groups"={"creative_work:item:put-submit-creative-work"},
 *             "openapi_context"={"summary" = "Founder submits group work for judgement"}
 *         },
 *          "put-import-creative-work-to-lesson"={
 *             "security"="is_granted('ROLE_ADMIN')",
 *             "method"="PUT",
 *             "path"="/creative_works/{id}/import-to-lesson",
 *             "controller"=ImportCreativeWork::class,
 *             "denormalization_context"={"groups"={"creative_work:item:put-submit-creative-work:write"}},
 *             "validation_groups"={"creative_work:item:put-submit-creative-work"},
 *             "openapi_context"={"summary" = "Admin import creative work to lesson entitiy for submiting it to Titik Pintar platform"}
 *         }
 *     },
 *      normalizationContext={"groups"={"creative_work:read"}},
 * )
 * @UniqueEntity(fields={"id"})
 * @ApiFilter(SearchFilter::class, properties={"createdFor": "exact", "creator": "exact", "creator.members": "exact"})
 * @ApiFilter(BooleanFilter::class, properties={"creator.isOpenForJoin","creator.isLocked"})
 */
class CreativeWork extends AbstractDate
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @Assert\Uuid
     * @Assert\NotBlank
     * @Groups({"creative_work:items:post","creative_work:read"})
     */
    private $id;

    /**
     * @var Organization|null The creator/author of this CreativeWork. This is the same as the Author property for CreativeWork.
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Organization", inversedBy="creativeWork", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     * @ApiProperty(iri="http://schema.org/creator",readableLink=false, writableLink=false)
     * @Assert\NotBlank
     * @Groups({"creative_work:items:post","creative_work:read"})
     * @CustomExpression(
     *     "user == this.getCreator().getFounder()",
     *     message="You don't own that organization",
     *     groups={"creative_work:items:post"}
     * )
     */
    private $creator;

    /**
     * @var Project|null the subject matter of the content
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Project", inversedBy="creativeWorks")
     * @ORM\JoinColumn(nullable=true)
     * @ApiProperty(readableLink=false, writableLink=false)
     * @Assert\Expression(
     *     "this.getCreator().getProject() == this.getCreatedFor()",
     *     message="Project is not the same as in organization",
     *     groups={"creative_work:items:post"}
     * )
     * @Assert\NotBlank(groups={"creative_work:items:post"})
     * @Groups({"creative_work:items:post","creative_work:read"})
     */
    private $createdFor;

    /**
     * @var Collection<Question>|null indicates a CreativeWork that is (in some sense) a part of this CreativeWork
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Question", mappedBy="isPartOf", orphanRemoval=true, cascade={"persist", "remove", "merge"})
     * @ORM\JoinTable(inverseJoinColumns={@ORM\JoinColumn(nullable=false, unique=true)})
     * @ApiProperty(iri="http://schema.org/hasPart",readableLink=false, writableLink=false)
     * @Groups({"creative_work:items:post","creative_work:read","creative_work:items:change"})
     * @ApiSubresource
     */
    private $hasParts;

    /**
     * @var DocumentObject|null  DocumentObject
     * @ApiProperty(iri="http://schema.org/DocumentObject",readableLink=false, writableLink=false)
     * @ORM\ManyToOne(targetEntity=DocumentObject::class)
     * @Groups({"creative_work:items:post","creative_work:read","creative_work:items:change"})
     */
    private $document;

    /**
     * @var ArchiveObject|null  ArchiveObject
     * @ApiProperty(iri="http://schema.org/ArchiveObject",readableLink=false, writableLink=false)
     * @ORM\ManyToOne(targetEntity=ArchiveObject::class)
     * @Groups({"creative_work:items:post","creative_work:read","creative_work:items:change"})
     */
    private $archive;

    /**
     * @var VideoObject|null  VideoObject
     * @ApiProperty(iri="http://schema.org/VideoObject",readableLink=false, writableLink=false)
     * @ORM\ManyToOne(targetEntity=VideoObject::class)
     * @Groups({"creative_work:items:post","creative_work:read","creative_work:items:change"})
     */
    private $video;

    /**
     * @var string|null a transcript of creative work in text
     *
     * @ORM\Column(type="text", nullable=true)
     * @ApiProperty(iri="http://schema.org/description")
     * @Groups({"creative_work:items:post","creative_work:read","creative_work:items:change"})
     */
    private $transcript;

    /**
     * @var Collection<ImageObject>|null collection of media objects
     * @ApiProperty(iri="http://schema.org/ImageObject",readableLink=false, writableLink=false)
     * @ORM\ManyToMany(targetEntity=ImageObject::class)
     * @Groups({"creative_work:items:post","creative_work:read","creative_work:items:change"})
     */
    private $images;

    /**
     * @var Collection<AudioObject>|null AudioObject
     * @ApiProperty(iri="http://schema.org/AudioObject",readableLink=false, writableLink=false)
     * @ORM\ManyToMany(targetEntity=AudioObject::class)
     * @Groups({"creative_work:items:post","creative_work:read","creative_work:items:change"})
     */
    private $audio;

    //submitted fields

    /**
     * @var DateTimeInterface|null date of first broadcast/publication
     *
     * @ORM\Column(type="datetime", nullable=true)
     * @ApiProperty(iri="http://schema.org/datePublished")
     * @Assert\DateTime
     * @Groups({"creative_work:item:put-submit-creative-work"})
     */
    private $datePublished;

    /**
     * @ApiProperty(iri="http://schema.org/Boolean")
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"creative_work:read","creative_work:item:put-submit-creative-work"})
     */
    private $isSubmitted;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"creative_work:read","admin:write"})
     */
    private $rank;

    public function __construct()
    {
        $this->hasParts = new ArrayCollection();
        $this->isSubmitted = false;
        $this->images = new ArrayCollection();
        $this->audio = new ArrayCollection();
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }


    public function setDatePublished(?\DateTimeInterface $datePublished): void
    {
        $this->datePublished = $datePublished;
    }

    public function getDatePublished(): ?\DateTimeInterface
    {
        return $this->datePublished;
    }

    public function addHasPart(Question $hasPart): void
    {
        $this->hasParts[] = $hasPart;
    }

    public function removeHasPart(Question $hasPart): void
    {
        $this->hasParts->removeElement($hasPart);
    }

    public function getHasParts(): Collection
    {
        return $this->hasParts;
    }

    /**
     * @return mixed
     */
    public function getIsSubmitted()
    {
        return $this->isSubmitted;
    }

    /**
     * @param mixed $isSubmitted
     * @return CreativeWork
     */
    public function setIsSubmitted($isSubmitted)
    {
        $this->isSubmitted = $isSubmitted;
        return $this;
    }

    public function getRank(): ?int
    {
        return $this->rank;
    }

    public function setRank(?int $rank): self
    {
        $this->rank = $rank;

        return $this;
    }

    public function getCreator(): ?Organization
    {
        return $this->creator;
    }

    public function setCreator(Organization $creator): self
    {
        $this->creator = $creator;

        return $this;
    }

    public function getCreatedFor(): ?Project
    {
        return $this->createdFor;
    }

    public function setCreatedFor(?Project $createdFor): self
    {
        $this->createdFor = $createdFor;

        return $this;
    }

    /**
     * @return DocumentObject|null
     */
    public function getDocument(): ?DocumentObject
    {
        return $this->document;
    }

    /**
     * @param DocumentObject|null $document
     * @return CreativeWork
     */
    public function setDocument(?DocumentObject $document): CreativeWork
    {
        $this->document = $document;
        return $this;
    }

    /**
     * @return ArchiveObject|null
     */
    public function getArchive(): ?ArchiveObject
    {
        return $this->archive;
    }

    /**
     * @param ArchiveObject|null $archive
     * @return CreativeWork
     */
    public function setArchive(?ArchiveObject $archive): CreativeWork
    {
        $this->archive = $archive;
        return $this;
    }

    /**
     * @return VideoObject|null
     */
    public function getVideo(): ?VideoObject
    {
        return $this->video;
    }

    /**
     * @param VideoObject|null $video
     * @return CreativeWork
     */
    public function setVideo(?VideoObject $video): CreativeWork
    {
        $this->video = $video;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTranscript(): ?string
    {
        return $this->transcript;
    }

    /**
     * @param string|null $transcript
     * @return CreativeWork
     */
    public function setTranscript(?string $transcript): CreativeWork
    {
        $this->transcript = $transcript;
        return $this;
    }

    /**
     * @return Collection|ImageObject []
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(ImageObject $mediaObject): self
    {
        if (!$this->images->contains($mediaObject)) {
            $this->images[] = $mediaObject;
        }

        return $this;
    }

    public function removeImage(ImageObject $mediaObject): self
    {
        if ($this->images->contains($mediaObject)) {
            $this->images->removeElement($mediaObject);
        }

        return $this;
    }


    public function getAudio()
    {
        return $this->audio;
    }

    public function addAudio(AudioObject $mediaObject): self
    {
        if (!$this->audio->contains($mediaObject)) {
            $this->audio[] = $mediaObject;
        }

        return $this;
    }

    public function removeAudio(AudioObject $mediaObject): self
    {
        if ($this->audio->contains($mediaObject)) {
            $this->audio->removeElement($mediaObject);
        }

        return $this;
    }

//    public function setWinner(?Project $winner): self
//    {
//        $this->winner = $winner;
//
//        // set (or unset) the owning side of the relation if necessary
//        $newWinner = null === $winner ? null : $this;
//        if ($winner->getWinner() !== $newWinner) {
//            $winner->setWinner($newWinner);
//        }
//
//        return $this;
//    }


}
