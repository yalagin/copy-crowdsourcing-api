<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;
use App\Controller\ResetPasswordAction;
use App\Filter\ArrayFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;

/**
 *
 * @ApiResource(
 *     collectionOperations={
 *          "get"= {"security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')"},
 *          "post"={
 *              "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *              "validation_groups"={"Default", "user:items:post","user:write"},
 *              "method"="POST",
 *              "denormalization_context"={"groups"={"user:items:post","user:write"}},
 *          },
 *     },
 *     itemOperations={
 *          "get"= {"security"="is_granted('ROLE_USER')",
 *          "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"}},
 *          "patch"={
 *              "security"="is_granted('ROLE_USER') and object == user or is_granted('ROLE_ADMIN')",
 *              "denormalization_context"={"groups"={"user:item:patch"},
 *              "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"}},
 *              "validation_groups"={"user:item:patch"}
 *          },
 *          "put"={
 *              "security"="is_granted('ROLE_USER') and object == user or is_granted('ROLE_ADMIN')",
 *              "denormalization_context"={"groups"={"user:item:patch"},
 *              "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"}},
 *              "validation_groups"={"user:item:patch"}
 *          },
 *          "delete"={
     *          "security"="is_granted('ROLE_ADMIN')",
     *          "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"},
 *          },
 *         "put-reset-password"={
 *             "security"="is_granted('IS_AUTHENTICATED_FULLY') and object == user",
 *             "method"="PUT",
 *             "path"="/users/{id}/reset-password",
 *             "controller"=ResetPasswordAction::class,
 *             "denormalization_context"={"groups"={"user:item:put-reset-password"}},
 *             "normalization_context"={"groups"={"user:item:put-reset-password"}},
 *             "validation_groups"={"user:item:put-reset-password"}
 *         },
 *          "put-reset-password-with-token"={
 *             "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *             "method"="PUT",
 *             "path"="/users/{id}/reset-password-with-token",
 *             "controller"=ResetPasswordAction::class,
 *             "denormalization_context"={"groups"={"user:item:put-reset-password-with-token"}},
 *             "normalization_context"={"groups"={"user:item:put-reset-password-with-token"}},
 *             "validation_groups"={"user:item:put-reset-password-with-token"}
 *         }
 *     },
 *      normalizationContext={"groups"={"user:read"}},
 *      denormalizationContext={"groups"={"user:write"}},
 * )
 * @ApiFilter(PropertyFilter::class)
 * @UniqueEntity(fields={"email"})
 * @UniqueEntity(fields={"id"})
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository",)
 * @ORM\Table(name="`user`")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", hardDelete=false)
 * @ApiFilter(ArrayFilter::class,properties={"roles"})
 * @ApiFilter(SearchFilter::class,properties={"id"})
 * @ApiFilter(OrderFilter::class)
 * @ORM\HasLifecycleCallbacks
 */
class User implements UserInterface
{
    use SoftDeleteableEntity;

    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_USER = 'ROLE_USER';
    const ROLE_TEACHER = 'ROLE_TEACHER';
    const ROLE_DESIGNER = 'ROLE_DESIGNER';
    const ROLE_INTERNAL = 'ROLE_INTERNAL';

    /**
     * @var string UUID
     *
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @Groups({"user:read", "user:items:post"})
     * @Assert\Uuid(groups={"Default","user:items:post"})
     * @Assert\NotBlank(groups={"Default","user:items:post"})
     */
    private string $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups({"user:read", "user:items:post", "admin:write"})
     * @Assert\NotBlank(groups={"Default","user:items:post"})
     * @Assert\Email(groups={"Default","user:items:post"})
     * @Assert\Length(min=6, max=255,groups={"Default","user:items:post"})
     */
    private string $email;

    /**
     * @var array
     * @ORM\Column(type="json",nullable=true)
     * @Groups({"admin:read"})
     */
    private array $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private string $password;

    /**
     * @var string Full name
     * @ORM\Column(type="string", length=255, unique=true)
     * @Groups({"Default","user:read", "user:write", "user:item:patch"})
     * @Assert\NotBlank(groups={"user:items:post","user:item:patch"})
     * @Assert\Length(min=3, max=255,groups={"user:write", "user:item:patch"})
     */
    private string $username;

    /**
     * @SerializedName("password")
     * @Assert\Regex(
     *     pattern="/(?=.*[a-zA-Z])(?=.*[0-9]).{7,}/",
     *     message="Password must be seven characters long and contain at least one digit and letters",
     * )
     * @Assert\NotBlank(groups={"user:items:post"})
     * @Groups({"Default","user:items:post"})
     */
    private  $plainPassword;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tags")
     * @Groups({"user:write","user:item:patch","user:read"})
     * @ApiProperty(readableLink=false, writableLink=false)
     */
    private $tags;

    /**
     * @Groups({"user:write", "user:item:patch","user:items:post"})
     * @SerializedName("role")
     * @Assert\Choice(choices = {User::ROLE_DESIGNER, User::ROLE_TEACHER})
     */
    private $roleAssigner;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $passwordChangeDate;

    /**
     * @Groups({"admin:read"})
     * @ORM\Column(type="boolean")
     */
    private bool $enabled = false;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $confirmationToken;

    /**
     * @var string
     * @Groups({"user:item:put-reset-password","user:item:put-reset-password-with-token"})
     * @Assert\NotBlank(groups={"user:item:put-reset-password","user:item:put-reset-password-with-token"})
     * @Assert\Regex(
     *     pattern="/(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).{7,}/",
     *     message="Password must be seven characters long and contain at least one digit, one upper case letter and one lower case letter",
     *     groups={"user:item:put-reset-password","user:item:put-reset-password-with-token"}
     * )
     */
    private $newPassword;

    /**
     * @var string
     * @Groups({"user:item:put-reset-password","user:item:put-reset-password-with-token"})
     * @Assert\NotBlank(groups={"user:item:put-reset-password","user:item:put-reset-password-with-token"})
     * @Assert\Expression(
     *     "this.getNewPassword() === this.getNewRetypedPassword()",
     *     message="Passwords does not match",
     *     groups={"user:item:put-reset-password","user:item:put-reset-password-with-token"}
     * )
     */
    private $newRetypedPassword;

    /**
     * @var string
     * @Groups({"Default","user:item:put-reset-password"})
     * @Assert\NotBlank(groups={"user:item:put-reset-password"})
     * @UserPassword(groups={"user:item:put-reset-password"})
     */
    private $oldPassword;


    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $passwordResetToken;

    /**
     * @var string
     * @Groups({"Default","user:item:put-reset-password-with-token"})
     * @Assert\Expression(
     *     "this.getPasswordResetToken() === this.getPasswordResetTokenFromUser()",
     *     message="Token is invalid",
     *     groups={"Default","user:item:put-reset-password-with-token"}
     * )
     * @SerializedName("token")
     * @Assert\NotBlank(groups={"user:item:put-reset-password-with-token"})
     */
    private $passwordResetTokenFromUser;

    /**
     * @var null|DateTimeInterface
     *
     * @ORM\Column(type="datetime",nullable=true, options={"default": "CURRENT_TIMESTAMP"} )
     * @ApiProperty(iri="http://schema.org/DateTime")
     */
    private  $passwordResetTokenTimeStamp;

    /**
     * @var Collection<OrganizationUnconfirmedMembers>|null
     *
     * @ORM\OneToMany(targetEntity=OrganizationUnconfirmedMembers::class, mappedBy="member")
     * @ApiProperty(readableLink=false, writableLink=false)
     * @Groups({"user:read"})
     */
    private $organizationUnconfirmedMembers;

    /**
     * @var Collection<Organization>|null
     *
     * @ORM\ManyToMany(targetEntity=Organization::class, mappedBy="members")
     * @ApiProperty(readableLink=false, writableLink=false)
     * @Groups({"user:read"})
     */
    private $organizations;

    /**
     * @var Collection<BankAccount>|null
     *
     * @ORM\OneToMany(targetEntity=BankAccount::class, mappedBy="owner", orphanRemoval=true)
     * @ApiProperty(readableLink=false, writableLink=false)
     * @Groups({"user:read"})
     */
    private $bankAccounts;

    /**
     * @var Collection<Review>|null
     *
     * @ORM\OneToMany(targetEntity=Review::class, mappedBy="itemReviewed", orphanRemoval=true)
     * @ApiProperty(readableLink=false, writableLink=false)
     */
    private $reviews;

    /**
     * @var Collection<Review>|null
     *
     * @ORM\OneToMany(targetEntity=Review::class, mappedBy="author", orphanRemoval=true)
     * @ApiProperty(readableLink=false, writableLink=false)
     */
    private $reviewsGiven;

    /**
     * @var Person|null
     *
     * @ORM\OneToOne(targetEntity=Person::class, mappedBy="user", cascade={"persist", "remove"})
     * @ApiProperty(readableLink=false, writableLink=false)
     */
    private $person;

    use SoftDeleteableEntity;

    /**
     * @var null|DateTimeInterface
     *
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     * @ApiProperty(iri="http://schema.org/DateTime")
     * @Groups({"read","admin:read"})
     */
    protected  $createdAt;

    /**
     * @var null|DateTimeInterface
     *
     * @ORM\Column(type="datetime",nullable=true, options={"default": "CURRENT_TIMESTAMP"} )
     * @ApiProperty(iri="http://schema.org/DateTime")
     * @Groups({"read","admin:read"})
     */
    protected  $updatedAt;

    public function setCreatedAt( DateTimeInterface $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getCreatedAt():? DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setUpdatedAt( DateTimeInterface $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function getUpdatedAt():? DateTimeInterface
    {
        return $this->updatedAt;
    }



    public function __construct()
    {
        $this->tags = new ArrayCollection();
        $this->organizationUnconfirmedMembers = new ArrayCollection();
        $this->organizations = new ArrayCollection();
        $this->bankAccounts = new ArrayCollection();
        $this->reviews = new ArrayCollection();
        $this->reviewsGiven = new ArrayCollection();
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * @throws \Exception
     * @throws \Exception
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new \DateTime(date('Y-m-d H:i:s')));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        }
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }


    public function getId(): string
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        $this->plainPassword = null;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    /**
     * @return Collection|Tags[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tags $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function removeTag(Tags $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function getRoleAssigner(): ?string
    {
        return $this->roleAssigner;
    }

    /**
     * @param string|null $roleAssigner
     */
    public function setRoleAssigner(?string $roleAssigner): void
    {
        $this->roleAssigner = $roleAssigner;
    }

    /**
     * @return mixed
     */
    public function getPasswordChangeDate()
    {
        return $this->passwordChangeDate;
    }

    /**
     * @param mixed $passwordChangeDate
     * @return User
     */
    public function setPasswordChangeDate($passwordChangeDate)
    {
        $this->passwordChangeDate = $passwordChangeDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param mixed $enabled
     * @return User
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getConfirmationToken()
    {
        return $this->confirmationToken;
    }

    /**
     * @param mixed $confirmationToken
     * @return User
     */
    public function setConfirmationToken($confirmationToken)
    {
        $this->confirmationToken = $confirmationToken;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNewPassword()
    {
        return $this->newPassword;
    }

    /**
     * @param mixed $newPassword
     * @return User
     */
    public function setNewPassword($newPassword)
    {
        $this->newPassword = $newPassword;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNewRetypedPassword()
    {
        return $this->newRetypedPassword;
    }

    /**
     * @param mixed $newRetypedPassword
     * @return User
     */
    public function setNewRetypedPassword($newRetypedPassword)
    {
        $this->newRetypedPassword = $newRetypedPassword;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOldPassword()
    {
        return $this->oldPassword;
    }

    /**
     * @param mixed $oldPassword
     * @return User
     */
    public function setOldPassword($oldPassword)
    {
        $this->oldPassword = $oldPassword;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPasswordResetToken()
    {
        return $this->passwordResetToken;
    }

    /**
     * @param mixed $passwordResetToken
     * @return User
     */
    public function setPasswordResetToken($passwordResetToken)
    {
        $this->passwordResetToken = $passwordResetToken;
        return $this;
    }

    /**
     * @return null| string
     */
    public function getPasswordResetTokenFromUser(): ?string
    {
        return $this->passwordResetTokenFromUser;
    }

    /**
     * @param string $passwordResetTokenFromUser
     * @return User
     */
    public function setPasswordResetTokenFromUser(string $passwordResetTokenFromUser): User
    {
        $this->passwordResetTokenFromUser = $passwordResetTokenFromUser;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getPasswordResetTokenTimeStamp(): ?DateTimeInterface
    {
        return $this->passwordResetTokenTimeStamp;
    }

    /**
     * @param DateTimeInterface|null $passwordResetTokenTimeStamp
     * @return User
     */
    public function setPasswordResetTokenTimeStamp(?DateTimeInterface $passwordResetTokenTimeStamp): User
    {
        $this->passwordResetTokenTimeStamp = $passwordResetTokenTimeStamp;
        return $this;
    }

    /**
     * @return Collection|OrganizationUnconfirmedMembers[]
     */
    public function getOrganizationUnconfirmedMembers(): Collection
    {
        return $this->organizationUnconfirmedMembers;
    }

    public function addOrganizationMember(OrganizationUnconfirmedMembers $organizationMember): self
    {
        if (!$this->organizationUnconfirmedMembers->contains($organizationMember)) {
            $this->organizationUnconfirmedMembers[] = $organizationMember;
            $organizationMember->setMember($this);
        }

        return $this;
    }

    public function removeOrganizationMember(OrganizationUnconfirmedMembers $organizationMember): self
    {
        if ($this->organizationUnconfirmedMembers->contains($organizationMember)) {
            $this->organizationUnconfirmedMembers->removeElement($organizationMember);
            // set the owning side to null (unless already changed)
            if ($organizationMember->getMember() === $this) {
                $organizationMember->setMember(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Organization[]
     */
    public function getOrganizations(): Collection
    {
        return $this->organizations;
    }

    public function addOrganization(Organization $organization): self
    {
        if (!$this->organizations->contains($organization)) {
            $this->organizations[] = $organization;
            $organization->addMember($this);
        }

        return $this;
    }

    public function removeOrganization(Organization $organization): self
    {
        if ($this->organizations->contains($organization)) {
            $this->organizations->removeElement($organization);
            $organization->removeMember($this);
        }

        return $this;
    }

    /**
     * @return Collection|BankAccount[]
     */
    public function getBankAccounts(): Collection
    {
        return $this->bankAccounts;
    }

    public function addBankAccount(BankAccount $bankAccount): self
    {
        if (!$this->bankAccounts->contains($bankAccount)) {
            $this->bankAccounts[] = $bankAccount;
            $bankAccount->setOwner($this);
        }

        return $this;
    }

    public function removeBankAccount(BankAccount $bankAccount): self
    {
        if ($this->bankAccounts->contains($bankAccount)) {
            $this->bankAccounts->removeElement($bankAccount);
            // set the owning side to null (unless already changed)
            if ($bankAccount->getOwner() === $this) {
                $bankAccount->setOwner(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Review[]
     */
    public function getReviews(): Collection
    {
        return $this->reviews;
    }

    public function addReview(Review $review): self
    {
        if (!$this->reviews->contains($review)) {
            $this->reviews[] = $review;
            $review->setItemReviewed($this);
        }

        return $this;
    }

    public function removeReview(Review $review): self
    {
        if ($this->reviews->contains($review)) {
            $this->reviews->removeElement($review);
            // set the owning side to null (unless already changed)
            if ($review->getItemReviewed() === $this) {
                $review->setItemReviewed(null);
            }
        }

        return $this;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function setPerson(Person $person): self
    {
        $this->person = $person;

        // set the owning side of the relation if necessary
        if ($person->getUser() !== $this) {
            $person->setUser($this);
        }

        return $this;
    }

    public function isAdmin()
    {
        if( in_array(self::ROLE_ADMIN, $this->getRoles()))
            return true;
    }

    /**
     * @return Collection|null
     */
    public function getReviewsGiven(): ?Collection
    {
        return $this->reviewsGiven;
    }

    /**
     * @param Collection|null $reviewsGiven
     * @return User
     */
    public function setReviewsGiven(?Collection $reviewsGiven): User
    {
        $this->reviewsGiven = $reviewsGiven;
        return $this;
    }



}
