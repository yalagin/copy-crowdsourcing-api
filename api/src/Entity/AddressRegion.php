<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\AddressRegionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * The region in which the locality is, and which is in the country. For example, California or another appropriate first-level Administrative division see https://en.wikipedia.org/wiki/List_of_administrative_divisions_by_country
 *
 * @see http://schema.org/addressRegion Documentation on Schema.org
 *
 * @author Maxim Yalagin <yalagin@gmail.com>
 *
 * @ORM\Entity(repositoryClass=AddressRegionRepository::class)
 * @ApiResource( iri="http://schema.org/addressRegion",
 * security="is_granted('ROLE_ADMIN')",
 *     collectionOperations={
 *          "get"={
 *              "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *          },
 *          "post"={
 *              "denormalization_context"={"groups"={"address-region:write","address-region:items:create"}},
 *          },
 *     },
 *     itemOperations={
 *           "get"={
 *              "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *          },
 *          "put",
 *          "patch",
 *          "delete"
 *     },
 *     normalizationContext={"groups"={"address-region:read"}},
 *     denormalizationContext={"groups"={"address-region:write"}}
 * )
 *  * @ApiFilter(SearchFilter::class, properties={"id": "exact", "people": "exact", "name": "partial"})
 * @UniqueEntity(fields={"id"})
 */
class AddressRegion extends AbstractDate
{
    /**
     * @var string UUID
     *
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @Assert\Uuid
     * @Assert\NotBlank()
     * @Groups({"address-region:read","address-region:items:create"})
     */
    private string $id;

    /**
     * @var string name of region or province
     *
     * @Assert\NotBlank()
     * @ORM\Column(type="string")
     * @Groups({"address-region:read","address-region:write"})
     */
    private $name;

    /**
     * @var Person people
     *
     * @Assert\NotBlank()
     * @Groups({"address-region:read","address-region:write"})
     * @ORM\OneToMany(targetEntity=Person::class, mappedBy="addressRegion")
     */
    private $people;

    public function __construct()
    {
        $this->people = new ArrayCollection();
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Person[]
     */
    public function getPeople(): Collection
    {
        return $this->people;
    }

    public function addPerson(Person $person): self
    {
        if (!$this->people->contains($person)) {
            $this->people[] = $person;
            $person->setAddressRegion($this);
        }

        return $this;
    }

    public function removePerson(Person $person): self
    {
        if ($this->people->contains($person)) {
            $this->people->removeElement($person);
            // set the owning side to null (unless already changed)
            if ($person->getAddressRegion() === $this) {
                $person->setAddressRegion(null);
            }
        }

        return $this;
    }

    /**
     * @return int
     * @Groups({"address-region:read"})
     */
    public function getPeopleCounter(): int
    {
        return $this->people->count();
    }
}
