<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Validator\Constraints\CustomExpression;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * A specific question - e.g. from a user seeking answers online, or collected in a Frequently Asked Questions (FAQ) document.
 *
 * @see http://schema.org/Question Documentation on Schema.org
 *
 * @author Maxim Yalagin <yalagin@gmail.com>
 *
 * @ORM\Entity
 * @ApiResource(
 *     iri="http://schema.org/Question",
 *     security="is_granted('ROLE_USER')",
 *     collectionOperations={
 *          "get",
 *          "post"={
 *              "denormalization_context"={"groups"={"question:items:post"}},
 *          }
 *     },
 *      itemOperations={
 *           "get",
 *           "put"={
 *              "security"="user in object.getCreator().getMembers().toArray() or is_granted('ROLE_ADMIN')",
 *              "denormalization_context"={"groups"={"question:items:change"}},
 *          },
 *          "patch"={
 *              "security"="user in object.getCreator().getMembers().toArray()  or is_granted('ROLE_ADMIN')",
 *              "denormalization_context"={"groups"={"question:items:change"}},
 *          },
 *          "delete"={
 *              "security"="user in object.getCreator().getMembers().toArray() or is_granted('ROLE_ADMIN')",
 *              "denormalization_context"={"groups"={"question:items:change"}},
 *          }
 *     },
 *     normalizationContext={"groups"={"question:read"}}
 * )
 * @UniqueEntity("id")
 */
class Question extends AbstractDate
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @Assert\Uuid
     * @Assert\NotBlank
     * @Groups({"question:items:post","question:read"})
     */
    private $id;

    /**
     * @var Organization The creator/author of this CreativeWork. This is the same as the Author property for CreativeWork.
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Organization")
     * @ApiProperty(iri="http://schema.org/creator",readableLink=false, writableLink=false)
     * @Assert\NotNull
     * @Groups({"question:items:post","question:read"})
     * @CustomExpression(
     *     "user in this.getCreator().getMembers().toArray()",
     *     message="You don't belong in that organization",
     * )
     */
    private $creator;

    /**
     * @var CreativeWork|null indicates a CreativeWork that this CreativeWork is (in some sense) part of
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\CreativeWork", inversedBy="hasParts")
     * @ORM\JoinColumn(nullable=false)
     * @ApiProperty(iri="http://schema.org/isPartOf",readableLink=false, writableLink=false)
     * @Groups({"question:items:post","question:read"})
     * @Assert\Expression(
     *     "this.getCreator() == this.getIsPartOf().getCreator()",
     *     message="Your organization don't match",
     * )
     */
    private $isPartOf;

    /**
     * @var Collection<Answer>|null
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Answer", mappedBy="parentItem", orphanRemoval=true, cascade={"persist", "remove", "merge"})
     * @ORM\JoinTable(inverseJoinColumns={@ORM\JoinColumn(nullable=false, unique=true)})
     * @Groups({"question:items:post","question:read","question:items:change"})
     * @ApiSubresource
     */
    private $connectedAnswers;

    /**
     * @var ?string a description of the item
     *
     * @ORM\Column(type="text", nullable=true)
     * @ApiProperty(iri="http://schema.org/description")
     * @Groups({"question:items:post","question:read","question:items:change"})
     */
    private $description;

    /**
     * @var ?string string|null a visual brief for the designer to create the question image
     *
     * @ORM\Column(type="text", nullable=true)
     * @ApiProperty(iri="http://schema.org/description")
     * @Groups({"question:items:post","question:read","question:items:change"})
     */
    private $visualBrief;

    /**
     * @var string|null the answer key that is shown as a hint
     *
     * @ORM\Column(type="text", nullable=true)
     * @ApiProperty(iri="http://schema.org/description")
     * @Groups({"question:items:post","question:read","question:items:change"})
     */
    private $answerKey;

    /**
     * @var string a type of item
     *
     * @ORM\Column(type="text")
     * @ApiProperty(iri="http://schema.org/type")
     * @Assert\NotNull
     * @Groups({"question:items:post","question:read","question:items:change"})
     */
    private $type;

    /**
     * @var ImageObject|null An image of the item. This can be a \[\[URL\]\] or a fully described \[\[ImageObject\]\].
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\ImageObject")
     * @ApiProperty(iri="http://schema.org/ImageObject")
     * @Groups({"question:items:post","question:read","question:items:change"})
     * @ApiSubresource
     */
    private $image;

    /**
     * @var integer|null this is for convenient sorting
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"question:items:post","question:read","question:items:change"})
     */
    private $ordering;



//    public function __toString()
//    {
//        return $this->description;
//    }

    public function __construct()
    {
        $this->connectedAnswers = new ArrayCollection();
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setCreator(Organization $creator): void
    {
        $this->creator = $creator;
    }

    public function getCreator(): Organization
    {
        return $this->creator;
    }

    public function setIsPartOf(?CreativeWork $isPartOf): void
    {
        $this->isPartOf = $isPartOf;
    }

    public function getIsPartOf(): ?CreativeWork
    {
        return $this->isPartOf;
    }

    public function addAcceptedAnswer(Answer $acceptedAnswer): void
    {
        $this->connectedAnswers[] = $acceptedAnswer;
    }

    public function removeAcceptedAnswer(Answer $acceptedAnswer): void
    {
        $this->connectedAnswers->removeElement($acceptedAnswer);
    }

    public function getConnectedAnswers(): Collection
    {
        return $this->connectedAnswers;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param MediaObject|null $image
     */
    public function setImage($image): void
    {
        $this->image = $image;
    }

    /**
     * @return MediaObject|null
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Question
     */
    public function setType(string $type): Question
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAnswerKey(): ?string
    {
        return $this->answerKey;
    }

    public function setAnswerKey(?string $answerKey): Question
    {
        $this->answerKey = $answerKey;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getVisualBrief(): ?string
    {
        return $this->visualBrief;
    }

    /**
     * @param string|null $visualBrief
     */
    public function setVisualBrief(?string $visualBrief): void
    {
        $this->visualBrief = $visualBrief;
    }

    /**
     * @return int|null
     */
    public function getOrdering(): ?int
    {
        return $this->ordering;
    }

    /**
     * @param int|null $ordering
     */
    public function setOrdering(?int $ordering): void
    {
        $this->ordering = $ordering;
    }
}
