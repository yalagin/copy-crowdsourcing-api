<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * Teacher or designer specialized in tags
 *
 * @author Maxim Yalagin <yalagin@gmail.com>
 *
 * @ORM\Entity
 * @ApiResource(
 *     collectionOperations={
 *     "get"={
 *              "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *     },
 *     "post"={
 *              "security"="is_granted('ROLE_ADMIN')",
 *              "denormalization_context"={"groups"={"user_tag:write","user_tag:items:post"}},
 *     }},
 *     itemOperations={
 *     "patch"={
 *          "security"="is_granted('ROLE_ADMIN')",
 *          "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"}
 *     },
 *     "put"={
 *          "security"="is_granted('ROLE_ADMIN')",
 *          "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"}
 *     },
 *     "get"={
 *          "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *          "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"}
 *     },
 *     "delete"={
 *          "security"="is_granted('ROLE_ADMIN')",
 *          "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"},
 *      }},
 *     normalizationContext={"groups"={"user_tag:read"}},
 *     denormalizationContext={"groups"={"user_tag:write"}},
 *     )
 * @ApiFilter(SearchFilter::class, properties={"id": "exact", "role": "exact", "tag": "partial"})
 * @UniqueEntity(fields={"id"})
 *
 * basically it's subjects todo rename to subjects
 */
class Tags extends AbstractDate
{
    const REVIEW_TAG = 'REVIEW_TAG';
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @Assert\Uuid
     * @Groups({"user_tag:read","user_tag:items:post","lesson:read","lesson:items:read"})
     */
    private string $id;

    /**
     * @var string teacher or designer
     *
     * @ORM\Column(type="text", nullable=false)
     * @Assert\NotBlank()
     * @Groups({"user_tag:read","user_tag:write"})
     */
    private string $role;

    /**
     * @var string what user teaching or specialized in
     *
     * @ORM\Column(type="text", nullable=false)
     * @Groups({"user_tag:read","user_tag:write","lesson:read","lesson:items:read"})
     * @Assert\NotBlank()
     */
    private string $tag;

    /**
     * @var string what user teaching or specialized in
     *
     * @ORM\Column(type="text", nullable=false)
     * @Groups({"user_tag:read","user_tag:write","lesson:read","lesson:items:read"})
     * @Assert\NotBlank()
     */
    private string $name;

    /**
     * @var string name for Indonesian
     *
     * @ORM\Column(type="text", nullable=false)
     * @Groups({"user_tag:read","user_tag:write","lesson:read","lesson:items:read"})
     * @Assert\NotBlank()
     */
    private string $value;


    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setTag(string $tag): void
    {
        $this->tag = $tag;
    }

    public function getTag(): ?string
    {
        return $this->tag;
    }

    public function setRole(string $role): void
    {
        $this->role = $role;
    }

    public function getRole(): string
    {
        return $this->role;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Tags
     */
    public function setName(string $name): Tags
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return Tags
     */
    public function setValue(string $value): Tags
    {
        $this->value = $value;
        return $this;
    }



}
