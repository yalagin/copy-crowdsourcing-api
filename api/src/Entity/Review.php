<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use App\Controller\SubmitReviewAction;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * A review of an item - for example, of a restaurant, movie, or store.
 *
 * @see http://schema.org/Review Documentation on Schema.org
 *
 * @author Maxim Yalagin <yalagin@gmail.com>
 *
 * @ORM\Entity
 * @ApiResource(iri="http://schema.org/Review",
 *     collectionOperations={
 *     "get"={
 *              "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *     },
 *     "post"={
 *              "security"="is_granted('ROLE_ADMIN')",
 *              "denormalization_context"={"groups"={"review:write","review:items:post"}},
 *              "openapi_context"={"summary" = "Only for Admin. Resource is outomatically genereted so user don't need to create review."}
 *     }},
 *     itemOperations={
 *     "patch"={
 *          "security"="is_granted('ROLE_USER') and user == object.getAuthor() or is_granted('ROLE_ADMIN')",
 *          "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"}
 *     },
 *     "put"={
 *          "security"="is_granted('ROLE_USER') and user == object.getAuthor() or is_granted('ROLE_ADMIN')",
 *          "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"},
 *          "controller"=SubmitReviewAction::class,
 *     },
 *     "get"={
 *          "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *          "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"},
 *          "controller"=SubmitReviewAction::class,
 *     },
 *     "delete"={
 *          "security"="is_granted('ROLE_ADMIN')",
 *          "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"},
 *      }},
 *     normalizationContext={"groups"={"review:read"}},
 *     denormalizationContext={"groups"={"review:write"}},
 * )
 * @UniqueEntity(fields={"id"})
 *  @ApiFilter(SearchFilter::class, properties={ "author": "exact","itemReviewed": "exact","organization": "exact","tags": "exact"})
 */
class Review extends AbstractDate
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @Assert\Uuid
     * @Groups({"review:read","review:items:post","person:read"})
     */
    private $id;

    /**
     * @var User|null The author of this content or rating. Please note that author is special in that HTML 5 provides a special mechanism for indicating authorship via the rel tag. That is equivalent to this and may be used interchangeably.
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="reviewsGiven")
     * @ApiProperty(iri="http://schema.org/author",readableLink=false, writableLink=false)
     * @Groups({"review:read","review:items:post","person:read"})
     */
    private $author;

    /**
     * @var User|null the item (person) that is being reviewed/rated
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="reviews")
     * @ApiProperty(iri="http://schema.org/itemReviewed",readableLink=false, writableLink=false)
     * @Groups({"review:read","review:items:post","person:read"})
     * @ORM\JoinColumn(nullable=false)
     *
     */
    private $itemReviewed;

    /**
     * @var string|null the actual body of the review
     *
     * @ORM\Column(type="text", nullable=true)
     * @ApiProperty(iri="http://schema.org/reviewBody")
     * @Groups({"review:read","review:write","person:read"})
     */
    private $reviewBody;

    /**
     * @var int|null The rating given in this review. Note that reviews can themselves be rated. The ```reviewRating``` applies to rating given by the review. The \[\[aggregateRating\]\] property applies to the review itself, as a creative work.
     *
     * @ORM\Column(type="integer", nullable=true)
     * @ApiProperty(iri="http://schema.org/reviewRating")
     * @Groups({"review:read","review:write","person:read"})
     * @Assert\Range(min=1,max=5)
     */
    private $reviewRating;

    /**
     * @var Organization|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Organization")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"review:read","review:items:post","person:read"})
     * @ApiProperty(iri="http://schema.org/Organization",readableLink=false, writableLink=false)
     */
    private $organization;

    /**
     * @var Collection<Tags>|null
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Tags")
     * @Groups({"review:read","review:write","person:read"})
     */
    private $tags;

    /**
     * @var bool|null
     *
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"review:read","review:items:post","person:read"})
     */
    private $isSubmitted;



    public function __construct()
    {
        $this->tags = new ArrayCollection();
        $this->isSubmitted = false;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return User|null
     */
    public function getAuthor(): ?User
    {
        return $this->author;
    }

    /**
     * @param User|null $author
     * @return Review
     */
    public function setAuthor(?User $author): Review
    {
        $this->author = $author;
        return $this;
    }

    public function getItemReviewed(): ?User
    {
        return $this->itemReviewed;
    }

    public function setItemReviewed(?User $itemReviewed): self
    {
        $this->itemReviewed = $itemReviewed;

        return $this;
    }

    public function setReviewBody(?string $reviewBody): void
    {
        $this->reviewBody = $reviewBody;
    }

    public function getReviewBody(): ?string
    {
        return $this->reviewBody;
    }

    public function setReviewRating(?int $reviewRating): void
    {
        $this->reviewRating = $reviewRating;
    }

    public function getReviewRating(): ?int
    {
        return $this->reviewRating;
    }

    /**
     * @param Organization|null $organization
     */
    public function setOrganization($organization): void
    {
        $this->organization = $organization;
    }

    /**
     * @return Organization|null
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param Tags $tag
     */
    public function addTag($tag): void
    {
        $this->tags[] = $tag;
    }

    /**
     * @param Tags $tag
     */
    public function removeTag($tag): void
    {
        $this->tags->removeElement($tag);
    }

    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function setIsSubmitted(?bool $isSubmitted)
    {
        $this->isSubmitted = $isSubmitted;
        return $this;
    }

    public function getIsSubmitted(): ?bool
    {
        return $this->isSubmitted;
    }


}
