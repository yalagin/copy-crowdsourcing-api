<?php


namespace App\Entity\Translation;


use Doctrine\ORM\Mapping as ORM;
use Gedmo\Translatable\Entity\MappedSuperclass\AbstractTranslation;

/**
 * @ORM\Table(name="lesson_question_translations", indexes={
 *      @ORM\Index(name="lesson_question_translation_idx", columns={"locale", "object_class", "field", "foreign_key"})
 * })
 * @ORM\Entity(repositoryClass="Gedmo\Translatable\Entity\Repository\TranslationRepository")
 */
class LessonQuestionTranslation extends AbstractTranslation
{
    /**
     * All required columns are mapped through inherited superclass
     */
}
