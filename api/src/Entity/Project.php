<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\ExistsFilter;
use ApiPlatform\Core\Annotation\ApiFilter;


/**
 * Project
 *
 * @see http://schema.org/Project Documentation on Schema.org
 *
 * @author Maxim Yalagin <yalagin@gmail.com>
 *
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRepository")
 * @ApiResource(
 *     iri="http://schema.org/Project",
 *     security="is_granted('ROLE_ADMIN')",
 *     collectionOperations={
 *          "get"={
 *              "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *          },
 *          "post"={
 *              "denormalization_context"={"groups"={"project:write","project:items:create"}},
 *          },
 *     },
 *     itemOperations={
 *           "get"={
 *              "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *          },
 *          "put",
 *          "patch",
 *          "delete"
 *     },
 *     normalizationContext={"groups"={"project:read"}},
 *     denormalizationContext={"groups"={"project:write"}}
 * )
 * @UniqueEntity(fields={"id"})
 * @ApiFilter(SearchFilter::class, properties={ "tags.value": "exact","tags": "exact","educationalLevel": "exact","organizations": "exact"})
 * @ApiFilter(DateFilter::class, properties={"startTime","endTime"})
 * @ApiFilter(BooleanFilter::class, properties={"organizations.isOpenForJoin","organizations.isLocked"})
 * @ApiFilter(ExistsFilter::class, properties={"organizations"})
 */
class Project extends AbstractDate
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @Assert\Uuid
     * @Groups({"project:read","project:items:create","lesson:read","lesson:items:read"})
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"project:read","project:write","lesson:read","lesson:items:read"})
     */
    private $title;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"project:read","project:write","lesson:read","lesson:items:read"})
     */
    private $objective;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"project:read","project:write","lesson:read","lesson:items:read"})
     */
    private $activity;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"project:read","project:write","lesson:read","lesson:items:read"})
     */
    private $assessment;

    /**
     * @var Collection<Tags>|null  subjects
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Tags")
     * @Groups({"project:read","project:write","lesson:read","lesson:items:read"})
     */
    private $tags;

    /**
     * @var EducationalLevel|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\EducationalLevel")
     * @Groups({"project:read","project:write","lesson:read","lesson:items:read"})
     */
    private $educationalLevel;

    /**
     * @var \DateTimeInterface|null
     *
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\Type("\DateTimeInterface")
     * @Groups({"project:read","project:write"})
     */
    private $startTime;

    /**
     * @var \DateTimeInterface|null
     *
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\Type("\DateTimeInterface")
     * @Groups({"project:read","project:write","lesson:read","lesson:items:read"})
     */
    private $endTime;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Groups({"project:read","project:write","lesson:read","lesson:items:read"})
     */
    private $minimumNumberOfQuestions;

    /**
     * @var Collection<CreativeWork>|null
     * @ORM\OneToMany(targetEntity=CreativeWork::class, mappedBy="createdFor", orphanRemoval=true)
     * @Groups({"project:read"})
     * @ApiProperty(readableLink=false, writableLink=false)
     */
    private $creativeWorks;

    /**
     * @var Collection<Organization>|null
     *
     * @ORM\OneToMany(targetEntity=Organization::class, mappedBy="project")
     * @Groups({"project:read","admin:write","project:write"})
     * @ApiProperty(readableLink=false, writableLink=false)
     */
    private $organizations;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean",nullable=true)
     * @Groups({"project:read","admin:write","project:write"})
     */
    private $isDraft;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"project:read","admin:write","project:write","lesson:read"})
     */
    private $material;

    /**
     * @var Subtheme|null
     *
     * @ORM\ManyToOne(targetEntity=Subtheme::class, inversedBy="projects")
     * @Groups({"project:read","admin:write","project:write","lesson:read","lesson:items:read"})
     *
     */
    private $subtheme;

    /**
     * @var Collection<Award>|null
     *
     * @ORM\OneToMany(targetEntity=Award::class, mappedBy="project")
     * @Groups({"project:read","admin:write","project:write"})
     */
    private $awards;

    /**
     * @ORM\Column(type="boolean", options={"default":"0"})
     * @Groups({"project:read","admin:write","project:write","lesson:read"})
     */
    private $internal;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
        $this->minimumNumberOfQuestions = 15;
        $this->creativeWorks = new ArrayCollection();
        $this->isDraft = true;
        $this->internal = false;
        $this->organizations = new ArrayCollection();
        $this->awards = new ArrayCollection();
    }

    public function setStartTime(?\DateTimeInterface $startTime): void
    {
        $this->startTime = $startTime;
    }

    public function getStartTime(): ?\DateTimeInterface
    {
        return $this->startTime;
    }

    public function setEndTime(?\DateTimeInterface $endTime): void
    {
        $this->endTime = $endTime;
    }

    public function getEndTime(): ?\DateTimeInterface
    {
        return $this->endTime;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): Project
    {
        $this->title = $title;
        return $this;
    }

    public function setObjective(?string $objective): void
    {
        $this->objective = $objective;
    }

    public function getObjective(): ?string
    {
        return $this->objective;
    }

    public function setActivity(?string $activity): void
    {
        $this->activity = $activity;
    }

    public function getActivity(): ?string
    {
        return $this->activity;
    }

    public function setAssessment(?string $assessment): void
    {
        $this->assessment = $assessment;
    }

    public function getAssessment(): ?string
    {
        return $this->assessment;
    }

    /**
     * @param Tags $tag
     */
    public function addTag($tag): void
    {
        $this->tags[] = $tag;
    }

    /**
     * @param Tags $tag
     */
    public function removeTag($tag): void
    {
        $this->tags->removeElement($tag);
    }

    public function getTags(): Collection
    {
        return $this->tags;
    }

    /**
     * @return educationalLevel|null
     */
    public function getEducationalLevel(): ?educationalLevel
    {
        return $this->educationalLevel;
    }

    /**
     * @param educationalLevel|null $educationalLevel
     * @return Project
     */
    public function setEducationalLevel(?educationalLevel $educationalLevel): Project
    {
        $this->educationalLevel = $educationalLevel;
        return $this;
    }

    public function getMinimumNumberOfQuestions(): ?int
    {
        return $this->minimumNumberOfQuestions;
    }

    public function setMinimumNumberOfQuestions(int $minimumNumberOfQuestions): self
    {
        $this->minimumNumberOfQuestions = $minimumNumberOfQuestions;

        return $this;
    }

    /**
     * @return Collection|CreativeWork[]
     */
    public function getCreativeWorks(): Collection
    {
        return $this->creativeWorks;
    }

    public function addCreativeWork(CreativeWork $creativeWork): self
    {
        if (!$this->creativeWorks->contains($creativeWork)) {
            $this->creativeWorks[] = $creativeWork;
            $creativeWork->setCreatedFor($this);
        }

        return $this;
    }

    public function removeCreativeWork(CreativeWork $creativeWork): self
    {
        if ($this->creativeWorks->contains($creativeWork)) {
            $this->creativeWorks->removeElement($creativeWork);
            // set the owning side to null (unless already changed)
            if ($creativeWork->getCreatedFor() === $this) {
                $creativeWork->setCreatedFor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Organization[]
     */
    public function getOrganizations(): Collection
    {
        return $this->organizations;
    }

    public function addOrganization(Organization $organization): self
    {
        if (!$this->organizations->contains($organization)) {
            $this->organizations[] = $organization;
            $organization->setProject($this);
        }

        return $this;
    }

    public function removeOrganization(Organization $organization): self
    {
        if ($this->organizations->contains($organization)) {
            $this->organizations->removeElement($organization);
            // set the owning side to null (unless already changed)
            if ($organization->getProject() === $this) {
                $organization->setProject(null);
            }
        }

        return $this;
    }

    /**
     * @Groups({"project:read"})
     */
    public function getTeamNeedPartnerCounter()
    {
        /** @var Organization $organization */
        return $this->getOrganizations()
            ->filter(fn($organization) => !$organization->isLocked() && $organization->isOpenForJoin())
            ->count();
    }

    public function getIsDraft(): ?bool
    {
        return $this->isDraft;
    }

    public function setIsDraft(bool $isDraft): self
    {
        $this->isDraft = $isDraft;

        return $this;
    }

    public function getMaterial(): ?string
    {
        return $this->material;
    }

    public function setMaterial(?string $material): self
    {
        $this->material = $material;

        return $this;
    }

    public function getSubtheme(): ?Subtheme
    {
        return $this->subtheme;
    }

    public function setSubtheme(?Subtheme $subtheme): self
    {
        $this->subtheme = $subtheme;

        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getAwards()
    {
        return $this->awards;
    }

//    /**
//     * @param Collection|null $awards
//     */
//    public function setAwards($awards): void
//    {
//        $this->awards = $awards;
//    }

    public function addAward(Award $award): self
    {
        if (!$this->awards->contains($award)) {
            $this->awards[] = $award;
            $award->setProject($this);
        }

        return $this;
    }

    public function removeAward(Award $award): self
    {
        if ($this->awards->contains($award)) {
            $this->awards->removeElement($award);
            // set the owning side to null (unless already changed)
            if ($award->getProject() === $this) {
                $award->setProject(null);
            }
        }

        return $this;
    }

    public function getInternal(): ?bool
    {
        return $this->internal;
    }

    public function setInternal(bool $internal): self
    {
        $this->internal = $internal;

        return $this;
    }
}
