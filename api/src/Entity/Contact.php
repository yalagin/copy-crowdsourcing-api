<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * A contact point—for example, a whats up email or telephone.
 *
 * @see http://schema.org/ContactPoint Documentation on Schema.org
 *
 * @author Maxim Yalagin <yalagin@gmail.com>
 *
 * @ORM\Entity
 * @ApiResource(
 *     iri="http://schema.org/ContactPoint",
 *     collectionOperations={
 *          "get"= {
 *              "security"="is_granted('ROLE_USER')",
 *          },
 *          "post"={
 *              "security"="is_granted('ROLE_USER')",
 *              "validation_groups"={"Default", "create"},
 *              "method"="POST",
 *              "denormalization_context"={"groups"={"contact:write","contact:items:create"}},
 *          },
 *     },
 *     itemOperations={
 *          "get"= {"security"="is_granted('ROLE_USER')",
 *          "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"}},
 *          "patch"={
 *              "security"="is_granted('ROLE_USER') and object.user == user or is_granted('ROLE_ADMIN')",
 *              "denormalization_context"={"groups"={"contact:item:change"},
 *               "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"}},
 *          },
 *          "put"={
 *              "security"="is_granted('ROLE_USER') and object.user == user or is_granted('ROLE_ADMIN')",
 *              "denormalization_context"={"groups"={"contact:item:change"},
 *               "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"}},
 *          },
 *          "delete"={
 *              "security"="is_granted('ROLE_ADMIN')",
 *              "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"},
 *           }
 *     },
 *      normalizationContext={"groups"={"contact:read"}},
 *      denormalizationContext={"groups"={"contact:write"},
 *     }
 * )
 *  @ApiFilter(SearchFilter::class, properties={"id": "exact", "user": "exact", "person": "exact"})
 * @UniqueEntity(fields={"id"})
 */
class Contact extends AbstractDate implements AuthoredEntityInterface
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @Assert\Uuid
     * @Assert\NotBlank()
     * @Groups({"contact:read", "contact:items:create"})
     */
    private $id;

    /**
     * @var string|null the name of the item
     *
     * @ORM\Column(type="text", nullable=true)
     * @ApiProperty(iri="http://schema.org/name")
     * @Groups({"contact:read", "contact:write","contact:item:change"})
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"contact:read", "contact:write","contact:item:change"})
     */
    private $value;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"contact:read","admin:write"})
     * @ApiProperty(readableLink=false, writableLink=false)
     */
    public User $user;

    /**
     * @var Person
     *
     * @ORM\ManyToOne(targetEntity=Person::class, inversedBy="contacts")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull
     * @Groups({"contact:read", "contact:write","contact:item:change"})
     */
    private Person $person;

    public function __construct()
    {
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setValue(?string $value): void
    {
        $this->value = $value;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Contact
     */
    public function setUser(User $user): Contact
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @param User $author
     * @return Contact
     */
    public function setAuthor(UserInterface $author): Contact
    {
        $this->setUser($author) ;
        return $this;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function setPerson(Person $person): self
    {
        $this->person = $person;

        return $this;
    }
}
