<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Validator\Constraints\CustomExpression;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * An answer offered to a question; perhaps correct, perhaps opinionated or wrong.
 *
 * @see http://schema.org/Answer Documentation on Schema.org
 *
 * @author Maxim Yalagin <yalagin@gmail.com>
 *
 * @ORM\Entity
 * @ApiResource(
 *     iri="http://schema.org/Answer",
 *     security="is_granted('ROLE_USER')",
 *     collectionOperations={
 *          "get",
 *          "post"={
 *              "denormalization_context"={"groups"={"answer:items:post"}},
 *          }
 *     },
 *      itemOperations={
 *           "get",
 *           "put"={
 *              "security"="user in object.getCreator().getMembers().toArray() or is_granted('ROLE_ADMIN')",
 *              "denormalization_context"={"groups"={"answer:items:change"}},
 *          },
 *          "patch"={
 *              "security"="user in object.getCreator().getMembers().toArray() or is_granted('ROLE_ADMIN')",
 *              "denormalization_context"={"groups"={"answer:items:change"}},
 *          },
 *          "delete"={
 *              "security"="user in object.getCreator().getMembers().toArray() or is_granted('ROLE_ADMIN')",
 *              "denormalization_context"={"groups"={"answer:items:change"}},
 *          }
 *     },
 *     normalizationContext={"groups"={"answer:read"}}
 * )
 * @UniqueEntity(fields={"id"})
 */
class Answer extends AbstractDate
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @Assert\Uuid
     * @Assert\NotBlank
     * @Groups({"answer:items:post","answer:read"})
     */
    private $id;

    /**
     * @var Organization The creator/author of this CreativeWork. This is the same as the Author property for CreativeWork.
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Organization")
     * @ApiProperty(iri="http://schema.org/creator",readableLink=false, writableLink=false)
     * @Assert\NotBlank
     * @Groups({"answer:items:post","answer:read"})
     * @CustomExpression(
     *     "user in this.getCreator().getMembers().toArray()",
     *     message="You don't belong in that organization",
     * )
     */
    private $creator;

    /**
     * @var Question A question or the parent of answer or item in general
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Question", inversedBy="connectedAnswers")
     * @ORM\JoinColumn(nullable=false)
     * @ApiProperty(iri="http://schema.org/parentItem",readableLink=false, writableLink=false)
     * @Assert\NotBlank
     * @Groups({"answer:items:post","answer:read"})
     * @Assert\Expression(
     *     "this.getCreator() == this.getParentItem().getCreator()",
     *     message="Your organization don't match",
     * )
     */
    private $parentItem;

    /**
     * @var string|null a description of the item
     *
     * @ORM\Column(type="text", nullable=true)
     * @ApiProperty(iri="http://schema.org/description")
     * @Groups({"answer:items:post","answer:read","answer:items:change"})
     */
    private $description;

    /**
     * @var bool|null
     *
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"answer:items:post","answer:read","answer:items:change"})
     */
    private $correct;

    /**
     * @var integer|null this is for convenient sorting
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"answer:items:post","answer:read","answer:items:change"})
     */
    private $ordering;

    /**
     * Answer constructor.
     */
    public function __construct()
    {
        $this->setCorrect(false);
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setCreator(Organization $creator): void
    {
        $this->creator = $creator;
    }

    public function getCreator(): Organization
    {
        return $this->creator;
    }

    public function setParentItem(Question $parentItem): void
    {
        $this->parentItem = $parentItem;
    }

    public function getParentItem(): Question
    {
        return $this->parentItem;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setCorrect(?bool $correct): void
    {
        $this->correct = $correct;
    }

    public function getCorrect(): ?bool
    {
        return $this->correct;
    }

    /**
     * @return int|null
     */
    public function getOrdering(): ?int
    {
        return $this->ordering;
    }

    /**
     * @param int|null $ordering
     * @return Answer
     */
    public function setOrdering(?int $ordering): Answer
    {
        $this->ordering = $ordering;
        return $this;
    }
}
