<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use App\Controller\CreateMediaObjectAction;
use App\Controller\UpdateMediaObjectAction;
use App\Controller\GetUploadingLinkAction;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * A media object, such as an image, video, or audio object embedded in a web page or a downloadable dataset i.e. DataDownload. Note that a creative work may have many media objects associated with it on the same web page. For example, a page about a single song (MusicRecording) may have a music video (VideoObject), and a high and low bandwidth audio stream (2 AudioObject's).
 *
 * @see http://schema.org/MediaObject Documentation on Schema.org
 *
 * @author Maxim Yalagin <yalagin@gmail.com>
 *
 * @ApiResource(
 *     iri="http://schema.org/MediaObject",
 *     deprecationReason="Use Image object instead",
 *     security="is_granted('ROLE_USER')",
 *     normalizationContext={
 *         "groups"={"media_object_read"},
 *     },
 *     collectionOperations={
 *          "post-get-aws-link"={
 *              "method"="POST",
 *              "path"="media_objects/give-me-put-link",
 *              "denormalization_context"={"groups"={"media_object:items:get_link"}},
 *              "normalization_context"={"groups"={"media_object:items:get_link:read"}},
 *              "controller"=GetUploadingLinkAction::class,
 *              "validation_groups"={"media_object:items:get_link"},
 *              "openapi_context"={"summary" = "Gets you aws uploading link"}
 *          },
 *         "post"={
 *             "controller"=CreateMediaObjectAction::class,
 *             "denormalization_context"={"groups"={"media_object:items:create"}},
 *             "validation_groups"={"Default","media_object:items:create"},
 *              "deserialize"=false,
 *         },
 *         "get"={
 *              "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *          },
 *     },
 *     itemOperations={
 *         "get"={
 *              "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *          },
 *          "patch"={
 *              "security"="user === object.getAuthor() or is_granted('ROLE_ADMIN')",
 *              "controller"=UpdateMediaObjectAction::class,
 *              "method"="PATCH",
 *          },
 *          "delete"={
 *              "security"="is_granted('ROLE_ADMIN')",
 *              "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"},
 *           }
 *     },
 *     normalizationContext={"groups"={"media_object:read"}},
 *     denormalizationContext={"groups"={"media_object:write"}},
 * )
 * @UniqueEntity(fields={"id"})
 * @ORM\Entity
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({
 *     "media" = "MediaObject",
 *     "image" = "ImageObject",
 *     "document" = "DocumentObject",
 *     "video" = "VideoObject",
 *     "audio" = "AudioObject",
 *     "animation" = "AnimationObject",
 *     "archive" = "ArchiveObject"
 * })
 * @Gedmo\TranslationEntity(class="App\Entity\Translation\MediaObjectTranslation")
 */
class MediaObject extends AbstractDate implements AuthoredEntityInterface
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @Assert\Uuid
     * @ORM\GeneratedValue(strategy="UUID")
     * @Groups({"media_object:read","media_object:items:create","lesson:read","lesson:items:read", "lesson_question:items:read", "lesson_question:read"})
     */
    protected $id;

    /**
     * @var User|null The author of this content or rating. Please note that author is special in that HTML 5 provides a special mechanism for indicating authorship via the rel tag. That is equivalent to this and may be used interchangeably.
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ApiProperty(iri="http://schema.org/author",readableLink=false, writableLink=false)
     * @Groups({"media_object:read"})
     */
    private $author;

    /**
     * @var string|null actual bytes of the media object, for example the image file or video file
     *
     * @ApiProperty(iri="http://schema.org/contentUrl")
     * @Groups({"media_object:read","lesson:read","lesson:items:read", "lesson_question:items:read", "lesson_question:read"})
     * @Assert\Url
     */
    public $contentUrl;

    /**
     * @var string|null
     *
     * @Gedmo\Translatable
     * @ORM\Column(nullable=true)
     * @Groups({"media_object:read","lesson:read","lesson:read","lesson:items:read", "lesson_question:items:read", "lesson_question:read"})
     */
    public $filePath;

    /**
     * @var string|null the original name of the item
     *
     * @Gedmo\Translatable
     * @ORM\Column(type="text", nullable=true)
     * @ApiProperty(iri="http://schema.org/name")
     * @Assert\NotBlank(groups={"media_object:items:create","media_object:read","media_object:write"})
     * @Groups({"media_object:read","media_object:items:create","lesson:read","lesson:items:read", "lesson_question:items:read", "lesson_question:read"})
     */
    public $name;

    //link fields
    /**
     * @var string after the dot for example png jpg ai docx doc swg avi ...
     * @Groups({"media_object:items:get_link"})
     * @Assert\NotBlank(groups={"media_object:items:get_link"})
     */
    public $fileExtension;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @var string you will get url to aws s3. please use PUT method, After you upload the file please post the url back to save it in database /media_objects
     * @Groups({"media_object:items:get_link:read","media_object:items:create"})
     * @Assert\NotBlank(groups={"media_object:items:create","media_object:read","media_object:write"})
     * @Assert\Url(groups={"media_object:items:create","media_object:read","media_object:write"})
     *
     */
    public $url;

    /**
     * @var string
     *
     * @Gedmo\Locale
     */
    public $locale;

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setAuthor(?UserInterface $author): self
    {
        $this->author = $author;
        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setContentUrl(?string $contentUrl): void
    {
        $this->contentUrl = $contentUrl;
    }

    public function getContentUrl(): ?string
    {
        return $this->contentUrl;
    }

    /**
     * @return string
     */
    public function getFileExtension(): string
    {
        return $this->fileExtension;
    }

    /**
     * @param string $fileExtension
     * @return MediaObject
     */
    public function setFileExtension(string $fileExtension): MediaObject
    {
        $this->fileExtension = $fileExtension;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return MediaObject
     */
    public function setUrl(string $url): MediaObject
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFilePath(): ?string
    {
        return $this->filePath;
    }

    /**
     * @param string|null $filePath
     * @return MediaObject
     */
    public function setFilePath(?string $filePath): MediaObject
    {
        $this->filePath = $filePath;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return MediaObject
     */
    public function setName(?string $name): MediaObject
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Locale
     */
    public function getLocale(): Locale
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     * @return MediaObject
     */
    public function setLocale($locale): MediaObject
    {
        $this->locale = $locale;
        return $this;
    }


}
