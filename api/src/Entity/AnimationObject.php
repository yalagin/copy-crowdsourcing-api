<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Controller\CreateMediaObjectAction;
use App\Controller\UpdateMediaObjectAction;
use App\Controller\GetUploadingLinkAction;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * A animation file.
 *
 * @see http://schema.org/AnimationObject Documentation on Schema.org
 *
 * @author Maxim Yalagin <yalagin@gmail.com>
 *
 * @ORM\Entity
 * @ApiResource(iri="http://schema.org/AnimationObject",
 *     security="is_granted('ROLE_USER')",
 *     normalizationContext={
 *         "groups"={"media_object_read"},
 *     },
 *     collectionOperations={
 *          "post-get-aws-link"={
 *              "method"="POST",
 *              "path"="animation_objects/give-me-put-link",
 *              "denormalization_context"={"groups"={"media_object:items:get_link"}},
 *              "normalization_context"={"groups"={"media_object:items:get_link:read"}},
 *              "controller"=GetUploadingLinkAction::class,
 *              "validation_groups"={"media_object:items:get_link"},
 *              "openapi_context"={"summary" = "choices = mp4,avi, mkv. Gets you aws uploading link "}
 *          },
 *         "post"={
 *             "controller"=CreateMediaObjectAction::class,
 *             "denormalization_context"={"groups"={"media_object:items:create"}},
 *             "validation_groups"={"Default","media_object:items:create"},
 *         },
 *         "get"={
 *              "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *          },
 *     },
 *     itemOperations={
 *         "get"={
 *              "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *          },
 *          "patch"={
 *              "security"="user === object.getAuthor() or is_granted('ROLE_ADMIN')",
 *              "controller"=UpdateMediaObjectAction::class,
 *              "method"="PATCH",
 *          },
 *          "delete"={
 *              "security"="is_granted('ROLE_ADMIN')",
 *              "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"},
 *           }
 *     },
 *     normalizationContext={"groups"={"media_object:read"}},
 *     denormalizationContext={"groups"={"media_object:write"}},
 * ))
 */
class AnimationObject extends MediaObject
{
    /**
     * @var string after the dot for example gif ...
     * @Groups({"media_object:items:get_link"})
     * @Assert\Choice(
     *     choices = {"gif"},
     *     groups={"media_object:items:get_link"}
     *     )
     */
    public $fileExtension;

    /**
     * @return string
     */
    public function getFileExtension(): string
    {
        return $this->fileExtension;
    }

    /**
     * @param string $fileExtension
     * @return MediaObject
     */
    public function setFileExtension(string $fileExtension): MediaObject
    {
        $this->fileExtension = $fileExtension;
        return $this;
    }
}
