<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     collectionOperations={
 *          "get"={
 *          "openapi_context"={"summary" = "without get adminpanel get error "},
 *          "deprecation_reason"="without get adminpanel get error"
 *          },
 *         "post"={
 *             "path"="/users/confirm",
 *              "openapi_context"={"summary" = "Post confirmation token for enabling user"}
 *         }
 *     },
 *     itemOperations={},
 *     iri="http://schema.org/Thing",
 *     normalizationContext={"groups"={"user_confirmation:read"}},
 *     denormalizationContext={"groups"={"user_confirmation:write"}})
 */
class UserConfirmation
{

    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Length(min=30, max=30)
     * @Groups({"user_confirmation:read", "user_confirmation:write"})
     */
    public $confirmationToken;

    /**
     * @return mixed
     */
    public function getConfirmationToken()
    {
        return $this->confirmationToken;
    }

    /**
     * @param mixed $confirmationToken
     */
    public function setConfirmationToken($confirmationToken): void
    {
        $this->confirmationToken = $confirmationToken;
    }
}
