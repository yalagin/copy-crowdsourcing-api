<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use App\Controller\PublishLessonAction;
use Gedmo\Mapping\Annotation as Gedmo;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * The LearningResource type can be used to indicate CreativeWorks (whether physical or digital) that have a particular and explicit orientation towards learning, education, skill acquisition, and other educational purposes.
 *
 * @see http://schema.org/LearningResource Documentation on Schema.org
 *
 * @author Maxim Yalagin <yalagin@gmail.com>
 *
 * @ORM\Entity
 * @ApiResource(
 *     iri="http://schema.org/LearningResource",
 *     security="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *     collectionOperations={
 *          "get"={
 *               "normalization_context"={"groups"={"lesson:items:read"}},
 *          },
 *          "post"={
 *              "security"="is_granted('ROLE_ADMIN')",
 *              "denormalization_context"={"groups"={"lesson:items:post"}},
 *              "validation_groups"={"Deafult","lesson:items:post"},
 *          }
 *     },
 *      itemOperations={
 *           "get",
 *           "put"={
 *              "denormalization_context"={"groups"={"lesson:item:change"}},
 *              "security"="is_granted('ROLE_ADMIN')",
 *          },
 *          "patch"={
 *              "denormalization_context"={"groups"={"lesson:item:change"}},
 *               "security"="is_granted('ROLE_ADMIN')",
 *          },
 *          "delete"={
 *              "denormalization_context"={"groups"={"lesson:item:change"}},
 *              "security"="is_granted('ROLE_ADMIN')",
 *          },
 *          "put-submit-creative-work"={
 *             "method"="PUT",
 *             "security"="is_granted('ROLE_ADMIN')",
 *             "path"="/lessons/{id}/publish",
 *             "controller"=PublishLessonAction::class,
 *             "denormalization_context"={"groups"={"lesson:item:put-submit-creative-work:write"}},
 *             "validation_groups"={"lesson:item:put-submit-creative-work"},
 *             "openapi_context"={"summary" = "Publish lesson to Titik Pintar"}
 *         }
 *     },
 *      normalizationContext={"groups"={"lesson:read"}},
 * )
 * @ApiFilter(SearchFilter::class, properties={"creator": "exact", "createdFor": "exact"})
 * @UniqueEntity(fields={"id"})
 * @Gedmo\TranslationEntity(class="App\Entity\Translation\LessonTranslation")
 */
class Lesson extends AbstractDate
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @Assert\Uuid
     * @Groups({"lesson:items:post","lesson:read","lesson:items:read"})
     */
    private $id;

    /**
     * @var Organization|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Organization")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"lesson:items:post","lesson:read","lesson:items:read"})
     * @ApiProperty(iri="http://schema.org/creator")
     */
    private $creator;

    /**
     * @var Project|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Project")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"lesson:items:post","lesson:read","lesson:items:read"})
     */
    private $createdFor;

    /**
     * @var Collection<LessonQuestion>|null
     *
     * @ORM\OneToMany(targetEntity="App\Entity\LessonQuestion", mappedBy="isPartOf")
     * @ORM\JoinTable(inverseJoinColumns={@ORM\JoinColumn(nullable=false, unique=true)})
     * @Groups({"lesson:items:post","lesson:read","lesson:item:change"})
     * @ApiProperty(iri="http://schema.org/hasPart")
     * @ApiSubresource
     */
    private $hasParts;

    /**
     * @var CreativeWork|null original creative work
     *
     * @ORM\OneToOne(targetEntity="App\Entity\CreativeWork")
     * @ApiProperty(iri="http://schema.org/CreativeWork")
     * @Groups({"lesson:items:post","lesson:read"})
     */
    private $originalCreativeWork;

    /**
     * @var DocumentObject|null collection of DocumentObject
     * @ApiProperty(iri="http://schema.org/DocumentObject")
     * @ORM\ManyToOne(targetEntity=DocumentObject::class)
     * @Groups({"lesson:items:post","lesson:read","lesson:item:change","lesson:read","lesson:items:read"})
     */
    private $document;

    /**
     * @var ArchiveObject|null collection of ArchiveObject
     * @ApiProperty(iri="http://schema.org/ArchiveObject")
     * @ORM\ManyToOne(targetEntity=ArchiveObject::class)
     * @Groups({"lesson:items:post","lesson:read","lesson:item:change","lesson:read","lesson:items:read"})
     */
    private $archive;

    /**
     * @var VideoObject|null collection of VideoObject
     * @ApiProperty(iri="http://schema.org/VideoObject")
     * @ORM\ManyToOne(targetEntity=VideoObject::class)
     * @Groups({"lesson:items:post","lesson:read","lesson:item:change","lesson:read","lesson:items:read"})
     */
    private $video;

    /**
     * @var VideoObject|null collection of VideoObject
     * @ApiProperty(iri="http://schema.org/VideoObject")
     * @ORM\ManyToOne(targetEntity=VideoObject::class)
     * @Groups({"lesson:items:post","lesson:read","lesson:item:change","lesson:read","lesson:items:read"})
     */
    private $derivativeForYoutube;

    /**
     * @var VideoObject|null collection of VideoObject
     * @ApiProperty(iri="http://schema.org/VideoObject")
     * @ORM\ManyToOne(targetEntity=VideoObject::class)
     * @Groups({"lesson:items:post","lesson:read","lesson:item:change","lesson:read","lesson:items:read"})
     */
    private $derivativeForTitikPintar;

    /**
     * @var VideoObject|null collection of VideoObject
     * @ApiProperty(iri="http://schema.org/VideoObject")
     * @ORM\ManyToOne(targetEntity=VideoObject::class)
     * @Groups({"lesson:items:post","lesson:read","lesson:item:change","lesson:read","lesson:items:read"})
     */
    private $derivativeForSahabatPintar;

    /**
     * @var string|null Translatable a transcript of creative work in text
     *
     * @Gedmo\Translatable
     * @ORM\Column(type="text", nullable=true)
     * @ApiProperty(iri="http://schema.org/description")
     * @Groups({"lesson:items:post","lesson:read","lesson:item:change","lesson:items:read"})
     */
    private $transcript;


    /**
     * @var Collection<ImageObject>|null collection of media objects
     * @ApiProperty(iri="http://schema.org/ImageObject")
     * @ORM\ManyToMany(targetEntity=ImageObject::class)
     * @Groups({"lesson:items:post","lesson:read","lesson:item:change","lesson:read","lesson:items:read"})
     */
    private $images;

    /**
     * @var Collection<AudioObject>|null AudioObject
     * @ApiProperty(iri="http://schema.org/AudioObject")
     * @ORM\ManyToMany(targetEntity=AudioObject::class)
     * @Groups({"lesson:items:post","lesson:read","lesson:item:change","lesson:items:read"})
     */
    private $audio;

    /**
     * @var boolean is published to Titik Pintar platform
     *
     * @ORM\Column(type="boolean", options={"default":"0"})
     * @ApiProperty(iri="http://schema.org/published")
     * @Groups({"lesson:items:post","lesson:read","lesson:item:change","lesson:items:read"})
     */
    private $published;

    /**
     * @var \DateTimeInterface|null
     *
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\Type("\DateTimeInterface")
     * @Groups({"lesson:read","lesson:item:change","lesson:items:read"})
     */
    private $publishedTime;

    /**
     * @ORM\Column(type="boolean", options={"default":"0"})
     */
    private $internal;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":"0"})
     * @Groups({"lesson:items:post","lesson:read","lesson:item:change","lesson:items:read"})
     */
    private $exported;

    /**
     * @var Locale
     *
     * @Gedmo\Locale
     */
    private $locale;

    public function __construct()
    {
        $this->hasParts = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->audio = new ArrayCollection();
        $this->published = false;
        $this->internal = false;
        $this->exported = false;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param Organization|null $creator
     */
    public function setCreator(?Organization $creator): void
    {
        $this->creator = $creator;
    }

    /**
     * @return Organization|null
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @param Project|null $createdFor
     */
    public function setCreatedFor(?Project $createdFor): void
    {
        $this->createdFor = $createdFor;
    }

    /**
     * @return Project|null
     */
    public function getCreatedFor()
    {
        return $this->createdFor;
    }

    public function addHasPart(LessonQuestion $hasPart): void
    {
        $this->hasParts[] = $hasPart;
    }

    public function removeHasPart(LessonQuestion $hasPart): void
    {
        $this->hasParts->removeElement($hasPart);
    }

    public function getHasParts(): Collection
    {
        return $this->hasParts;
    }

    /**
     * @param CreativeWork|null $originalCreativeWork
     */
    public function setOriginalCreativeWork(?CreativeWork $originalCreativeWork): void
    {
        $this->originalCreativeWork = $originalCreativeWork;
    }

    /**
     * @return CreativeWork|null
     */
    public function getOriginalCreativeWork()
    {
        return $this->originalCreativeWork;
    }

    /**
     * @return DocumentObject|null
     */
    public function getDocument(): ?DocumentObject
    {
        return $this->document;
    }

    /**
     * @param DocumentObject|null $document
     * @return Lesson
     */
    public function setDocument(?DocumentObject $document): Lesson
    {
        $this->document = $document;
        return $this;
    }

    /**
     * @return ArchiveObject|null
     */
    public function getArchive(): ?ArchiveObject
    {
        return $this->archive;
    }

    /**
     * @param ArchiveObject|null $archive
     * @return Lesson
     */
    public function setArchive(?ArchiveObject $archive): Lesson
    {
        $this->archive = $archive;
        return $this;
    }

    /**
     * @return VideoObject|null
     */
    public function getVideo(): ?VideoObject
    {
        return $this->video;
    }

    /**
     * @param VideoObject|null $video
     * @return Lesson
     */
    public function setVideo(?VideoObject $video): Lesson
    {
        $this->video = $video;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTranscript(): ?string
    {
        return $this->transcript;
    }

    /**
     * @param string|null $transcript
     * @return Lesson
     */
    public function setTranscript(?string $transcript): Lesson
    {
        $this->transcript = $transcript;
        return $this;
    }

    /**
     * @return bool
     */
    public function isPublished(): bool
    {
        return $this->published;
    }

    /**
     * @param bool $published
     * @return Lesson
     */
    public function setPublished(bool $published): Lesson
    {
        $this->published = $published;
        return $this;
    }

    /**
     * @return Locale
     */
    public function getLocale(): Locale
    {
        return $this->locale;
    }

    /**
     * @param Locale $locale
     * @return Lesson
     */
    public function setLocale(Locale $locale): Lesson
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getPublishedTime(): ?\DateTimeInterface
    {
        return $this->publishedTime;
    }

    /**
     * @param \DateTimeInterface|null $publishedTime
     * @return Lesson
     */
    public function setPublishedTime(?\DateTimeInterface $publishedTime): Lesson
    {
        $this->publishedTime = $publishedTime;
        return $this;
    }


    /**
     * @return Collection|ImageObject []
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(ImageObject $mediaObject): self
    {
        if (!$this->images->contains($mediaObject)) {
            $this->images[] = $mediaObject;
        }

        return $this;
    }

    public function removeImage(ImageObject $mediaObject): self
    {
        if ($this->images->contains($mediaObject)) {
            $this->images->removeElement($mediaObject);
        }

        return $this;
    }


    public function getAudio()
    {
        return $this->audio;
    }

    public function addAudio(AudioObject $mediaObject): self
    {
        if (!$this->audio->contains($mediaObject)) {
            $this->audio[] = $mediaObject;
        }

        return $this;
    }

    public function removeAudio(AudioObject $mediaObject): self
    {
        if ($this->audio->contains($mediaObject)) {
            $this->audio->removeElement($mediaObject);
        }

        return $this;
    }

    public function getInternal(): ?bool
    {
        return $this->internal;
    }

    public function setInternal(bool $internal): self
    {
        $this->internal = $internal;

        return $this;
    }

    public function getExported(): ?bool
    {
        return $this->exported;
    }

    public function setExported(bool $exported): self
    {
        $this->exported = $exported;

        return $this;
    }

    /**
     * @return VideoObject|null
     */
    public function getDerivativeForYoutube(): ?VideoObject
    {
        return $this->derivativeForYoutube;
    }

    /**
     * @param VideoObject|null $derivativeForYoutube
     * @return Lesson
     */
    public function setDerivativeForYoutube(?VideoObject $derivativeForYoutube): Lesson
    {
        $this->derivativeForYoutube = $derivativeForYoutube;
        return $this;
    }

    /**
     * @return VideoObject|null
     */
    public function getDerivativeForTitikPintar(): ?VideoObject
    {
        return $this->derivativeForTitikPintar;
    }

    /**
     * @param VideoObject|null $derivativeForTitikPintar
     * @return Lesson
     */
    public function setDerivativeForTitikPintar(?VideoObject $derivativeForTitikPintar): Lesson
    {
        $this->derivativeForTitikPintar = $derivativeForTitikPintar;
        return $this;
    }

    /**
     * @return VideoObject|null
     */
    public function getDerivativeForSahabatPintar(): ?VideoObject
    {
        return $this->derivativeForSahabatPintar;
    }

    /**
     * @param VideoObject|null $derivativeForSahabatPintar
     * @return Lesson
     */
    public function setDerivativeForSahabatPintar(?VideoObject $derivativeForSahabatPintar): Lesson
    {
        $this->derivativeForSahabatPintar = $derivativeForSahabatPintar;
        return $this;
    }

}
