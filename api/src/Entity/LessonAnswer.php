<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * An answer offered to a question; perhaps correct, perhaps opinionated or wrong.
 *
 * @see http://schema.org/Answer Documentation on Schema.org
 *
 * @author Maxim Yalagin <yalagin@gmail.com>
 *
 * @ORM\Entity
 * @ApiResource(
 *     iri="http://schema.org/Answer",
 *     security="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *     collectionOperations={
 *          "get",
 *          "post"={
 *              "denormalization_context"={"groups"={"lesson_answer:items:post"}},
 *              "security"="is_granted('ROLE_ADMIN')",
 *
 *          }
 *     },
 *      itemOperations={
 *           "get",
 *           "put"={
 *              "denormalization_context"={"groups"={"lesson_answer:items:change"}},
 *              "security"="is_granted('ROLE_ADMIN')",
 *          },
 *          "patch"={
 *              "denormalization_context"={"groups"={"lesson_answer:items:change"}},
 *              "security"="is_granted('ROLE_ADMIN')",
 *          },
 *          "delete"={
 *              "denormalization_context"={"groups"={"lesson_answer:items:change"}},
 *              "security"="is_granted('ROLE_ADMIN')",
 *          }
 *     },
 *     normalizationContext={"groups"={"lesson_answer:read"}}
 * )
 * @UniqueEntity(fields={"id"})
 * @Gedmo\TranslationEntity(class="App\Entity\Translation\LessonAnswerTranslation")
 */
class LessonAnswer extends AbstractDate
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @Assert\Uuid
     * @Assert\NotBlank
     * @Groups({"lesson_answer:items:post","lesson_answer:read","lesson:read","lesson_question:read"})
     */
    private $id;


    /**
     * @var LessonQuestion A question or the parent of answer or item in general
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\LessonQuestion", inversedBy="connectedAnswers")
     * @ORM\JoinColumn(nullable=false)
     * @ApiProperty(iri="http://schema.org/parentItem",readableLink=false, writableLink=false)
     * @Groups({"lesson_answer:items:post","lesson_answer:read"})
     */
    private $parentItem;

    /**
     * @var string|null Translatable a description of the item
     *
     * @Gedmo\Translatable
     * @ORM\Column(type="text")
     * @ApiProperty(iri="http://schema.org/description")
     * @Assert\NotNull
     * @Groups({"lesson_answer:items:post","lesson_answer:read","lesson_answer:items:change","lesson:read","lesson_question:read"})
     */
    private $description;

    /**
     * @var bool|null
     *
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"lesson_answer:items:post","lesson_answer:read","lesson_answer:items:change","lesson:read","lesson_question:read"})
     */
    private $correct;

    /**
     * @var integer|null this is for convenient sorting
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"lesson_answer:items:post","lesson_answer:read","lesson_answer:items:change","lesson:read","lesson_question:read"})
     */
    private $ordering;

    /**
     *  @var ?Answer  original Answer
     *
     * @ORM\OneToOne(targetEntity=Answer::class, cascade={"persist", "remove"})
     * @Groups({"lesson_answer:items:post","lesson_answer:read","lesson_answer:items:change"})
     * @ApiProperty(iri="http://schema.org/Answer",readableLink=false, writableLink=false)
     */
    private $originalAnswer;

    /**
     * @var Locale
     *
     * @Gedmo\Locale
     */
    private $locale;

    public function setId(string $id): LessonAnswer
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param LessonQuestion $parentItem
     */
    public function setParentItem($parentItem): LessonAnswer
    {
        $this->parentItem = $parentItem;
        return $this;
    }

    /**
     * @return LessonQuestion
     */
    public function getParentItem()
    {
        return $this->parentItem;
    }

    public function setDescription(?string $description): LessonAnswer
    {
        $this->description = $description;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setCorrect(?bool $correct): LessonAnswer
    {
        $this->correct = $correct;
        return $this;
    }

    public function getCorrect(): ?bool
    {
        return $this->correct;
    }

    public function getOrdering(): ?int
    {
        return $this->ordering;
    }

    public function setOrdering(?int $ordering): LessonAnswer
    {
        $this->ordering = $ordering;
        return $this;
    }

    public function getOriginalAnswer(): ?Answer
    {
        return $this->originalAnswer;
    }

    public function setOriginalAnswer(?Answer $originalAnswer): self
    {
        $this->originalAnswer = $originalAnswer;

        return $this;
    }

    /**
     * @return Locale
     */
    public function getLocale(): Locale
    {
        return $this->locale;
    }

    /**
     * @param Locale $locale
     * @return LessonAnswer
     */
    public function setLocale(Locale $locale): LessonAnswer
    {
        $this->locale = $locale;
        return $this;
    }
}
