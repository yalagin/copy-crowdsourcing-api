<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Controller\CreateMediaObjectAction;
use App\Controller\UpdateMediaObjectAction;
use App\Controller\GetUploadingLinkAction;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * A video file.
 *
 * @see http://schema.org/VideoObject Documentation on Schema.org
 *
 * @author Maxim Yalagin <yalagin@gmail.com>
 *
 * @ORM\Entity
 * @ApiResource(iri="http://schema.org/VideoObject",
 *     security="is_granted('ROLE_USER')",
 *     normalizationContext={
 *         "groups"={"media_object_read"},
 *     },
 *     collectionOperations={
 *          "post-get-aws-link"={
 *              "method"="POST",
 *              "path"="video_objects/give-me-put-link",
 *              "denormalization_context"={"groups"={"media_object:items:get_link"}},
 *              "normalization_context"={"groups"={"media_object:items:get_link:read"}},
 *              "controller"=GetUploadingLinkAction::class,
 *              "validation_groups"={"media_object:items:get_link"},
 *              "openapi_context"={"summary" = "choices = mp4,avi, mkv. Gets you aws uploading link "}
 *          },
 *         "post"={
 *             "controller"=CreateMediaObjectAction::class,
 *             "denormalization_context"={"groups"={"media_object:items:create"}},
 *             "validation_groups"={"Default","media_object:items:create"},
 *              "openapi_context"={"summary" = "create resource, additioals fields are image, animation"}
 *         },
 *         "get"={
 *              "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *          },
 *     },
 *     itemOperations={
 *         "patch"={
 *              "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"},
 *              "security"="user === object.getAuthor() or is_granted('ROLE_ADMIN')",
 *              "controller"=UpdateMediaObjectAction::class,
 *         },
 *         "put"={
 *              "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"},
 *              "denormalization_context"={"groups"={"media_object:items:create"}},
 *              "validation_groups"={"Default","media_object:items:create"},
 *         },
 *         "get"={
 *              "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *          },
 *         "delete"={
 *              "security"="is_granted('ROLE_ADMIN')",
 *              "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"},
 *           }
 *     },
 *     normalizationContext={"groups"={"media_object:read"}},
 *     denormalizationContext={"groups"={"media_object:write"}},
 * ))
 */
class VideoObject extends MediaObject
{
    /**
     * @var string after the dot for example png jpg ai docx doc swg avi ...
     * @Groups({"media_object:items:get_link"})
     * @Assert\Choice(
     *     choices = {"mp4","avi","mkv"},
     *     groups={"media_object:items:get_link"}
     *     )
     */
    public $fileExtension;

    /**
     * @var ImageObject|null An image of the item. This can be a \[\[URL\]\] or a fully described \[\[ImageObject\]\].
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\ImageObject")
     * @ApiProperty(iri="http://schema.org/ImageObject",readableLink=false, writableLink=false)
     * @Groups({"media_object:items:create","media_object:read","media_object:items:change","lesson:read","lesson:items:read"})
     * @ApiSubresource
     */
    private $image;

    /**
     * @var AnimationObject|null An AnimationObject
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\AnimationObject")
     * @ApiProperty(iri="http://schema.org/AnimationObject",readableLink=false, writableLink=false)
     * @Groups({"media_object:items:create","media_object:read","media_object:items:change","lesson:read","lesson:items:read"})
     * @ApiSubresource
     */
    private $animation;

    /**
     * @var integer|null The duration (in sec) of a movie
     *
     * @ORM\Column(type="integer")
     * @ApiProperty(iri="http://schema.org/duration")
     * @Groups({"media_object:items:create","media_object:read","media_object:items:change","lesson:read","lesson:items:read"})
     */
    private $duration;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @var string|null type of video rendered_source derivative_for_youtube  derivative_for_titik_pintar derivative_for_sahabat_pintar
     * @Groups({"media_object:items:get_link:read","media_object:items:create"})
     *
     */
    public $type;

    /**
     * @return string
     */
    public function getFileExtension(): string
    {
        return $this->fileExtension;
    }

    /**
     * @param string $fileExtension
     * @return MediaObject
     */
    public function setFileExtension(string $fileExtension): MediaObject
    {
        $this->fileExtension = $fileExtension;
        return $this;
    }

    /**
     * @return ImageObject|null
     */
    public function getImage(): ?ImageObject
    {
        return $this->image;
    }

    /**
     * @param ImageObject|null $image
     * @return VideoObject
     */
    public function setImage(?ImageObject $image): VideoObject
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return AnimationObject|null
     */
    public function getAnimation(): ?AnimationObject
    {
        return $this->animation;
    }

    /**
     * @param AnimationObject|null $animation
     * @return VideoObject
     */
    public function setAnimation(?AnimationObject $animation): VideoObject
    {
        $this->animation = $animation;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getDuration(): ?int
    {
        return $this->duration;
    }

    /**
     * @param int|null $duration
     * @return VideoObject
     */
    public function setDuration(?int $duration): VideoObject
    {
        $this->duration = $duration;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     * @return VideoObject
     */
    public function setType(?string $type): VideoObject
    {
        $this->type = $type;
        return $this;
    }
}
