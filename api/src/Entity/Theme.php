<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * Theme
 *
 * @see http://schema.org/Thing Documentation on Schema.org
 *
 * @author Maxim Yalagin <yalagin@gmail.com>
 *
 * @ORM\Entity
 * @ApiResource(
 *     collectionOperations={
 *     "get"={
 *              "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *     },
 *     "post"={
 *              "security"="is_granted('ROLE_ADMIN')",
 *              "denormalization_context"={"groups"={"theme:write","theme:items:post"}},
 *     }},
 *     itemOperations={
 *     "patch"={
 *          "security"="is_granted('ROLE_ADMIN')",
 *          "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"}
 *     },
 *     "put"={
 *          "security"="is_granted('ROLE_ADMIN')",
 *          "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"}
 *     },
 *     "get"={
 *          "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *          "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"}
 *     },
 *     "delete"={
 *          "security"="is_granted('ROLE_ADMIN')",
 *          "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"},
 *      }},
 *     normalizationContext={"groups"={"theme:read"}},
 *     denormalizationContext={"groups"={"theme:write"}},
 *     )
 * @ApiFilter(SearchFilter::class, properties={"id": "exact", "subject": "exact", "subthemes": "exact", "name": "partial"})
 * @UniqueEntity(fields={"id"})
 */
class Theme extends AbstractDate
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @Assert\Uuid
     * @Groups({"theme:read","theme:items:post","lesson:read","lesson:items:read"})
     */
    private $id;

    /**
     * @var string|null the name of the item
     *
     * @ORM\Column(type="text", nullable=true)
     * @ApiProperty(iri="http://schema.org/name")
     * @Groups({"theme:read","theme:write","project:read","subtheme:read","lesson:read","lesson:items:read"})
     */
    private $name;

    /**
     * @var Tags|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Tags")
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"theme:read","theme:write"})
     * @ApiProperty(readableLink=false, writableLink=false)
     */
    private $subject;

    /**
     * @var Collection<Subtheme>|null
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Subtheme", mappedBy="theme")
     * @Groups({"theme:read","theme:write"})
     * @ApiProperty(readableLink=false, writableLink=false)
     */
    private $subthemes;

    public function __construct()
    {
        $this->subthemes = new ArrayCollection();
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param Tags|null $subject
     */
    public function setSubject($subject): void
    {
        $this->subject = $subject;
    }

    /**
     * @return Tags|null
     */
    public function getSubject()
    {
        return $this->subject;
    }

    public function addSubtheme(Subtheme $subtheme): void
    {
        $this->subthemes[] = $subtheme;
    }

    public function removeSubtheme(Subtheme $subtheme): void
    {
        $this->subthemes->removeElement($subtheme);
    }

    public function getSubthemes(): Collection
    {
        return $this->subthemes;
    }
}
