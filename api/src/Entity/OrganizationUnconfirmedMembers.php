<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\OrganizationUnconfirmedMembersRepository;
use App\Validator\Constraints\CustomExpression;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use App\Controller\Organizations\SetJoinStatusAction;
use App\Controller\Organizations\SetInviteStatusAction;
use App\Controller\Organizations\AcceptRequestAction;
use App\Controller\Organizations\SetRejectStatusAction;

/**
 * An organization members with status. They are not currently in organization
 *
 * @see http://schema.org/members Documentation on Schema.org
 *
 * @author Maxim Yalagin <yalagin@gmail.com>
 *
 * @ApiResource(
 *     iri="http://schema.org/Organization",
 *     security="is_granted('ROLE_USER')",
 *     collectionOperations={
 *          "get",
 *          "post-invite"={
 *              "method"="POST",
 *              "path"="/organization_unconfirmed_members/invite",
 *              "denormalization_context"={"groups"={"organizaton_members:items:invite"}},
 *              "validation_groups"={"Default","organizaton_members:items:invite"},
 *              "controller"=SetInviteStatusAction::class,
 *          },
 *          "post-join"={
 *              "method"="POST",
 *              "path"="/organization_unconfirmed_members/join",
 *              "denormalization_context"={"groups"={"organizaton_members:items:join"}},
 *              "validation_groups"={"Default","organizaton_members:items:join"},
 *              "controller"=SetJoinStatusAction::class,
 *          }
 *      },
 *      itemOperations={
 *           "get",
 *          "delete"={
 *              "security"="object.getOrganization().getFounder() == user or is_granted('ROLE_ADMIN') or object.getMember() == user",
 *              "denormalization_context"={"groups"={"organizaton_members:items:change"}},
 *          },
 *          "put-accept-invitation"={
 *             "security"="object.getStatus() == 'STATUS_INVITED' and user == object.getMember()",
 *             "method"="PUT",
 *             "path"="/organization_unconfirmed_members/{id}/accept-invitation",
 *             "controller"=AcceptRequestAction::class,
 *             "denormalization_context"={"groups"={"organizaton_members:item:accept-initation"}},
 *             "validation_groups"={"Default","organizaton_members:item:accept-initation"}
 *         },
 *          "put-accept-join-request"={
 *             "security"="object.getStatus() == 'STATUS_WANTED_TO_JOIN' and user == object.getOrganization().getFounder()",
 *             "method"="PUT",
 *             "path"="/organization_unconfirmed_members/{id}/accept-join-request",
 *             "controller"=AcceptRequestAction::class,
 *             "denormalization_context"={"groups"={"organizaton_members:item:accept-join-request"}},
 *             "validation_groups"={"Default","organizaton_members:item:accept-join-request"}
 *         },
 *          "put-reject"={
 *             "security"="user == object.getOrganization().getFounder() or user == object.getMember()",
 *             "method"="PUT",
 *             "path"="/organization_unconfirmed_members/{id}/reject",
 *             "controller"=SetRejectStatusAction::class,
 *             "denormalization_context"={"groups"={"organizaton_members:item:accept-join-request"}},
 *             "validation_groups"={"Default","organizaton_members:item:accept-join-request"}
 *         }
 *     },
 *     normalizationContext={"groups"={"organizaton_members:read"}},
 *     denormalizationContext={"groups"={"organizaton_members:write"}},
 * )
 * @ORM\Entity(repositoryClass=OrganizationUnconfirmedMembersRepository::class)
 * @UniqueEntity(fields={"id"})
 * @UniqueEntity(
 *     fields={"organization","member","status"},
 *     groups={"organizaton_members:items:join","organizaton_members:items:invite"}
 * )
 * @ApiFilter(SearchFilter::class, properties={
 *     "organization": "exact",
 *     "member": "exact",
 *     "status": "exact",
 *     "organization.project": "exact",
 *     "organization.project": "exact"
 * })
 */
class OrganizationUnconfirmedMembers extends AbstractDate
{
    const STATUS_WANTED_TO_JOIN = 'STATUS_WANTED_TO_JOIN';
    const STATUS_INVITED = 'STATUS_INVITED';
    const STATUS_REJECTED = 'STATUS_REJECTED';

    /**
     * @var string
     *
     * @Groups({"organizaton_members:items:invite","organizaton_members:items:join","organizaton_members:read","person:read"})
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @Assert\Uuid
     * @Assert\NotBlank
     */
    private $id;

    /**
     * @var Organization Organization
     *
     * @Groups({"organizaton_members:items:invite","organizaton_members:items:join","organizaton_members:read","person:read"})
     * @ApiProperty(iri="http://schema.org/Organization",readableLink=false, writableLink=false)
     * @Assert\NotBlank
     * @CustomExpression(
     *     "user == this.getOrganization().getFounder()",
     *     message="You can not invite to not yours organization",
     *     groups={"organizaton_members:items:invite"}
     * )
     * @Assert\Expression(
     *     "this.getOrganization().isOpenForJoin()",
     *     message="This organization is not open for joining",
     *     groups={"organizaton_members:items:join"}
     * )
     *
     * @ORM\ManyToOne(targetEntity=Organization::class, inversedBy="organizationUnconfirmedMembers")
     */
    private $organization;

    /**
     * @var User User
     *
     * @Groups({"organizaton_members:items:invite","organizaton_members:items:join","organizaton_members:read","person:read"})
     * @ApiProperty(iri="http://schema.org/User",readableLink=false, writableLink=false)
     * @Assert\NotBlank
     * @CustomExpression (
     *     "user == this.getMember()",
     *     message="When join to organization you sould provide your user as a member",
     *     groups={"organizaton_members:items:join"}
     * )
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="organizationUnconfirmedMembers")
     *
     */
    private $member;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"organizaton_members:read","person:read"})
     * @Assert\NotBlank
     */
    private $status;


    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"organizaton_members:read","person:read"})
     */
    private $rejectedReason;

    /**
     * @ORM\Column(type="smallint", options={"default":"0"})
     */
    private $emailSentCounter;

    public function __construct()
    {
        $this->emailSentCounter = 0;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return OrganizationUnconfirmedMembers
     */
    public function setId(string $id): OrganizationUnconfirmedMembers
    {
        $this->id = $id;
        return $this;
    }

    public function getOrganization(): ?Organization
    {
        return $this->organization;
    }

    public function setOrganization(?Organization $organization): self
    {
        $this->organization = $organization;

        return $this;
    }

    public function getMember(): ?User
    {
        return $this->member;
    }

    public function setMember(?User $member): self
    {
        $this->member = $member;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getRejectedReason(): ?string
    {
        return $this->rejectedReason;
    }

    /**
     * @param string $rejectedReason
     * @return OrganizationUnconfirmedMembers
     */
    public function setRejectedReason(string $rejectedReason): OrganizationUnconfirmedMembers
    {
        $this->rejectedReason = $rejectedReason;
        return $this;
    }

    public function getEmailSentCounter(): ?int
    {
        return $this->emailSentCounter;
    }

    public function setEmailSentCounter(int $emailSentCounter): self
    {
        $this->emailSentCounter = $emailSentCounter;

        return $this;
    }
}
