<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * A person (alive, dead, undead, or fictional).
 *
 * @see http://schema.org/Person Documentation on Schema.org
 *
 * @author Maxim Yalagin <yalagin@gmail.com>
 *
 * @ORM\Entity(repositoryClass="App\Repository\PersonRepository")
 * @ApiResource(
 *     collectionOperations={
 *          "get"= {
 *              "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *          },
 *          "post"={
 *              "security"="is_granted('ROLE_USER')",
 *              "validation_groups"={"Default", "create"},
 *              "method"="POST",
 *              "denormalization_context"={"groups"={"person:write","person:items:create"}},
 *          },
 *     },
 *     itemOperations={
 *          "get"= {"security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *          "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"}},
 *          "patch"={
 *              "security"="is_granted('ROLE_USER') and object.user == user or is_granted('ROLE_ADMIN')",
 *              "denormalization_context"={"groups"={"person:item:change"},
 *               "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"}},
 *          },
 *          "put"={
 *              "security"="is_granted('ROLE_USER') and object.user == user or is_granted('ROLE_ADMIN')",
 *              "denormalization_context"={"groups"={"person:item:change"},
 *               "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"}},
 *          },
 *          "delete"={
 *              "security"="is_granted('ROLE_ADMIN')",
 *              "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"},
 *           }
 *     },
 *      iri="http://schema.org/Person",
 *      normalizationContext={"groups"={"person:read"}},
 *      denormalizationContext={"groups"={"person:write"},
 *     },
 * )
 * @UniqueEntity(fields={"id"})
 * @ApiFilter(SearchFilter::class, properties={"id": "exact", "user": "exact", "addressRegion": "exact"})
 */
class Person extends AbstractDate implements AuthoredEntityInterface
{
    /**
     * @var string UUID
     *
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @Assert\Uuid
     * @Assert\NotBlank()
     * @Groups({"person:read", "person:items:create"})
     */
    private string $id;

    /**
     * @var User|null
     *
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="person", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"person:read","admin:write"})
     * @ApiProperty(readableLink=false, writableLink=false)
     */
    public $user;

    /**
     * @var string|null Organizations outside of system that the person works for.
     *
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"person:read", "person:write","person:item:change"})
     * @Assert\Length(min=3, max=200, allowEmptyString=true, groups={"Default","person:write","person:item:change"})
     */
    private $worksFor;

    /**
     * @var ImageObject|null An image of the item. This can be a \[\[URL\]\] or a fully described \[\[ImageObject\]\].
     * other organizations can use that image
     * @Groups({"person:read", "person:write","person:item:change"})
     * @ORM\ManyToOne(targetEntity="App\Entity\ImageObject")
     * @ApiProperty(iri="http://schema.org/ImageObject",readableLink=false, writableLink=false)
     * @ApiSubresource
     */
    private $image;

    /**
     * @var AddressRegion|null  The region in which the locality is, and which is in the country. For example, California or another appropriate first-level Administrative division see https://en.wikipedia.org/wiki/List_of_administrative_divisions_by_country
     * @ORM\ManyToOne(targetEntity=AddressRegion::class, inversedBy="people")
     * @Groups({"person:read", "person:write","person:item:change"})
     * @ApiProperty(iri="http://schema.org/image",readableLink=false, writableLink=false)
     * @ApiSubresource
     */
    private $addressRegion;

    /**
     *  @var Contact|null  A contact point—for example, a whats up email or telephone.
     *
     * @ORM\OneToMany(targetEntity=Contact::class, mappedBy="person", orphanRemoval=true)
     * @ApiProperty(iri="http://schema.org/ContactPoint",readableLink=false, writableLink=false)
     * @Groups({"person:read", "person:write","person:item:change"})
     * @ApiSubresource
     */
    private $contacts;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"Default", "person:read", "person:write", "person:item:change"})
     */
    private $gender;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"Default", "person:read", "person:write", "person:item:change"})
     */
    private $jobTitle;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"Default", "person:read", "person:write", "person:item:change"})
     */
    private $phoneNumber;

    public function __construct()
    {
        $this->contacts = new ArrayCollection();
    }

    /**
     * @return float
     * @Groups({"person:read"})
     */
    public function getAggregateRating(): ?float
    {
        /** @var Review $review */
        $a = $this->getUser()->getReviews()->map(fn($review)=>($review->getReviewRating()))->toArray();
        $a = array_filter($a);
        $aggregate = null;
        if($a){
            $aggregate = round(array_sum($a) / count($a),1);
        }
        return $aggregate;
    }

    /**
     * @return int
     * @Groups({"person:read"})
     */
    public function getAggregateWins(): ?int
    {
        /** @var Organization $org */
        $a = $this->getUser()->getOrganizations()->filter(
            fn($org)=> $org->getWinner() instanceof Award
        )->count();

        return $a ? $a : null;
    }

    /**
     * @Groups({"person:read"})
     * @ApiProperty(readableLink=false, writableLink=false)
     */
    public function getOrganizations()
    {
        /** @var Organization $org */
        return $this->getUser()->getOrganizations();
    }

    /**
     * @Groups({"person:read"})
     * @ApiProperty(readableLink=false, writableLink=false)
     */
    public function getOrganizationUnconfirmedMembers()
    {
        /** @var Organization $org */
        return $this->getUser()->getOrganizationUnconfirmedMembers();
    }

    /**
     * @Groups({"person:read"})
     */
    public function getReviews()
    {
        /** @var Organization $org */
        return $this->getUser()->getReviews();
    }

    /**
     * @Groups({"person:read"})
     */
    public function getReviewsGiven()
    {
        /** @var Organization $org */
        return $this->getUser()->getReviewsGiven();
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param User|null $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    public function setAuthor(UserInterface $user): AuthoredEntityInterface
    {
        $this->setUser($user);
        return $this;
    }

    public function getWorksFor(): ?string
    {
        return $this->worksFor;
    }

    public function setWorksFor(?string $worksFor): Person
    {
        $this->worksFor = $worksFor;
        return $this;
    }

    /**
     * @param MediaObject|null $image
     */
    public function setImage($image): void
    {
        $this->image = $image;
    }

    /**
     * @return MediaObject|null
     */
    public function getImage()
    {
        return $this->image;
    }

    public function getAddressRegion(): ?AddressRegion
    {
        return $this->addressRegion;
    }

    public function setAddressRegion(?AddressRegion $addressRegion): self
    {
        $this->addressRegion = $addressRegion;

        return $this;
    }

    /**
     * @return Collection|Contact[]
     */
    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    public function addContact(Contact $contact): self
    {
        if (!$this->contacts->contains($contact)) {
            $this->contacts[] = $contact;
            $contact->setPerson($this);
        }

        return $this;
    }

    public function removeContact(Contact $contact): self
    {
        if ($this->contacts->contains($contact)) {
            $this->contacts->removeElement($contact);
//            // set the owning side to null (unless already changed)
//            if ($contact->getPerson() === $this) {
//                $contact->setPerson(null);
//            }
        }

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(?string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getJobTitle(): ?string
    {
        return $this->jobTitle;
    }

    public function setJobTitle(?string $jobTitle): self
    {
        $this->jobTitle = $jobTitle;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    /**
     * @param string|null $phoneNumber
     * @return Person
     */
    public function setPhoneNumber(?string $phoneNumber): Person
    {
        $this->phoneNumber = $phoneNumber;
        return $this;
    }

}
