<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * A product or service offered by a bank whereby one may deposit, withdraw or transfer money and in some cases be paid interest.
 *
 * @see http://schema.org/BankAccount Documentation on Schema.org
 *
 * @author Maxim Yalagin <yalagin@gmail.com>
 *
 * @ORM\Entity
 * @ApiResource(
 *     collectionOperations={
 *          "get"= {
 *              "security"="is_granted('ROLE_ADMIN')",
 *          },
 *          "post"={
 *              "security"="is_granted('ROLE_USER')",
 *              "validation_groups"={"Default", "create"},
 *              "method"="POST",
 *              "denormalization_context"={"groups"={"bank_accounts:write","bank_accounts:items:create"}},
 *          },
 *     },
 *     itemOperations={
 *          "get"= {"security"="object.getOwner() == user or is_granted('ROLE_ADMIN')",
 *          "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"}},
 *          "patch"={
 *              "security"="is_granted('ROLE_USER') and object.getOwner() == user or is_granted('ROLE_ADMIN')",
 *              "denormalization_context"={"groups"={"bank_accounts:item:change"},
 *               "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"}},
 *          },
 *          "put"={
 *              "security"="is_granted('ROLE_USER') and object.getOwner() == user or is_granted('ROLE_ADMIN')",
 *              "denormalization_context"={"groups"={"bank_accounts:item:change"},
 *               "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"}},
 *          },
 *          "delete"={
 *              "security"="is_granted('ROLE_ADMIN')",
 *              "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"},
 *           }
 *     },
 *      iri="http://schema.org/BankAccount",
 *      normalizationContext={"groups"={"bank_accounts:read", "read"}},
 *      denormalizationContext={"groups"={"bank_accounts:write"},
 *     },
 * )
 * @UniqueEntity(fields={"id"})
 */
class BankAccount extends AbstractDate implements AuthoredEntityInterface
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @Assert\Uuid
     * @Assert\NotBlank
     * @Groups({"bank_accounts:read", "bank_accounts:items:create"})
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"bank_accounts:read", "bank_accounts:write","bank_accounts:item:change"})
     */
    private $holder;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     *  @Groups({"bank_accounts:read", "bank_accounts:write","bank_accounts:item:change"})
     */
    private $accountNumber;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     *  @Groups({"bank_accounts:read", "bank_accounts:write","bank_accounts:item:change"})
     */
    private $additionalInfo;

    /**
     * @ORM\ManyToOne(targetEntity=Bank::class, inversedBy="bankAccounts")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"bank_accounts:read", "bank_accounts:write","bank_accounts:item:change"})
     * @Assert\NotBlank
     * @ApiProperty(readableLink=false, writableLink=false)
     * @ApiSubresource
     */
    private $bank;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="bankAccounts")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"bank_accounts:read","admin:write"})
     * @ApiProperty(readableLink=false, writableLink=false)
     */
    private $owner;

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setHolder(?string $holder): void
    {
        $this->holder = $holder;
    }

    public function getHolder(): ?string
    {
        return $this->holder;
    }

    public function setAccountNumber(?string $accountNumber): void
    {
        $this->accountNumber = $accountNumber;
    }

    public function getAccountNumber(): ?string
    {
        return $this->accountNumber;
    }

    public function setAdditionalInfo(?string $additionalInfo): void
    {
        $this->additionalInfo = $additionalInfo;
    }

    public function getAdditionalInfo(): ?string
    {
        return $this->additionalInfo;
    }

    public function getBank(): ?Bank
    {
        return $this->bank;
    }

    public function setBank(?Bank $bank): self
    {
        $this->bank = $bank;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @param User $user
     * @return $this|AuthoredEntityInterface
     */
    public function setAuthor(UserInterface $user): AuthoredEntityInterface
    {
       $this->setOwner($user);
       return $this;
    }
}
