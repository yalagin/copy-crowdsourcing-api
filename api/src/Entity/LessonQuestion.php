<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * A specific question - e.g. from a user seeking answers online, or collected in a Frequently Asked Questions (FAQ) document.
 *
 * @see http://schema.org/Question Documentation on Schema.org
 *
 * @author Maxim Yalagin <yalagin@gmail.com>
 *
 * @ORM\Entity
 * @ApiResource(
 *     iri="http://schema.org/Question",
 *     security="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *     collectionOperations={
 *          "get",
 *          "post"={
 *              "denormalization_context"={"groups"={"lesson_question:items:post"}},
 *              "security"="is_granted('ROLE_ADMIN')",
 *          }
 *     },
 *      itemOperations={
 *           "get",
 *           "put"={
 *              "denormalization_context"={"groups"={"lesson_question:items:change"}},
 *              "security"="is_granted('ROLE_ADMIN')",
 *          },
 *          "patch"={
 *              "denormalization_context"={"groups"={"lesson_question:items:change"}},
 *              "security"="is_granted('ROLE_ADMIN')",
 *          },
 *          "delete"={
 *              "denormalization_context"={"groups"={"lesson_question:items:change"}},
 *              "security"="is_granted('ROLE_ADMIN')",
 *          }
 *     },
 *     normalizationContext={"groups"={"lesson_question:read"}}
 * )
 * @UniqueEntity("id")
 * @Gedmo\TranslationEntity(class="App\Entity\Translation\LessonQuestionTranslation")
 */
class LessonQuestion extends AbstractDate
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @Assert\Uuid
     * @Assert\NotBlank
     * @Groups({"lesson_question:items:post","lesson_question:read","lesson:read","lesson_answer:read"})
     */
    private $id;

    /**
     * @var Lesson|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Lesson", inversedBy="hasParts")
     * @ORM\JoinColumn(nullable=false)
     * @ApiProperty(iri="http://schema.org/isPartOf",readableLink=false, writableLink=false)
     * @Groups({"lesson_question:items:post","lesson_question:read","lesson_answer:read"})
     */
    private $isPartOf;

    /**
     * @var Collection<LessonAnswer>|null
     *
     * @ORM\OneToMany(targetEntity="App\Entity\LessonAnswer", mappedBy="parentItem")
     * @ORM\JoinTable(inverseJoinColumns={@ORM\JoinColumn(nullable=false, unique=true)})
     * @Groups({"lesson_question:items:post","lesson_question:read","lesson_question:items:change","lesson:read"})
     * @ApiSubresource
     */
    private $connectedAnswers;

    /**
     * @var ?string Translatable a description of the item
     *
     * @Gedmo\Translatable
     * @ORM\Column(type="text", nullable=true)
     * @ApiProperty(iri="http://schema.org/description")
     * @Assert\NotNull
     * @Groups({"lesson_question:items:post","lesson_question:read","lesson_question:items:change","lesson:read","lesson_answer:read"})
     */
    private $description;

    /**
     * @var ?string  Translatable string|null a visual brief for the designer to create the question image
     *
     * @Gedmo\Translatable
     * @ORM\Column(type="text", nullable=true)
     * @ApiProperty(iri="http://schema.org/description")
     * @Groups({"lesson_question:items:post","lesson_question:read","lesson_question:items:change","lesson:read","lesson_answer:read"})
     */
    private $visualBrief;

    /**
     * @var string|null Translatable the answer key that is shown as a hint
     *
     * @Gedmo\Translatable
     * @ORM\Column(type="text", nullable=true)
     * @ApiProperty(iri="http://schema.org/description")
     * @Groups({"lesson_question:items:post","lesson_question:read","lesson_question:items:change","lesson:read","lesson_answer:read"})
     */
    private $answerKey;

    /**
     * @var string a type of item
     *
     * @ORM\Column(type="text")
     * @ApiProperty(iri="http://schema.org/type")
     * @Assert\NotNull
     * @Groups({"lesson_question:items:post","lesson_question:read","lesson_question:items:change","lesson:read","lesson_answer:read"})
     */
    private $type;

    /**
     * @var ImageObject|null An image of the item. This can be a \[\[URL\]\] or a fully described \[\[ImageObject\]\].
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\ImageObject")
     * @ApiProperty(iri="http://schema.org/ImageObject")
     * @Groups({"lesson_question:items:post","lesson_question:read","lesson_question:items:change","lesson:read","lesson_answer:read", "lesson_question:items:read"})
     * @ApiSubresource
     */
    private $image;

    /**
     * @var integer|null this is for convenient sorting
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"lesson_question:items:post","lesson_question:read","lesson_question:items:change","lesson:read","lesson_answer:read"})
     */
    private $ordering;

    /**
     *  @var ?Question  original question
     *
     * @ORM\OneToOne(targetEntity=Question::class, cascade={"persist", "remove"})
     * @Groups({"lesson_question:items:post","lesson_question:read","lesson_question:items:change"})
     * @ApiProperty(iri="http://schema.org/Question",readableLink=false, writableLink=false)
     */
    private $originalQuestion;

    /**
     * @var Locale
     *
     * @Gedmo\Locale
     */
    private $locale;

    public function __construct()
    {
        $this->connectedAnswers = new ArrayCollection();
    }

//    public function __toString()
//    {
//        return $this->description;
//    }

    public function setId(string $id): LessonQuestion
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param Lesson|null $isPartOf
     * @return LessonQuestion
     */
    public function setIsPartOf(?Lesson $isPartOf): LessonQuestion
    {
        $this->isPartOf = $isPartOf;
        return $this;
    }

    /**
     * @return Lesson|null
     */
    public function getIsPartOf()
    {
        return $this->isPartOf;
    }

    public function addAcceptedAnswer(Answer $acceptedAnswer): LessonQuestion
    {
        $this->connectedAnswers[] = $acceptedAnswer;
        return $this;
    }

    public function removeAcceptedAnswer(Answer $acceptedAnswer): LessonQuestion
    {
        $this->connectedAnswers->removeElement($acceptedAnswer);
        return $this;
    }

    public function getConnectedAnswers(): Collection
    {
        return $this->connectedAnswers;
    }

    public function setDescription(?string $description): LessonQuestion
    {
        $this->description = $description;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return string|null
     */
    public function getVisualBrief(): ?string
    {
        return $this->visualBrief;
    }

    /**
     * @param string|null $visualBrief
     * @return LessonQuestion
     */
    public function setVisualBrief(?string $visualBrief): LessonQuestion
    {
        $this->visualBrief = $visualBrief;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAnswerKey(): ?string
    {
        return $this->answerKey;
    }

    /**
     * @param string|null $answerKey
     * @return LessonQuestion
     */
    public function setAnswerKey(?string $answerKey): LessonQuestion
    {
        $this->answerKey = $answerKey;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return LessonQuestion
     */
    public function setType(string $type): LessonQuestion
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return ImageObject|null
     */
    public function getImage(): ?ImageObject
    {
        return $this->image;
    }

    /**
     * @param ImageObject|null $image
     * @return LessonQuestion
     */
    public function setImage(?ImageObject $image): LessonQuestion
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getOrdering(): ?int
    {
        return $this->ordering;
    }

    /**
     * @param int|null $ordering
     * @return LessonQuestion
     */
    public function setOrdering(?int $ordering): LessonQuestion
    {
        $this->ordering = $ordering;
        return $this;
    }


    public function getOriginalQuestion(): ?Question
    {
        return $this->originalQuestion;
    }

    public function setOriginalQuestion(?Question $originalQuestion): LessonQuestion
    {
        $this->originalQuestion = $originalQuestion;
        return $this;
    }

    /**
     * @return Locale
     */
    public function getLocale(): Locale
    {
        return $this->locale;
    }

    /**
     * @param Locale $locale
     * @return LessonQuestion
     */
    public function setLocale(Locale $locale): LessonQuestion
    {
        $this->locale = $locale;
        return $this;
    }
}
