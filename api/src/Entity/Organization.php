<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;
use App\Controller\Organizations\CloneOrganizationAction;
use App\Controller\Organizations\ExitOrganizationAction;
use App\Controller\Organizations\LockOrganizationAction;
use App\Controller\Organizations\RemoveMemberFromOrganizationAction;

/**
 * An organization such as a school, NGO, corporation, club, etc.
 *
 * @see http://schema.org/Organization Documentation on Schema.org
 *
 * @author Maxim Yalagin <yalagin@gmail.com>
 *
 * @ORM\Entity(repositoryClass="App\Repository\OrganizationRepository")
 * @ApiResource(
 *     iri="http://schema.org/Organization",
 *     security="is_granted('ROLE_USER')",
 *     collectionOperations={
 *          "get"={
 *              "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *          },
 *          "post"={
 *              "denormalization_context"={"groups"={"organizaton:items:post"}},
 *          }
 *      },
 *      itemOperations={
 *           "get"={
 *              "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *           },
 *           "put"={
 *              "security"="object.getFounder() == user or is_granted('ROLE_ADMIN')",
 *              "denormalization_context"={"groups"={"organizaton:items:change"}},
 *          },
 *          "patch"={
 *              "security"="object.getFounder() == user or is_granted('ROLE_ADMIN')",
 *              "denormalization_context"={"groups"={"organizaton:items:change"}},
 *          },
 *          "delete"={
 *              "security"="object.getFounder() == user && !object.isLocked() or is_granted('ROLE_ADMIN')",
 *              "denormalization_context"={"groups"={"organizaton:items:change"}},
 *          },
 *          "clone"={
 *              "method"="POST",
 *             "path"="/organizations/{id}/clone",
 *             "controller"=CloneOrganizationAction::class,
 *              "security"="object.getFounder() == user or is_granted('ROLE_ADMIN')",
 *              "denormalization_context"={"groups"={"organizaton:items:clone"}},
 *              "validation_groups"={"organizaton:items:clone"},
 *              "openapi_context"={"summary" = "Founder copying the organization for new project, all members get invited status and moved from members to organizationUnconfirmedMembers"}
 *          },
 *          "put-exit"={
 *             "security"="user in object.getMembers().toArray()",
 *             "method"="PUT",
 *             "path"="/organizations/{id}/exit",
 *             "controller"=ExitOrganizationAction::class,
 *             "denormalization_context"={"groups"={"organizaton:item:exit"}},
 *             "openapi_context"={"summary" = "Member will remove himself form the team. Be careful, if member will be founder we get first other member and make him founder, if there is only founder then org will be deleted"}
 *         },
 *          "put-remove"={
 *             "security"="object.getFounder() == user or is_granted('ROLE_ADMIN')",
 *             "method"="PUT",
 *             "path"="/organizations/{id}/remove",
 *             "controller"=RemoveMemberFromOrganizationAction::class,
 *             "denormalization_context"={"groups"={"organizaton:item:remove"}},
 *             "openapi_context"={"summary" = "founder can remove any member before the locking org"}
 *         },
 *          "put-lock"={
 *             "security"="object.getFounder() == user or is_granted('ROLE_ADMIN')",
 *             "method"="PUT",
 *             "path"="/organizations/{id}/lock",
 *             "controller"=LockOrganizationAction::class,
 *             "denormalization_context"={"groups"={"organizaton:item:lock"}},
 *             "openapi_context"={"summary" = "Lock the organization. Be careful organization can not be unlocked"}
 *         }
 *     },
 *     normalizationContext={"groups"={"organizaton:read"}},
 * )
 * @UniqueEntity(fields={"id"})
 * @ApiFilter(SearchFilter::class, properties={
 *     "members": "exact",
 *     "founder": "exact",
 *     "organizationUnconfirmedMembers": "exact",
 *     "organizationUnconfirmedMembers.member": "exact",
 *     "organizationUnconfirmedMembers.status": "exact",
 *     "project": "exact",
 *     "creativeWork": "exact",
 * })
 * @ApiFilter(BooleanFilter::class, properties={"creativeWork.isSubmitted"})
 */
class Organization extends AbstractDate implements AuthoredEntityInterface
{
    const STATUS_CREATIVE_WORK_NEEDED = "STATUS_CREATIVE_WORK_NEEDED";
    const STATUS_NOT_OPEN_FOR_JOIN = "STATUS_NOT_OPEN_FOR_JOIN";
    const STATUS_IN_PROGRESS = "STATUS_IN_PROGRESS";
    const STATUS_COMPLETED = "STATUS_COMPLETED";
    const STATUS_ERROR = "STATUS_ERROR";
    const STATUS_NEED_PARTNER = "STATUS_NEED_PARTNER";
    const STATUS_LOCKED = "STATUS_LOCKED";
    const STATUSES = [
        self::STATUS_CREATIVE_WORK_NEEDED,
        self::STATUS_NOT_OPEN_FOR_JOIN,
        self::STATUS_IN_PROGRESS,
        self::STATUS_COMPLETED,
        self::STATUS_ERROR,
        self::STATUS_NEED_PARTNER,
        self::STATUS_LOCKED,
    ];

    /**
     * @var string
     *
     * @Groups({"organizaton:items:post","organizaton:read","person:read","lesson:read","lesson:items:read"})
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @Assert\Uuid
     * @Assert\NotBlank
     */
    private $id;

    /**
     * @var string|null the name of the item
     *
     * @Groups({"organizaton:items:post","organizaton:items:change","organizaton:read","person:read","lesson:read","lesson:items:read"})
     * @ORM\Column(type="text", nullable=true)
     * @ApiProperty(iri="http://schema.org/name")
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @var Collection<User>|null a member of this organization
     *
     * @Groups({"organizaton:read","admin:write","person:read"})
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="organizations")
     * @ApiProperty(iri="http://schema.org/members",readableLink=false, writableLink=false)
     */
    private $members;

    /**
     * @var User|null a person who founded this organization
     *
     * @Groups({"organizaton:items:change","organizaton:read","admin:write","person:read","lesson:read","lesson:items:read"})
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ApiProperty(iri="http://schema.org/founder",readableLink=false, writableLink=false)
     */
    private $founder;

    /**
     * @var MediaObject|null An image of the item. This can be a \[\[URL\]\] or a fully described \[\[ImageObject\]\].
     * other organizations can use that image
     * @Groups({"organizaton:items:post","organizaton:items:change","organizaton:read","admin:write","person:read","lesson:read","lesson:items:read"})
     * @ORM\ManyToOne(targetEntity="App\Entity\MediaObject")
     * @ApiProperty(iri="http://schema.org/image")
     */
    private $image;

    /**
     * @ORM\OneToMany(targetEntity=OrganizationUnconfirmedMembers::class, mappedBy="organization", orphanRemoval=true, cascade={"persist", "remove", "merge"})
     * @Groups({"organizaton:read"})
     * @ApiProperty(iri="http://schema.org/members",readableLink=false, writableLink=false)
     *
     */
    private $organizationUnconfirmedMembers;

    /**
     * @var boolean is Open To join by people
     *
     * @ApiProperty(iri="http://schema.org/Boolean")
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"organizaton:read","organizaton:items:post","organizaton:items:change","admin:read","person:read","lesson:read","lesson:items:read"})
     * @SerializedName("openForJoin")
     */
    private $isOpenForJoin;

    /**
     * @var boolean is locked
     *
     * @ApiProperty(iri="http://schema.org/Boolean")
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"organizaton:read","admin:read","person:read","lesson:read","lesson:items:read"})
     * @SerializedName("locked")
     */
    private $isLocked;

    /**
     * @ORM\ManyToOne(targetEntity=Project::class, inversedBy="organizations")
     * @Groups({"organizaton:items:post","organizaton:read","person:read"})
     * @Assert\NotBlank
     * @ApiProperty(iri="http://schema.org/Project")
     */
    private $project;

    /**
     *  @var Award|null
     *
     * @ORM\OneToOne(targetEntity=Award::class, mappedBy="winner")
     * @Groups({"organizaton:read","admin:write","person:read","lesson:read"})
     * @ApiProperty(readableLink=false, writableLink=false)
     */
    private $winner;

    //cloning fields
    /**
     * @var string id
     * @Assert\Uuid(groups={"organizaton:items:clone"})
     * @Assert\NotBlank(groups={"organizaton:items:clone"})
     * @Groups({"organizaton:items:clone"})
     */
    private $cloneForProject;

    /**
     * @var string  id
     * @Assert\NotBlank(groups={"organizaton:items:clone"})
     * @Assert\NotBlank(groups={"organizaton:items:clone"})
     * @Groups({"organizaton:items:clone"})
     */
    private $newOrganizationId;

    /**
     * @var CreativeWork
     * @Groups({"organizaton:read","admin:write"})
     * @ORM\OneToOne(targetEntity=CreativeWork::class, mappedBy="creator", cascade={"persist", "remove"})
     * @ApiProperty(iri="http://schema.org/CreativeWork",readableLink=false, writableLink=false)
     */
    private $creativeWork;

    /**
     * @var User|null remove a member of this organization (User)
     * for removal
     * @Groups({"organizaton:item:remove"})
     * @ApiProperty(iri="http://schema.org/members",readableLink=false, writableLink=false)
     */
    public $memberForRemoval;

    public function __construct()
    {
        $this->members = new ArrayCollection();
        $this->organizationUnconfirmedMembers = new ArrayCollection();
        $this->setIsOpenForJoin(true);
        $this->setIsLocked(false);
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return Collection|User[]
     */
    public function getMembers(): Collection
    {
        return $this->members;
    }

    public function addMember(User $member): self
    {
        if (!$this->members->contains($member) && $this->members->count() <=4) {
            $this->members[] = $member;
        }

        return $this;
    }

    public function removeMember(User $member): self
    {
        if ($this->members->contains($member)) {
            $this->members->removeElement($member);
        }

        return $this;
    }

    /**
     * @param User|null $founder
     */
    public function setFounder($founder): void
    {
        $this->founder = $founder;
    }

    /**
     * @return User|null
     */
    public function getFounder()
    {
        return $this->founder;
    }

    /**
     * @param MediaObject|null $image
     */
    public function setImage($image): void
    {
        $this->image = $image;
    }

    /**
     * @return MediaObject|null
     */
    public function getImage()
    {
        return $this->image;
    }

    public function setAuthor(UserInterface $user): AuthoredEntityInterface
    {
        $this->setFounder($user);
        $this->addMember($user);
        return $this;
    }

    /**
     * @return Collection|OrganizationUnconfirmedMembers[]
     */
    public function getOrganizationUnconfirmedMembers(): Collection
    {
        return $this->organizationUnconfirmedMembers;
    }

    public function addOrganizationMember(OrganizationUnconfirmedMembers $organizationMember): self
    {
        if (!$this->organizationUnconfirmedMembers->contains($organizationMember)) {
            $this->organizationUnconfirmedMembers[] = $organizationMember;
            $organizationMember->setOrganization($this);
        }

        return $this;
    }

    public function removeOrganizationMember(OrganizationUnconfirmedMembers $organizationMember): self
    {
        if ($this->organizationUnconfirmedMembers->contains($organizationMember)) {
            $this->organizationUnconfirmedMembers->removeElement($organizationMember);
            // set the owning side to null (unless already changed)
            if ($organizationMember->getOrganization() === $this) {
                $organizationMember->setOrganization(null);
            }
        }

        return $this;
    }

    /**
     * @return bool
     * @Groups({"organizaton:read"})
     */
    public function isOpenForJoin(): bool
    {
        return $this->isOpenForJoin;
    }

    /**
     * @param bool $isOpenForJoin
     * @return Organization
     */
    public function setIsOpenForJoin(bool $isOpenForJoin): Organization
    {
        $this->isOpenForJoin = $isOpenForJoin;
        return $this;
    }

    /**
     * @return bool
     * @Groups({"organizaton:read"})
     */
    public function isLocked(): bool
    {
        return $this->isLocked;
    }

    /**
     * @param bool $isLocked
     * @return Organization
     */
    public function setIsLocked(bool $isLocked): Organization
    {
        $this->isLocked = $isLocked;
        return $this;
    }

    /**
     * @return string status of organization. todo bad practice depending on another entity in future it will slow down backend. possible solutions is move it to frontend or make another switch like is creative_work submitted.
     * @Groups({"organizaton:read"})
     * @ApiProperty(iri="http://schema.org/Status",readableLink=false, writableLink=false, openapiContext={"summary" = "
      STATUS_NEED_PARTNER (open_for_join = true, islocked=false)
     * STATUS_NOT_OPEN_FOR_JOIN (open_for_join = false, islocked=false)
     * STATUS_IN_PROGRESS (islocked = true, creative_work.is_submitted = false)
     * STATUS_COMPLETED (islocked = true, creative_work.is_submitted = true)
     * and STATUS_ERROR , STATUS_LOCKED
     * "})
     */
    public function getStatus()
    {

        if ($this->isOpenForJoin() && !$this->isLocked()) {
            return self::STATUS_NEED_PARTNER;
        }
        if (!$this->isOpenForJoin() && !$this->isLocked()) {
            return self::STATUS_NOT_OPEN_FOR_JOIN;
        }
        if ($this->isLocked() && $this->getCreativeWork() && !$this->getCreativeWork()->getIsSubmitted()) {
            return self::STATUS_IN_PROGRESS;
        }
        if ($this->isLocked() && $this->getCreativeWork() && $this->getCreativeWork()->getIsSubmitted()) {
            return self::STATUS_COMPLETED;
        }
        if ($this->isLocked()) {
            return self::STATUS_LOCKED;
        }
        return self::STATUS_ERROR;
    }

    /**
     * @return string
     */
    public function getCloneForProject(): string
    {
        return $this->cloneForProject;
    }

    /**
     * @param string $cloneForProject
     * @return Organization
     */
    public function setCloneForProject(string $cloneForProject): Organization
    {
        $this->cloneForProject = $cloneForProject;
        return $this;
    }

    /**
     * @return string
     */
    public function getNewOrganizationId(): string
    {
        return $this->newOrganizationId;
    }

    /**
     * @param string $newOrganizationId
     * @return Organization
     */
    public function setNewOrganizationId(string $newOrganizationId): Organization
    {
        $this->newOrganizationId = $newOrganizationId;
        return $this;
    }

    public function getCreativeWork(): ?CreativeWork
    {
        return $this->creativeWork;
    }

    public function setCreativeWork(CreativeWork $creativeWork): self
    {
        $this->creativeWork = $creativeWork;

        // set the owning side of the relation if necessary
        if ($creativeWork->getCreator() !== $this) {
            $creativeWork->setCreator($this);
        }

        return $this;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): self
    {
        $this->project = $project;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getMemberForRemoval(): ?User
    {
        return $this->memberForRemoval;
    }

    /**
     * @param User|null $memberForRemoval
     * @return Organization
     */
    public function setMemberForRemoval(?User $memberForRemoval): Organization
    {
        $this->memberForRemoval = $memberForRemoval;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWinner()
    {
        return $this->winner;
    }

    /**
     * @param mixed $winner
     * @return Organization
     */
    public function setWinner($winner)
    {
        $this->winner = $winner;
        return $this;
    }


}
