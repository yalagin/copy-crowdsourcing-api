<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * Award for project
 *
 * @see http://schema.org/Thing Documentation on Schema.org
 *
 * @author Maxim Yalagin <yalagin@gmail.com>
 *
 * @ORM\Entity
 * @ApiResource(
 *     collectionOperations={
 *     "get"={
 *              "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *     },
 *     "post"={
 *              "security"="is_granted('ROLE_ADMIN')",
 *              "denormalization_context"={"groups"={"award:write","award:items:post"}},
 *     }},
 *     itemOperations={
 *     "patch"={
 *          "security"="is_granted('ROLE_ADMIN')",
 *          "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"}
 *     },
 *     "put"={
 *          "security"="is_granted('ROLE_ADMIN')",
 *          "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"}
 *     },
 *     "get"={
 *          "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *          "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"}
 *     },
 *     "delete"={
 *          "security"="is_granted('ROLE_ADMIN')",
 *          "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"},
 *      }},
 *     normalizationContext={"groups"={"award:read"}},
 *     denormalizationContext={"groups"={"award:write"}},
 *     )
 * @ApiFilter(SearchFilter::class, properties={"id": "exact", "rank": "exact", "winner": "exact", "project": "exact", "name": "partial"})
 * @UniqueEntity(fields={"id"})
 */
class Award extends AbstractDate
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @Assert\Uuid
     * @Groups({"award:read","award:items:post"})
     */
    private $id;

    /**
     * @var string|null the name of the item
     *
     * @ORM\Column(type="text", nullable=true)
     * @ApiProperty(iri="http://schema.org/name")
     * @Groups({"award:read","award:write"})
     */
    private $name;

    /**
     * @var int|null
     *
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"award:read","award:write"})
     */
    private $rank;

    /**
     * @var Project|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Project", inversedBy="awards")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"award:read","award:write"})
     */
    private $project;

    /**
     * @var Organization|null Organization
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Organization", inversedBy="winner")
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"award:read","award:write"})
     */
    private $winner;

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setRank(?int $rank): void
    {
        $this->rank = $rank;
    }

    public function getRank(): ?int
    {
        return $this->rank;
    }

    /**
     * @param Project|null $project
     */
    public function setProject($project): void
    {
        $this->project = $project;
    }

    /**
     * @return Project|null
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param Organization|null $winner
     */
    public function setWinner($winner): void
    {
        $this->winner = $winner;
    }

    /**
     * @return Organization|null
     */
    public function getWinner()
    {
        return $this->winner;
    }
}
