<?php


namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use App\Controller\SendResetPasswordLinkToEmailAction;
use App\Controller\SendConfirmationTokenToEmailAction;
use App\Controller\SendMobileApplicationLinkToEmailAction;

/**
 * @ApiResource(
 *     collectionOperations={
 *          "get"={
 *          "openapi_context"={"summary" = "without get adminpanel get error "},
 *          "deprecation_reason"="without get adminpanel get error"
 *          },
 *         "post"={
 *             "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY') ",
 *             "method"="POST",
 *             "path"="/user/send-token-to-email",
 *             "controller"=SendResetPasswordLinkToEmailAction::class,
 *             "openapi_context"={"summary" = "Send reset token to users email for resetting password"}
 *         },
 *          "post-activation-link-to-email"={
 *             "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY') ",
 *             "method"="POST",
 *             "path"="/user/send-activation-link-to-email",
 *             "controller"=SendConfirmationTokenToEmailAction::class,
 *             "openapi_context"={"summary" = "Send confirmation link to email again"}
 *         },
 *          "post-app-link-to-email"={
 *             "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY') ",
 *             "method"="POST",
 *             "path"="/user/send-mobile-application-link-to-email",
 *             "controller"=SendMobileApplicationLinkToEmailAction::class,
 *             "openapi_context"={"summary" = "Send mobile application link to email"}
 *         }
 *     },
 *     itemOperations={},
 *     normalizationContext={"groups"={"user_reset_password:read"}},
 *     denormalizationContext={"groups"={"user_reset_password:write"}})
 */
class UserCustomActions
{
    /**
     * @var string
     *
     * @Assert\NotBlank
     * @Assert\Email
     * @Groups({"user_reset_password:read", "user_reset_password:write"})
     */
    public $email;

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }
}
