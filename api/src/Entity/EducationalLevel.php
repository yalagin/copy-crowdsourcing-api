<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;

/**
 * The level in terms of progression through an educational or training context. Examples of educational levels include 'beginner', 'intermediate' or 'advanced', and formal sets of level indicators.
 *
 * @see http://schema.org/educationalLevel Documentation on Schema.org
 *
 * @author Maxim Yalagin <yalagin@gmail.com>
 *
 * @ORM\Entity
 * @ApiResource(
 *     iri="http://schema.org/educationalLevel",
 *     collectionOperations={
 *     "get",
 *     "post"={
 *              "security"="is_granted('ROLE_ADMIN')",
 *              "validation_groups"={"Default","educational_level:items:create"},
 *              "denormalization_context"={"groups"={"educational_level:write","educational_level:items:create"}},
 *     }},
 *     itemOperations={
 *     "patch"={
 *          "security"="is_granted('ROLE_ADMIN')",
 *          "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"}
 *     },
 *     "put"={
 *          "security"="is_granted('ROLE_ADMIN')",
 *          "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"}
 *     },
 *     "get"={
 *          "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"}
 *     },
 *     "delete"={
 *          "security"="is_granted('ROLE_ADMIN')",
 *          "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"},
 *      }},
 *     normalizationContext={"groups"={"educational_level:read"}},
 *     denormalizationContext={"groups"={"educational_level:write"}},
 *     attributes={"pagination_enabled"=false}
 * )
 * @UniqueEntity(fields={"id"})
 */
class EducationalLevel extends AbstractDate
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @Assert\Uuid(groups={"educational_level:items:create",})
     * @Groups({"educational_level:read","educational_level:items:create","lesson:read","lesson:items:read"})
     */
    private $id;

    /**
     * @var string|null The level in terms of progression through an educational or training context. Examples of educational levels include 'beginner', 'intermediate' or 'advanced', and formal sets of level indicators.
     *
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"educational_level:read","educational_level:write","lesson:read","lesson:items:read"})
     * @Assert\NotBlank()
     */
    private $level;

    /**
     * @var integer|null this is for convenient sorting
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"educational_level:read","educational_level:write","lesson:read","lesson:items:read"})
     * @Assert\NotBlank()
     */
    private $ordering;

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setLevel(?string $level): void
    {
        $this->level = $level;
    }

    public function getLevel(): ?string
    {
        return $this->level;
    }

    /**
     * @return int|null
     */
    public function getOrdering(): ?int
    {
        return $this->ordering;
    }

    /**
     * @param int|null $ordering
     * @return EducationalLevel
     */
    public function setOrdering(?int $ordering): EducationalLevel
    {
        $this->ordering = $ordering;
        return $this;
    }
}
