<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * Subtheme
 *
 * @see http://schema.org/Thing Documentation on Schema.org
 *
 * @author Maxim Yalagin <yalagin@gmail.com>
 *
 * @ORM\Entity
 * @ApiResource(
 *     collectionOperations={
 *     "get"={
 *              "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *     },
 *     "post"={
 *              "security"="is_granted('ROLE_ADMIN')",
 *              "denormalization_context"={"groups"={"subtheme:write","subtheme:items:post"}},
 *     }},
 *     itemOperations={
 *     "patch"={
 *          "security"="is_granted('ROLE_ADMIN')",
 *          "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"}
 *     },
 *     "put"={
 *          "security"="is_granted('ROLE_ADMIN')",
 *          "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"}
 *     },
 *     "get"={
 *          "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *          "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"},
 *          "normalization_context"={"groups"={"subtheme:read"}},
 *     },
 *     "delete"={
 *          "security"="is_granted('ROLE_ADMIN')",
 *          "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"},
 *      }},
 *     normalizationContext={"groups"={"subtheme:read"}},
 *     denormalizationContext={"groups"={"subtheme:write"}},
 *     )
 * @ApiFilter(SearchFilter::class, properties={"id": "exact", "subject": "exact", "projects": "exact", "subthemes": "exact", "name": "partial"})
 * @UniqueEntity(fields={"id"})
 */
class Subtheme extends AbstractDate
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @Assert\Uuid
     * @Groups({"subtheme:read","subtheme:items:post"})
     */
    private $id;

    /**
     * @var string|null the name of the item
     *
     * @ORM\Column(type="text", nullable=true)
     * @ApiProperty(iri="http://schema.org/name")
     * @Groups({"subtheme:read","subtheme:write","project:read","lesson:read","lesson:items:read"})
     */
    private $name;

    /**
     * @var Theme|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Theme", inversedBy="subthemes")
     * @Groups({"subtheme:read","subtheme:write","project:read","lesson:read","lesson:items:read"})
     */
    private $theme;

    /**
     * @ORM\OneToMany(targetEntity=Project::class, mappedBy="subtheme")
     * @Groups({"subtheme:read","subtheme:write"})
     * @ApiProperty(readableLink=false, writableLink=false)
     */
    private $projects;

    public function __construct()
    {
        $this->projects = new ArrayCollection();
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setTheme(?Theme $theme): void
    {
        $this->theme = $theme;
    }

    public function getTheme(): ?Theme
    {
        return $this->theme;
    }

    /**
     * @return Collection|Project[]
     */
    public function getProjects(): Collection
    {
        return $this->projects;
    }

    public function addProject(Project $project): self
    {
        if (!$this->projects->contains($project)) {
            $this->projects[] = $project;
            $project->setSubtheme($this);
        }

        return $this;
    }

    public function removeProject(Project $project): self
    {
        if ($this->projects->contains($project)) {
            $this->projects->removeElement($project);
            // set the owning side to null (unless already changed)
            if ($project->getSubtheme() === $this) {
                $project->setSubtheme(null);
            }
        }

        return $this;
    }
}
