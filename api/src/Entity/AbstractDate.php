<?php


namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;


/**
 * created at updated at parent class
 *
 * @see http://schema.org/DateTime Documentation on Schema.org
 *
 * @author Maxim Yalagin <yalagin@gmail.com>
 *
 * @ORM\HasLifecycleCallbacks
 * @ORM\MappedSuperclass
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", hardDelete=false)
 * @ApiFilter(SearchFilter::class)
 * @ApiFilter(OrderFilter::class)
 */
abstract class AbstractDate
{
    use SoftDeleteableEntity;

    /**
     * @var null|DateTimeInterface
     *
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     * @ApiProperty(iri="http://schema.org/DateTime")
     * @Groups({"read","admin:read"})
     */
    protected  $createdAt;

    /**
     * @var null|DateTimeInterface
     *
     * @ORM\Column(type="datetime",nullable=true, options={"default": "CURRENT_TIMESTAMP"} )
     * @ApiProperty(iri="http://schema.org/DateTime")
     * @Groups({"read","admin:read"})
     */
    protected  $updatedAt;

    public function setCreatedAt( DateTimeInterface $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getCreatedAt():? DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setUpdatedAt( DateTimeInterface $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function getUpdatedAt():? DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * @throws \Exception
     * @throws \Exception
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new \DateTime(date('Y-m-d H:i:s')));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        }
    }
}
