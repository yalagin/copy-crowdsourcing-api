<?php


namespace App\Service;


use App\Email\Mailer;
use App\Entity\Answer;
use App\Entity\CreativeWork;
use App\Entity\Lesson;
use App\Entity\LessonAnswer;
use App\Entity\LessonQuestion;
use App\Entity\Question;
use App\Entity\Review;
use App\Entity\User;
use App\Exception\InvalidCreativeWorkException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Uid\Uuid;

class CreativeWorkService
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $em;
    /**
     * @var Mailer
     */
    private Mailer $mailer;

    /**
     * @inheritDoc
     */
    public function __construct(EntityManagerInterface $entityManager, Mailer $mailer)
    {
        $this->em = $entityManager;
        $this->mailer = $mailer;
    }

    /**
     * @param CreativeWork $data
     * @return CreativeWork
     * @throws InvalidCreativeWorkException
     */
    public function handle(CreativeWork $data): CreativeWork
    {
        $this->validate($data);

        $data = $this->submit($data);

        $this->sendEmailToEachMember($data);

        return $data;
    }

    /**
     * @param CreativeWork $data
     * @throws InvalidCreativeWorkException
     * @todo change to EAGER loading
     */
    public function validate(CreativeWork $data): void
    {
        $questionsNumber = $data->getCreatedFor()->getMinimumNumberOfQuestions();

        if ($data->getCreatedFor()->getEndTime() < (new \DateTime(date('Y-m-d H:i:s')))) {
            throw new InvalidCreativeWorkException('Maaf, pengiriman sudah selesai');
        }
        if ($data->getHasParts()->count() < $questionsNumber) {
            throw new InvalidCreativeWorkException('Minimal jumlah pertanyaan adalah ' . $questionsNumber);
        }

        /** @var Question $question */
        foreach ($data->getHasParts() as $question) {
            $questionHasAnswer = false;
            /** @var Answer $answer */
            foreach ($question->getConnectedAnswers() as $answer) {
                if ($answer->getCorrect()) {
                    $questionHasAnswer = true;
                }

            }
            if (!$questionHasAnswer) {
                throw new InvalidCreativeWorkException($question->getDescription() . ' pertanyaan harus memiliki setidaknya satu jawaban yang benar');
            }
            if (!$question->getConnectedAnswers()->count()) {
                throw new InvalidCreativeWorkException($question->getDescription() . ' pertanyaan harus memiliki jawaban, harap hapus atau tambahkan jawaban');
            }
        }
        if(!$data->getCreator() || $data->getCreator()->getMembers()->isEmpty()){
            throw new InvalidCreativeWorkException();
        }
        if($data->getIsSubmitted()){
            throw new InvalidCreativeWorkException('Sudah dikirim');
        }
    }

    private function submit(CreativeWork $data): CreativeWork
    {
        $data->setDatePublished(new \DateTime(date('Y-m-d H:i:s')));
        $data->setIsSubmitted(true);
        /** @var User $user */
        $users = $data->getCreator()->getMembers();
        foreach($users as $author){
            foreach($users as $user){
                if($author !== $user) {
                    $review = new Review();
                    $review->setId(Uuid::v4());
                    $review->setAuthor($author);
                    $review->setOrganization($data->getCreator());
                    $review->setItemReviewed($user);
                    $review->setIsSubmitted(false);
                    $this->em->persist($review);
                }
            }
        }

        return $data;
    }

    public function importToLesson(CreativeWork $data)
    {
        $lesson = new Lesson();
        $lesson->setId(Uuid::v4());
        $lesson->setCreator($data->getCreator());
        $lesson->setCreatedFor($data->getCreatedFor());
        $lesson->setOriginalCreativeWork($data);
        $lesson->setDocument($data->getDocument());
        $lesson->setArchive($data->getArchive());
        $lesson->setVideo($data->getVideo());
        $lesson->setTranscript($data->getTranscript());
        foreach($data->getAudio() as $audio){
            $lesson->addAudio($audio);
        }
        foreach($data->getImages() as $image){
            $lesson->addImage($image);
        }
        $this->em->persist($lesson);

        /** @var Question $question */
        foreach($data->getHasParts() as $question){
            $lessonQuestion = new LessonQuestion();
            $lessonQuestion->setId(Uuid::v4());
            $lessonQuestion->setIsPartOf($lesson);
            $lessonQuestion->setDescription($question->getDescription());
            $lessonQuestion->setVisualBrief($question->getVisualBrief());
            $lessonQuestion->setAnswerKey($question->getAnswerKey());
            $lessonQuestion->setType($question->getType());
            $lessonQuestion->setImage($question->getImage());
            $lessonQuestion->setOrdering($question->getOrdering());
            $lessonQuestion->setOriginalQuestion($question);
            $this->em->persist($lessonQuestion);

            /** @var Answer $answer */
            foreach($question->getConnectedAnswers() as $answer){
                $lessonAnswer = new LessonAnswer();
                $lessonAnswer->setId(Uuid::v4());
                $lessonAnswer->setParentItem($lessonQuestion);
                $lessonAnswer->setDescription($answer->getDescription());
                $lessonAnswer->setCorrect($answer->getCorrect());
                $lessonAnswer->setOrdering($answer->getOrdering());
                $lessonAnswer->setOriginalAnswer($answer);
                $this->em->persist($lessonAnswer);
            }
        }

        $this->em->flush();

        return $lesson;
    }

    /**
     * @param CreativeWork $data
     */
    public function sendEmailToEachMember(CreativeWork $data): void
    {
        foreach($data->getCreator()->getMembers() as $user){
            $this->mailer->sendProposalIsSubmitted($user,$data);
        }
    }
}
