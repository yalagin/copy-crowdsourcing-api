<?php


namespace App\Service;


use Aws\S3\S3Client;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;

class AwsS3Service
{
    /**
     * @var S3Client
     */
    private $s3;
    private $bucketName;

    /**
     * AwsS3Service constructor.
     * @param S3Client $s3
     * @param $bucketName
     */
    public function __construct(S3Client $s3, $bucketName)
    {
        $this->s3 = $s3;
        $this->bucketName = $bucketName;
    }

    /**
     * @return mixed
     */
    public function getS3()
    {
        return $this->s3;
    }

    public function getSignedUrl($fileName) :string
    {
        $s3Client = $this->getS3();

        $cmd = $s3Client->getCommand('PutObject', [
            'Bucket' => $this->bucketName,
            'Key' => $fileName,
//          'ContentType' => $uploadedFile->getMimeType(),
//          'ResponseContentDisposition' => 'attachment; filename="'.$fileName.'"'
        ]);
        $request = $s3Client->createPresignedRequest($cmd, '+30 minutes');
        return (string)$request->getUri();
    }

    public function uploadDirectlyToS3(UploadedFile $uploadedFile)
    {
        $result = $this->getS3()->putObject([
            'Bucket' => $this->bucketName,
            'Key' => $uploadedFile->getClientOriginalName(),
            'SourceFile' => $uploadedFile,
        ]);
        return $result;
    }

    public function getObjectUrl($fileName): string
    {
        return $this->getS3()->getObjectUrl($this->bucketName, $fileName);
    }
}
