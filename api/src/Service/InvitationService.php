<?php


namespace App\Service;


use App\Email\Mailer;
use App\Entity\Organization;
use App\Entity\OrganizationUnconfirmedMembers;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpFoundation\JsonResponse;

class InvitationService
{

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;
    /**
     * @var Mailer
     */
    private Mailer $mailer;

    public function __construct(EntityManagerInterface $entityManager, Mailer $mailer)
    {
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
    }



    public function sendReminders(SymfonyStyle $io = null){

        $io->note('Getting invited users IDs');
        $invitationsUserIDs = $this->entityManager->getRepository(OrganizationUnconfirmedMembers::class)
            ->getInvitedUserIDs()
        ;
        $io->note('Getting founders IDs');
        $founderIDs = $this->entityManager->getRepository(OrganizationUnconfirmedMembers::class)
            ->getFoundersIdsThatReceivedInvitation()
        ;

        $userIDs = array_unique(array_merge($invitationsUserIDs,$founderIDs));
        $io->note('Sending emails');
        $io->progressStart(count($userIDs));
        foreach ($userIDs as $newInvitationsUserID) {
            $this->getInvitesAndSendEmail($newInvitationsUserID);
            $io->progressAdvance();
            //sleep(1); #needed for MailTrap.io
        }

        $this->entityManager
            ->getRepository(OrganizationUnconfirmedMembers::class)
            ->incrementCounterByOne()
            ->changeStatusToRejectedIfCounterMoreThanAmountFromSettings()
        ;
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @param $newInvitationsUserID
     */
    private function getInvitesAndSendEmail($newInvitationsUserID): void
    {
        $user = $this->entityManager->getRepository(User::class)->find($newInvitationsUserID);
        $newInvitations = $this->entityManager
            ->getRepository(OrganizationUnconfirmedMembers::class)
            ->getNewInvitationsForUser($user);

        $oldInvitations = $this->entityManager
            ->getRepository(OrganizationUnconfirmedMembers::class)
            ->getOldInvitationsForUser($user);

        $organizationsThatReceivedJoinRequestsIds = $this->entityManager
            ->getRepository(OrganizationUnconfirmedMembers::class)
            ->getOrganizationsThatReceivedJoinRequests($user);

        $organizationsThatReceivedJoinRequests = $this->entityManager
        ->getRepository(Organization::class)
            ->findById($organizationsThatReceivedJoinRequestsIds);

        $this->mailer->sendInvitationSummaryReport(
            $user,
            $newInvitations,
            $oldInvitations,
            $organizationsThatReceivedJoinRequests
        );
    }

}
