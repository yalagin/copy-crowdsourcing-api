<?php


namespace App\Service;


use App\Entity\AnimationObject;
use App\Entity\ImageObject;
use App\Entity\MediaObject;
use App\Entity\VideoObject;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class MediaObjectService
{
    public function processMediaObject(MediaObject $data, Request $request, EntityManagerInterface $entityManager): MediaObject
    {
        $data->setLocale($request->getLocale());

        $parametersAsArray = [];
        if ($content = $request->getContent()) {
            $parametersAsArray = json_decode($content, true);
        }
        $name = isset($parametersAsArray['name']) ? $parametersAsArray['name'] : null;
        $url = isset($parametersAsArray['url']) ? $parametersAsArray['url'] : null;
        if ($name) {
            $data->setName($name);
        }

        if ($data instanceof VideoObject) {
            /** @var VideoObject $data */
            $imageId = isset($parametersAsArray['image']) ? substr($parametersAsArray['image'], -36) : null;
            $animationId = isset($parametersAsArray['animation']) ? substr($parametersAsArray['animation'], -36) : null;
            $duration = isset($parametersAsArray['duration']) ? $parametersAsArray['duration'] : null;

            if ($animationId) {
                $data->setAnimation($entityManager->getRepository(AnimationObject::class)->find($animationId));
            }
            if ($imageId) {
                $data->setImage($entityManager->getRepository(ImageObject::class)->find($imageId));
            }
            $data->setDuration($duration);
        }

        if ($url) {
            //get just file
            $file = trim(parse_url($url, PHP_URL_PATH));
            //remove first slash
            $file = substr($file, 1);
            $data->setFilePath($file);
        }
        return $data;
    }

}
