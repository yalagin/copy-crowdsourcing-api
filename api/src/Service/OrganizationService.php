<?php


namespace App\Service;


use App\Entity\Organization;
use App\Entity\OrganizationUnconfirmedMembers;
use App\Entity\Project;
use App\Entity\User;
use App\Exception\OrganizationException;
use Doctrine\ORM\EntityManagerInterface;

class OrganizationService
{

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function declineAllUserInvitationsForCurrentProject(User $user,Project $project)
    {
        $invitations = $this->entityManager->getRepository(OrganizationUnconfirmedMembers::class)->findInvitationsForProject($user,$project);

        /** @var OrganizationUnconfirmedMembers $invitation */
        foreach($invitations as $invitation){
            $invitation->setStatus(OrganizationUnconfirmedMembers::STATUS_REJECTED);
            $invitation->setRejectedReason('Accepted another team');
            $this->entityManager->persist($invitation);
        }
    }

    public function checkIfUserAlreadyMemberInTeamForCurrentProject(User $user, Project $project)
    {
         if(count($this->entityManager->getRepository(Organization::class)->getOrganizationsForUserAndProject($user,$project))){
             throw new OrganizationException('You are already member of organization for this project');
         }
    }
}
