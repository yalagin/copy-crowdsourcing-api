<?php



namespace App\Validator\Constraints;

use App\Entity\User;
use App\Exception\UnauthorizedUserException;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Bernhard Schussek <bschussek@symfony.com>
 */
class CustomExpressionValidator extends ConstraintValidator
{
    private $expressionLanguage;
    /**
     * @var TokenStorageInterface
     */
    private TokenStorageInterface $tokenStorage;

    public function __construct(ExpressionLanguage $expressionLanguage = null,TokenStorageInterface $tokenStorage)
    {
        $this->expressionLanguage = $expressionLanguage;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * {@inheritdoc}
     * @throws UnauthorizedUserException
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof CustomExpression) {
            throw new UnexpectedTypeException($constraint, CustomExpression::class);
        }

        $token = $this->tokenStorage->getToken();

        if (null === $token) {
            return;
        }

        /** @var UserInterface $author */
        $user = $token->getUser();

        if (!$user instanceof User) {
            throw new UnauthorizedUserException();
        }


        $variables = $constraint->values;
        $variables['value'] = $value;
        $variables['this'] = $this->context->getObject();
        $variables['user'] = $user;

        if (!$this->getExpressionLanguage()->evaluate($constraint->expression, $variables)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $this->formatValue($value, self::OBJECT_TO_STRING))
                ->setCode(CustomExpression::EXPRESSION_FAILED_ERROR)
                ->addViolation();
        }
    }

    private function getExpressionLanguage(): ExpressionLanguage
    {
        if (null === $this->expressionLanguage) {
            $this->expressionLanguage = new ExpressionLanguage();
        }

        return $this->expressionLanguage;
    }
}
