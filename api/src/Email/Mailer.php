<?php

namespace App\Email;

use App\Entity\CreativeWork;
use App\Entity\Organization;
use App\Entity\Project;
use App\Entity\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class Mailer
{

    private MailerInterface $mailer;

    private $from ;
    private $bcc ;

    public function __construct(
        MailerInterface $mailer
    )
    {
        $this->mailer = $mailer;
        $this->from = $_ENV['EMAIL_FROM'];
        $this->bcc = $_ENV['EMAIL_BCC'];
    }

    public function sendConfirmationEmail(User $user)
    {
        $email = $this->getEmailTemplateAndSetFromAndTo($user)
            ->subject('Terima kasih sudah mendaftar!')

            // path of the Twig template to render
            ->htmlTemplate('emails/email-confirmation.html.twig')

            // pass variables (name => value) to the template
            ->context([
                'expiration_date' => new \DateTime('+7 days'),
                'user'=> $user,
                'url' => $_ENV['REACT_APP_API_ENTRYPOINT']
            ]);

        $this->mailer->send($email);
    }

    public function sendResetPasswordEmail(User $user)
    {
        $email = $this->getEmailTemplateAndSetFromAndTo($user)
            ->subject('Atur ulang kata sandi Anda di sini')

            // path of the Twig template to render
            ->htmlTemplate('emails/reset-password.html.twig')

            // pass variables (name => value) to the template
            ->context([
                'expiration_date' => new \DateTime('+7 days'),
                'user'=> $user,
                'url' => $_ENV['REACT_APP_API_ENTRYPOINT'],
                'reset_password_url' => $_ENV['REACT_APP_FRONTEND_ENTRYPOINT'] . '/set-password'
            ]);

        $this->mailer->send($email);
    }

    public function sendTestEmail($mailAddress)
    {
        $TemplatedEmail  = new TemplatedEmail();
        $TemplatedEmail->from(new Address($this->from))
            ->to(new Address($mailAddress, $mailAddress))
            ->bcc($this->bcc);

        $email = $TemplatedEmail
            ->subject('Test mail')

            // path of the Twig template to render
            ->htmlTemplate('emails/test.html.twig')

            // pass variables (name => value) to the template
            ->context([
                'url' => $_ENV['REACT_APP_API_ENTRYPOINT'],
            ]);
        ;

        $this->mailer->send($email);

    }

    /**
     * @param User $user
     * @return TemplatedEmail
     */
    public function getEmailTemplateAndSetFromAndTo(User $user): TemplatedEmail
    {
        return (new TemplatedEmail())
            ->from(new Address($this->from))
            ->to(new Address($user->getEmail(), $user->getUsername()))
            ->bcc($this->bcc)
            ;
    }

    public function sendProposalIsSubmitted(User $user,CreativeWork $data){
        $email = $this->getEmailTemplateAndSetFromAndTo($user)
            ->subject('Thank you for your proposal!')
            ->context([
                'user'=> $user,
                'team' => $data->getCreator(),
                'project' => $data->getCreator()->getProject(),
                'url' => $_ENV['REACT_APP_API_ENTRYPOINT'],
            ])

            // path of the Twig template to render
            ->htmlTemplate('emails/proposal-is-submitted.html.twig');

        $this->mailer->send($email);
    }

    public function sendDeadlineReminderForTeams(User $user, Organization $organization, Project $project){
        $email = $this->getEmailTemplateAndSetFromAndTo($user)
            ->subject('SahabatPintar - Progress on '.$project->getTitle())
            ->context([
                'user'=> $user,
                'team' => $organization,
                'project' => $project
            ])

            // path of the Twig template to render
            ->htmlTemplate('emails/deadline-reminder-for-teams.html.twig');

        $this->mailer->send($email);
    }

//    /**
//     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
//     */
//    public function sentCongratulationEmail(?Project $project)
//    {
//        foreach($project->getWinner()->getCreator()->getMembers() as $member){
//            $email = $this->getEmailTemplateAndSetFromAndTo($member)
//                ->subject('Congratulations you are the winner!!!')
//                ->htmlTemplate('emails/congratulation-on-winning.html.twig')
//                ->context([
//                    'project'=> $project,
//                    'user'=> $member
//                ]);
//
//            $this->mailer->send($email);
//        }
//    }

    public function sendLinkToMobileAppInEmail(string $email)
    {
        $email = (new TemplatedEmail())
            ->from(new Address($this->from))
            ->to(new Address($email))
            ->bcc($this->bcc)
            ->subject('Unduh Titik Pintar sekarang juga')
            ->context([
                'url' => $_ENV['REACT_APP_API_ENTRYPOINT']
            ])
            ->htmlTemplate('emails/send-mobile-application-link.html.twig');

        $this->mailer->send($email);
    }

    public function sendInvitationSummaryReport(?User $user, $newInvitations, $oldInvitations, $organizationsThatReceivedJoinRequests)
    {
        $email = $this->getEmailTemplateAndSetFromAndTo($user)
            ->subject('SahabatPintar.id - Pengingat Undangan')
            ->context([
                'user'=> $user,
                'new_invitations' => $newInvitations,
                'old_invitations' => $oldInvitations,
                'organizations_that_received_join_requests' => $organizationsThatReceivedJoinRequests,
                'url' => $_ENV['REACT_APP_API_ENTRYPOINT'],
                'frontend_url' => $_ENV['REACT_APP_FRONTEND_ENTRYPOINT'],
            ])
            // path of the Twig template to render
            ->htmlTemplate('emails/invitations-reminder.html.twig');

        $this->mailer->send($email);
    }
}
