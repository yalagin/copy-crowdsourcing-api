<?php


namespace App\Exception;


use Exception;
use Throwable;

class UnauthorizedUserException extends Exception
{
    public function __construct(
    string $message = 'User is unauthorized',
    int $code = 0,
    Throwable $previous = null
) {
    parent::__construct($message, $code, $previous);
}
}
