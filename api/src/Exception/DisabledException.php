<?php


namespace App\Exception;


use Throwable;

class DisabledException extends \Exception
{
    public function __construct(
        string $message = 'Account is disabled.',
        int $code = 401,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
