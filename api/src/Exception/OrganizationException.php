<?php


namespace App\Exception;


use Throwable;

class OrganizationException extends \Exception
{
    public function __construct(
        string $message = 'Organization is already locked',
        int $code = 0,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
