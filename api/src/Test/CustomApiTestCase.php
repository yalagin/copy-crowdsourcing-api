<?php

namespace App\Test;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Client;
use App\Entity\Person;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Provider\Uuid;

class CustomApiTestCase extends ApiTestCase
{
    protected function createUser(string $email, string $password,bool $flush = true,bool $person = true): User
    {
        $user = new User();
        $user->setEmail($email);
        $user->setEnabled(true);
        $user->setID(Uuid::uuid());
        $user->setUsername(substr($email, 0, strpos($email, '@')));

        $encoded = self::$container->get('security.password_encoder')
            ->encodePassword($user, $password);
        $user->setPassword($encoded);
        $em = $this->getEntityManager();
        $em->persist($user);

        if($person) {
            $person = new Person();
            $person->setUser($user);
            $person->setID(Uuid::uuid());
            $person->setPhoneNumber('777');
            $em->persist($person);
        }
        if($flush) $em->flush();
        return $user;
    }

    protected function createAdmin(string $email, string $password): User
    {
        $user =$this->createUser( $email, $password,false);
        $user->setRoles([User::ROLE_ADMIN]);
        $em = $this->getEntityManager();
        $em->flush();

        return $user;
    }

    protected function logIn(Client $client, string $email, string $password, bool $assert200 = true) : ?string
    {
        $client->request('POST', '/authentication_token', [
            'headers' => ['Content-Type' => 'application/ld+json'],
            'json' => [
                'email' => $email,
                'password' => $password
            ],
        ]);
        if($assert200) {
            $this->assertResponseStatusCodeSame(200);
            return $client->getResponse()->toArray()['token'];
        }
        return null;
    }

    protected function createUserAndLogIn(Client $client, string $email, string $password, bool $person = true): array
    {
        $user = $this->createUser($email, $password, true, $person);

        $token = $this->logIn($client, $email, $password);

        return [$user,$token];
    }

    protected function createAdminAndLogIn(Client $client, string $email, string $password): array
    {
        $user = $this->createAdmin($email, $password);

        $token = $this->logIn($client, $email, $password);

        return [$user,$token];
    }

    protected function getEntityManager(): EntityManagerInterface
    {
        return self::$container->get('doctrine')->getManager();
    }

    protected function enableAccount(User $user)
    {
        $user->setEnabled(true);
        $em = $this->getEntityManager();
        $em->persist($user);
        $em->flush();
    }

    /**
     * @param string $id
     * @return object|null
     */
    public function enableUser(string $id): object
    {
        $user = $this->getEntityManager()->getRepository(User::class)->find($id);
        $user->setEnabled(true);
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
        return $user;
    }
}
