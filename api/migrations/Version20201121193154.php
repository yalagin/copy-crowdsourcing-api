<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201121193154 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE lesson_image_object (lesson_id UUID NOT NULL, image_object_id UUID NOT NULL, PRIMARY KEY(lesson_id, image_object_id))');
        $this->addSql('CREATE INDEX IDX_9093F547CDF80196 ON lesson_image_object (lesson_id)');
        $this->addSql('CREATE INDEX IDX_9093F547FBAF8D7F ON lesson_image_object (image_object_id)');
        $this->addSql('ALTER TABLE lesson_image_object ADD CONSTRAINT FK_9093F547CDF80196 FOREIGN KEY (lesson_id) REFERENCES lesson (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE lesson_image_object ADD CONSTRAINT FK_9093F547FBAF8D7F FOREIGN KEY (image_object_id) REFERENCES media_object (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP INDEX uniq_16b9077a2956195f');
        $this->addSql('DROP INDEX uniq_16b9077a29c1004e');
        $this->addSql('DROP INDEX uniq_16b9077ac33f7837');
        $this->addSql('ALTER TABLE creative_work ADD audio_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE creative_work ADD CONSTRAINT FK_16B9077A3A3123C7 FOREIGN KEY (audio_id) REFERENCES media_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_16B9077AC33F7837 ON creative_work (document_id)');
        $this->addSql('CREATE INDEX IDX_16B9077A2956195F ON creative_work (archive_id)');
        $this->addSql('CREATE INDEX IDX_16B9077A29C1004E ON creative_work (video_id)');
        $this->addSql('CREATE INDEX IDX_16B9077A3A3123C7 ON creative_work (audio_id)');
        $this->addSql('DROP INDEX uniq_f87474f32956195f');
        $this->addSql('DROP INDEX uniq_f87474f329c1004e');
        $this->addSql('DROP INDEX uniq_f87474f3c33f7837');
        $this->addSql('ALTER TABLE lesson ADD audio_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE lesson ADD CONSTRAINT FK_F87474F33A3123C7 FOREIGN KEY (audio_id) REFERENCES media_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_F87474F3C33F7837 ON lesson (document_id)');
        $this->addSql('CREATE INDEX IDX_F87474F32956195F ON lesson (archive_id)');
        $this->addSql('CREATE INDEX IDX_F87474F329C1004E ON lesson (video_id)');
        $this->addSql('CREATE INDEX IDX_F87474F33A3123C7 ON lesson (audio_id)');
        $this->addSql('ALTER TABLE media_object ADD image_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE media_object ADD animation_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE media_object ADD CONSTRAINT FK_14D431323DA5256D FOREIGN KEY (image_id) REFERENCES media_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE media_object ADD CONSTRAINT FK_14D431323858647E FOREIGN KEY (animation_id) REFERENCES media_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_14D431323DA5256D ON media_object (image_id)');
        $this->addSql('CREATE INDEX IDX_14D431323858647E ON media_object (animation_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE lesson_image_object');
        $this->addSql('ALTER TABLE creative_work DROP CONSTRAINT FK_16B9077A3A3123C7');
        $this->addSql('DROP INDEX IDX_16B9077AC33F7837');
        $this->addSql('DROP INDEX IDX_16B9077A2956195F');
        $this->addSql('DROP INDEX IDX_16B9077A29C1004E');
        $this->addSql('DROP INDEX IDX_16B9077A3A3123C7');
        $this->addSql('ALTER TABLE creative_work DROP audio_id');
        $this->addSql('CREATE UNIQUE INDEX uniq_16b9077a2956195f ON creative_work (archive_id)');
        $this->addSql('CREATE UNIQUE INDEX uniq_16b9077a29c1004e ON creative_work (video_id)');
        $this->addSql('CREATE UNIQUE INDEX uniq_16b9077ac33f7837 ON creative_work (document_id)');
        $this->addSql('ALTER TABLE media_object DROP CONSTRAINT FK_14D431323DA5256D');
        $this->addSql('ALTER TABLE media_object DROP CONSTRAINT FK_14D431323858647E');
        $this->addSql('DROP INDEX IDX_14D431323DA5256D');
        $this->addSql('DROP INDEX IDX_14D431323858647E');
        $this->addSql('ALTER TABLE media_object DROP image_id');
        $this->addSql('ALTER TABLE media_object DROP animation_id');
        $this->addSql('ALTER TABLE lesson DROP CONSTRAINT FK_F87474F33A3123C7');
        $this->addSql('DROP INDEX IDX_F87474F3C33F7837');
        $this->addSql('DROP INDEX IDX_F87474F32956195F');
        $this->addSql('DROP INDEX IDX_F87474F329C1004E');
        $this->addSql('DROP INDEX IDX_F87474F33A3123C7');
        $this->addSql('ALTER TABLE lesson DROP audio_id');
        $this->addSql('CREATE UNIQUE INDEX uniq_f87474f32956195f ON lesson (archive_id)');
        $this->addSql('CREATE UNIQUE INDEX uniq_f87474f329c1004e ON lesson (video_id)');
        $this->addSql('CREATE UNIQUE INDEX uniq_f87474f3c33f7837 ON lesson (document_id)');
    }
}
