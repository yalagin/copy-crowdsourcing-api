<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201223185443 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE creative_work_audio_object (creative_work_id UUID NOT NULL, audio_object_id UUID NOT NULL, PRIMARY KEY(creative_work_id, audio_object_id))');
        $this->addSql('CREATE INDEX IDX_ABC8A67C2DBB54F ON creative_work_audio_object (creative_work_id)');
        $this->addSql('CREATE INDEX IDX_ABC8A67777C5B2B ON creative_work_audio_object (audio_object_id)');
        $this->addSql('CREATE TABLE lesson_audio_object (lesson_id UUID NOT NULL, audio_object_id UUID NOT NULL, PRIMARY KEY(lesson_id, audio_object_id))');
        $this->addSql('CREATE INDEX IDX_3D81AA29CDF80196 ON lesson_audio_object (lesson_id)');
        $this->addSql('CREATE INDEX IDX_3D81AA29777C5B2B ON lesson_audio_object (audio_object_id)');
        $this->addSql('ALTER TABLE creative_work_audio_object ADD CONSTRAINT FK_ABC8A67C2DBB54F FOREIGN KEY (creative_work_id) REFERENCES creative_work (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE creative_work_audio_object ADD CONSTRAINT FK_ABC8A67777C5B2B FOREIGN KEY (audio_object_id) REFERENCES media_object (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE lesson_audio_object ADD CONSTRAINT FK_3D81AA29CDF80196 FOREIGN KEY (lesson_id) REFERENCES lesson (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE lesson_audio_object ADD CONSTRAINT FK_3D81AA29777C5B2B FOREIGN KEY (audio_object_id) REFERENCES media_object (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE creative_work DROP CONSTRAINT fk_16b9077a3a3123c7');
        $this->addSql('DROP INDEX idx_16b9077a3a3123c7');
        $this->addSql('ALTER TABLE creative_work DROP audio_id');
        $this->addSql('ALTER TABLE lesson DROP CONSTRAINT fk_f87474f33a3123c7');
        $this->addSql('DROP INDEX idx_f87474f33a3123c7');
        $this->addSql('ALTER TABLE lesson DROP audio_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE creative_work_audio_object');
        $this->addSql('DROP TABLE lesson_audio_object');
        $this->addSql('ALTER TABLE creative_work ADD audio_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE creative_work ADD CONSTRAINT fk_16b9077a3a3123c7 FOREIGN KEY (audio_id) REFERENCES media_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_16b9077a3a3123c7 ON creative_work (audio_id)');
        $this->addSql('ALTER TABLE lesson ADD audio_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE lesson ADD CONSTRAINT fk_f87474f33a3123c7 FOREIGN KEY (audio_id) REFERENCES media_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_f87474f33a3123c7 ON lesson (audio_id)');
    }
}
