<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201025182543 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE award (id UUID NOT NULL, project_id UUID NOT NULL, winner_id UUID NOT NULL, name TEXT DEFAULT NULL, rank INT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_8A5B2EE7166D1F9C ON award (project_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8A5B2EE75DFCD4B8 ON award (winner_id)');
        $this->addSql('CREATE TABLE subtheme (id UUID NOT NULL, theme_id UUID DEFAULT NULL, name TEXT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_EDC608D259027487 ON subtheme (theme_id)');
        $this->addSql('CREATE TABLE theme (id UUID NOT NULL, subject_id UUID DEFAULT NULL, name TEXT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_9775E70823EDC87 ON theme (subject_id)');
        $this->addSql('ALTER TABLE award ADD CONSTRAINT FK_8A5B2EE7166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE award ADD CONSTRAINT FK_8A5B2EE75DFCD4B8 FOREIGN KEY (winner_id) REFERENCES organization (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE subtheme ADD CONSTRAINT FK_EDC608D259027487 FOREIGN KEY (theme_id) REFERENCES theme (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE theme ADD CONSTRAINT FK_9775E70823EDC87 FOREIGN KEY (subject_id) REFERENCES tags (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE project ADD subtheme_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE project ADD is_draft BOOLEAN DEFAULT NULL');
        $this->addSql('ALTER TABLE project ADD material TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE project RENAME COLUMN theme TO title');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EE5292C90E FOREIGN KEY (subtheme_id) REFERENCES subtheme (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_2FB3D0EE5292C90E ON project (subtheme_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE project DROP CONSTRAINT FK_2FB3D0EE5292C90E');
        $this->addSql('ALTER TABLE subtheme DROP CONSTRAINT FK_EDC608D259027487');
        $this->addSql('DROP TABLE award');
        $this->addSql('DROP TABLE subtheme');
        $this->addSql('DROP TABLE theme');
        $this->addSql('DROP INDEX IDX_2FB3D0EE5292C90E');
        $this->addSql('ALTER TABLE project ADD theme TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE project DROP subtheme_id');
        $this->addSql('ALTER TABLE project DROP title');
        $this->addSql('ALTER TABLE project DROP is_draft');
        $this->addSql('ALTER TABLE project DROP material');
    }
}
