<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201224150745 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE lesson ADD derivative_for_youtube_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE lesson ADD derivative_for_titik_pintar_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE lesson ADD derivative_for_sahabat_pintar_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE lesson ADD CONSTRAINT FK_F87474F34802CE81 FOREIGN KEY (derivative_for_youtube_id) REFERENCES media_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE lesson ADD CONSTRAINT FK_F87474F352924718 FOREIGN KEY (derivative_for_titik_pintar_id) REFERENCES media_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE lesson ADD CONSTRAINT FK_F87474F39F62EDB FOREIGN KEY (derivative_for_sahabat_pintar_id) REFERENCES media_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_F87474F34802CE81 ON lesson (derivative_for_youtube_id)');
        $this->addSql('CREATE INDEX IDX_F87474F352924718 ON lesson (derivative_for_titik_pintar_id)');
        $this->addSql('CREATE INDEX IDX_F87474F39F62EDB ON lesson (derivative_for_sahabat_pintar_id)');
        $this->addSql('ALTER TABLE media_object ADD type TEXT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE media_object DROP type');
        $this->addSql('ALTER TABLE lesson DROP CONSTRAINT FK_F87474F34802CE81');
        $this->addSql('ALTER TABLE lesson DROP CONSTRAINT FK_F87474F352924718');
        $this->addSql('ALTER TABLE lesson DROP CONSTRAINT FK_F87474F39F62EDB');
        $this->addSql('DROP INDEX IDX_F87474F34802CE81');
        $this->addSql('DROP INDEX IDX_F87474F352924718');
        $this->addSql('DROP INDEX IDX_F87474F39F62EDB');
        $this->addSql('ALTER TABLE lesson DROP derivative_for_youtube_id');
        $this->addSql('ALTER TABLE lesson DROP derivative_for_titik_pintar_id');
        $this->addSql('ALTER TABLE lesson DROP derivative_for_sahabat_pintar_id');
    }
}
