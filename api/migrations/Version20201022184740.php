<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201022184740 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";');
        $this->addSql('CREATE SEQUENCE refresh_tokens_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE address_region (id UUID NOT NULL, name VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE answer (id UUID NOT NULL, creator_id UUID DEFAULT NULL, parent_item_id UUID NOT NULL, description TEXT DEFAULT NULL, correct BOOLEAN DEFAULT NULL, ordering INT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_DADD4A2561220EA6 ON answer (creator_id)');
        $this->addSql('CREATE INDEX IDX_DADD4A2560272618 ON answer (parent_item_id)');
        $this->addSql('CREATE TABLE bank (id UUID NOT NULL, name TEXT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE bank_account (id UUID NOT NULL, bank_id UUID NOT NULL, owner_id UUID NOT NULL, holder TEXT DEFAULT NULL, account_number TEXT DEFAULT NULL, additional_info TEXT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_53A23E0A11C8FB41 ON bank_account (bank_id)');
        $this->addSql('CREATE INDEX IDX_53A23E0A7E3C61F9 ON bank_account (owner_id)');
        $this->addSql('CREATE TABLE contact (id UUID NOT NULL, user_id UUID NOT NULL, person_id UUID NOT NULL, name TEXT DEFAULT NULL, value TEXT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4C62E638A76ED395 ON contact (user_id)');
        $this->addSql('CREATE INDEX IDX_4C62E638217BBB47 ON contact (person_id)');
        $this->addSql('CREATE TABLE creative_work (id UUID NOT NULL, creator_id UUID NOT NULL, created_for_id UUID DEFAULT NULL, document_id UUID DEFAULT NULL, archive_id UUID DEFAULT NULL, video_id UUID DEFAULT NULL, transcript TEXT DEFAULT NULL, date_published TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, is_submitted BOOLEAN DEFAULT NULL, rank INT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_16B9077A61220EA6 ON creative_work (creator_id)');
        $this->addSql('CREATE INDEX IDX_16B9077A2F97E6E2 ON creative_work (created_for_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_16B9077AC33F7837 ON creative_work (document_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_16B9077A2956195F ON creative_work (archive_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_16B9077A29C1004E ON creative_work (video_id)');
        $this->addSql('CREATE TABLE educational_level (id UUID NOT NULL, level TEXT DEFAULT NULL, ordering INT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE ext_translations (id SERIAL NOT NULL, locale VARCHAR(8) NOT NULL, object_class VARCHAR(191) NOT NULL, field VARCHAR(32) NOT NULL, foreign_key VARCHAR(64) NOT NULL, content TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX translations_lookup_idx ON ext_translations (locale, object_class, foreign_key)');
        $this->addSql('CREATE UNIQUE INDEX lookup_unique_idx ON ext_translations (locale, object_class, field, foreign_key)');
        $this->addSql('CREATE TABLE lesson (id UUID NOT NULL, creator_id UUID NOT NULL, created_for_id UUID NOT NULL, original_creative_work_id UUID DEFAULT NULL, document_id UUID DEFAULT NULL, archive_id UUID DEFAULT NULL, video_id UUID DEFAULT NULL, transcript TEXT DEFAULT NULL, published BOOLEAN NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F87474F361220EA6 ON lesson (creator_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F87474F32F97E6E2 ON lesson (created_for_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F87474F3CF74F395 ON lesson (original_creative_work_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F87474F3C33F7837 ON lesson (document_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F87474F32956195F ON lesson (archive_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F87474F329C1004E ON lesson (video_id)');
        $this->addSql('CREATE TABLE lesson_answer (id UUID NOT NULL, parent_item_id UUID NOT NULL, original_answer_id UUID DEFAULT NULL, description TEXT NOT NULL, correct BOOLEAN DEFAULT NULL, ordering INT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A94BBA7260272618 ON lesson_answer (parent_item_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A94BBA72C0A03712 ON lesson_answer (original_answer_id)');
        $this->addSql('CREATE TABLE lesson_answer_translations (id SERIAL NOT NULL, locale VARCHAR(8) NOT NULL, object_class VARCHAR(191) NOT NULL, field VARCHAR(32) NOT NULL, foreign_key VARCHAR(64) NOT NULL, content TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX lesson_answer_translation_idx ON lesson_answer_translations (locale, object_class, field, foreign_key)');
        $this->addSql('CREATE TABLE lesson_question (id UUID NOT NULL, is_part_of_id UUID NOT NULL, image_id UUID DEFAULT NULL, original_question_id UUID DEFAULT NULL, description TEXT DEFAULT NULL, visual_brief TEXT DEFAULT NULL, answer_key TEXT DEFAULT NULL, type TEXT NOT NULL, ordering INT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_FEB003571474703B ON lesson_question (is_part_of_id)');
        $this->addSql('CREATE INDEX IDX_FEB003573DA5256D ON lesson_question (image_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_FEB00357E9281695 ON lesson_question (original_question_id)');
        $this->addSql('CREATE TABLE lesson_question_translations (id SERIAL NOT NULL, locale VARCHAR(8) NOT NULL, object_class VARCHAR(191) NOT NULL, field VARCHAR(32) NOT NULL, foreign_key VARCHAR(64) NOT NULL, content TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX lesson_question_translation_idx ON lesson_question_translations (locale, object_class, field, foreign_key)');
        $this->addSql('CREATE TABLE lesson_translations (id SERIAL NOT NULL, locale VARCHAR(8) NOT NULL, object_class VARCHAR(191) NOT NULL, field VARCHAR(32) NOT NULL, foreign_key VARCHAR(64) NOT NULL, content TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX lesson_translation_idx ON lesson_translations (locale, object_class, field, foreign_key)');
        $this->addSql('CREATE TABLE locale (id UUID NOT NULL, code VARCHAR(7) NOT NULL, name VARCHAR(45) NOT NULL, is_default BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE media_object (id UUID NOT NULL, author_id UUID DEFAULT NULL, file_path VARCHAR(255) DEFAULT NULL, name TEXT DEFAULT NULL, url TEXT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, discr VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_14D43132F675F31B ON media_object (author_id)');
        $this->addSql('CREATE TABLE organization (id UUID NOT NULL, founder_id UUID DEFAULT NULL, image_id UUID DEFAULT NULL, project_id UUID DEFAULT NULL, name TEXT DEFAULT NULL, is_open_for_join BOOLEAN DEFAULT NULL, is_locked BOOLEAN DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_C1EE637C19113B3C ON organization (founder_id)');
        $this->addSql('CREATE INDEX IDX_C1EE637C3DA5256D ON organization (image_id)');
        $this->addSql('CREATE INDEX IDX_C1EE637C166D1F9C ON organization (project_id)');
        $this->addSql('CREATE TABLE organization_user (organization_id UUID NOT NULL, user_id UUID NOT NULL, PRIMARY KEY(organization_id, user_id))');
        $this->addSql('CREATE INDEX IDX_B49AE8D432C8A3DE ON organization_user (organization_id)');
        $this->addSql('CREATE INDEX IDX_B49AE8D4A76ED395 ON organization_user (user_id)');
        $this->addSql('CREATE TABLE organization_unconfirmed_members (id UUID NOT NULL, organization_id UUID DEFAULT NULL, member_id UUID DEFAULT NULL, status VARCHAR(255) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E43AD2032C8A3DE ON organization_unconfirmed_members (organization_id)');
        $this->addSql('CREATE INDEX IDX_E43AD207597D3FE ON organization_unconfirmed_members (member_id)');
        $this->addSql('CREATE TABLE person (id UUID NOT NULL, user_id UUID NOT NULL, image_id UUID DEFAULT NULL, address_region_id UUID DEFAULT NULL, works_for TEXT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_34DCD176A76ED395 ON person (user_id)');
        $this->addSql('CREATE INDEX IDX_34DCD1763DA5256D ON person (image_id)');
        $this->addSql('CREATE INDEX IDX_34DCD1762673D1D ON person (address_region_id)');
        $this->addSql('CREATE TABLE project (id UUID NOT NULL, educational_level_id UUID DEFAULT NULL, winner_id UUID DEFAULT NULL, theme TEXT DEFAULT NULL, objective TEXT DEFAULT NULL, activity TEXT DEFAULT NULL, assessment TEXT DEFAULT NULL, start_time TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, end_time TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, minimum_number_of_questions INT NOT NULL, award TEXT DEFAULT NULL, email_sent BOOLEAN NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_2FB3D0EE98C901D6 ON project (educational_level_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2FB3D0EE5DFCD4B8 ON project (winner_id)');
        $this->addSql('CREATE TABLE project_tags (project_id UUID NOT NULL, tags_id UUID NOT NULL, PRIMARY KEY(project_id, tags_id))');
        $this->addSql('CREATE INDEX IDX_562D5C3E166D1F9C ON project_tags (project_id)');
        $this->addSql('CREATE INDEX IDX_562D5C3E8D7B4FB4 ON project_tags (tags_id)');
        $this->addSql('CREATE TABLE question (id UUID NOT NULL, creator_id UUID DEFAULT NULL, is_part_of_id UUID NOT NULL, image_id UUID DEFAULT NULL, description TEXT DEFAULT NULL, visual_brief TEXT DEFAULT NULL, answer_key TEXT DEFAULT NULL, type TEXT NOT NULL, ordering INT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B6F7494E61220EA6 ON question (creator_id)');
        $this->addSql('CREATE INDEX IDX_B6F7494E1474703B ON question (is_part_of_id)');
        $this->addSql('CREATE INDEX IDX_B6F7494E3DA5256D ON question (image_id)');
        $this->addSql('CREATE TABLE refresh_tokens (id INT NOT NULL, refresh_token VARCHAR(128) NOT NULL, username VARCHAR(255) NOT NULL, valid TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9BACE7E1C74F2195 ON refresh_tokens (refresh_token)');
        $this->addSql('CREATE TABLE review (id UUID NOT NULL, author_id UUID DEFAULT NULL, item_reviewed_id UUID NOT NULL, organization_id UUID NOT NULL, review_body TEXT DEFAULT NULL, review_rating INT DEFAULT NULL, is_submitted BOOLEAN DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_794381C6F675F31B ON review (author_id)');
        $this->addSql('CREATE INDEX IDX_794381C6E54D1663 ON review (item_reviewed_id)');
        $this->addSql('CREATE INDEX IDX_794381C632C8A3DE ON review (organization_id)');
        $this->addSql('CREATE TABLE review_tags (review_id UUID NOT NULL, tags_id UUID NOT NULL, PRIMARY KEY(review_id, tags_id))');
        $this->addSql('CREATE INDEX IDX_8D08D93E3E2E969B ON review_tags (review_id)');
        $this->addSql('CREATE INDEX IDX_8D08D93E8D7B4FB4 ON review_tags (tags_id)');
        $this->addSql('CREATE TABLE tags (id UUID NOT NULL, role TEXT NOT NULL, tag TEXT NOT NULL, name TEXT NOT NULL, value TEXT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE "user" (id UUID NOT NULL, email VARCHAR(180) NOT NULL, roles JSON DEFAULT NULL, password VARCHAR(255) NOT NULL, username VARCHAR(255) NOT NULL, password_change_date INT DEFAULT NULL, enabled BOOLEAN NOT NULL, confirmation_token VARCHAR(40) DEFAULT NULL, password_reset_token VARCHAR(40) DEFAULT NULL, password_reset_token_time_stamp TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON "user" (email)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649F85E0677 ON "user" (username)');
        $this->addSql('CREATE TABLE user_tags (user_id UUID NOT NULL, tags_id UUID NOT NULL, PRIMARY KEY(user_id, tags_id))');
        $this->addSql('CREATE INDEX IDX_153DD8EFA76ED395 ON user_tags (user_id)');
        $this->addSql('CREATE INDEX IDX_153DD8EF8D7B4FB4 ON user_tags (tags_id)');
        $this->addSql('ALTER TABLE answer ADD CONSTRAINT FK_DADD4A2561220EA6 FOREIGN KEY (creator_id) REFERENCES organization (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE answer ADD CONSTRAINT FK_DADD4A2560272618 FOREIGN KEY (parent_item_id) REFERENCES question (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bank_account ADD CONSTRAINT FK_53A23E0A11C8FB41 FOREIGN KEY (bank_id) REFERENCES bank (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bank_account ADD CONSTRAINT FK_53A23E0A7E3C61F9 FOREIGN KEY (owner_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE contact ADD CONSTRAINT FK_4C62E638A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE contact ADD CONSTRAINT FK_4C62E638217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE creative_work ADD CONSTRAINT FK_16B9077A61220EA6 FOREIGN KEY (creator_id) REFERENCES organization (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE creative_work ADD CONSTRAINT FK_16B9077A2F97E6E2 FOREIGN KEY (created_for_id) REFERENCES project (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE creative_work ADD CONSTRAINT FK_16B9077AC33F7837 FOREIGN KEY (document_id) REFERENCES media_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE creative_work ADD CONSTRAINT FK_16B9077A2956195F FOREIGN KEY (archive_id) REFERENCES media_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE creative_work ADD CONSTRAINT FK_16B9077A29C1004E FOREIGN KEY (video_id) REFERENCES media_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE lesson ADD CONSTRAINT FK_F87474F361220EA6 FOREIGN KEY (creator_id) REFERENCES organization (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE lesson ADD CONSTRAINT FK_F87474F32F97E6E2 FOREIGN KEY (created_for_id) REFERENCES project (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE lesson ADD CONSTRAINT FK_F87474F3CF74F395 FOREIGN KEY (original_creative_work_id) REFERENCES creative_work (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE lesson ADD CONSTRAINT FK_F87474F3C33F7837 FOREIGN KEY (document_id) REFERENCES media_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE lesson ADD CONSTRAINT FK_F87474F32956195F FOREIGN KEY (archive_id) REFERENCES media_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE lesson ADD CONSTRAINT FK_F87474F329C1004E FOREIGN KEY (video_id) REFERENCES media_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE lesson_answer ADD CONSTRAINT FK_A94BBA7260272618 FOREIGN KEY (parent_item_id) REFERENCES lesson_question (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE lesson_answer ADD CONSTRAINT FK_A94BBA72C0A03712 FOREIGN KEY (original_answer_id) REFERENCES answer (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE lesson_question ADD CONSTRAINT FK_FEB003571474703B FOREIGN KEY (is_part_of_id) REFERENCES lesson (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE lesson_question ADD CONSTRAINT FK_FEB003573DA5256D FOREIGN KEY (image_id) REFERENCES media_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE lesson_question ADD CONSTRAINT FK_FEB00357E9281695 FOREIGN KEY (original_question_id) REFERENCES question (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE media_object ADD CONSTRAINT FK_14D43132F675F31B FOREIGN KEY (author_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE organization ADD CONSTRAINT FK_C1EE637C19113B3C FOREIGN KEY (founder_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE organization ADD CONSTRAINT FK_C1EE637C3DA5256D FOREIGN KEY (image_id) REFERENCES media_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE organization ADD CONSTRAINT FK_C1EE637C166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE organization_user ADD CONSTRAINT FK_B49AE8D432C8A3DE FOREIGN KEY (organization_id) REFERENCES organization (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE organization_user ADD CONSTRAINT FK_B49AE8D4A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE organization_unconfirmed_members ADD CONSTRAINT FK_E43AD2032C8A3DE FOREIGN KEY (organization_id) REFERENCES organization (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE organization_unconfirmed_members ADD CONSTRAINT FK_E43AD207597D3FE FOREIGN KEY (member_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE person ADD CONSTRAINT FK_34DCD176A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE person ADD CONSTRAINT FK_34DCD1763DA5256D FOREIGN KEY (image_id) REFERENCES media_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE person ADD CONSTRAINT FK_34DCD1762673D1D FOREIGN KEY (address_region_id) REFERENCES address_region (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EE98C901D6 FOREIGN KEY (educational_level_id) REFERENCES educational_level (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EE5DFCD4B8 FOREIGN KEY (winner_id) REFERENCES creative_work (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE project_tags ADD CONSTRAINT FK_562D5C3E166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE project_tags ADD CONSTRAINT FK_562D5C3E8D7B4FB4 FOREIGN KEY (tags_id) REFERENCES tags (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE question ADD CONSTRAINT FK_B6F7494E61220EA6 FOREIGN KEY (creator_id) REFERENCES organization (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE question ADD CONSTRAINT FK_B6F7494E1474703B FOREIGN KEY (is_part_of_id) REFERENCES creative_work (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE question ADD CONSTRAINT FK_B6F7494E3DA5256D FOREIGN KEY (image_id) REFERENCES media_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE review ADD CONSTRAINT FK_794381C6F675F31B FOREIGN KEY (author_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE review ADD CONSTRAINT FK_794381C6E54D1663 FOREIGN KEY (item_reviewed_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE review ADD CONSTRAINT FK_794381C632C8A3DE FOREIGN KEY (organization_id) REFERENCES organization (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE review_tags ADD CONSTRAINT FK_8D08D93E3E2E969B FOREIGN KEY (review_id) REFERENCES review (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE review_tags ADD CONSTRAINT FK_8D08D93E8D7B4FB4 FOREIGN KEY (tags_id) REFERENCES tags (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_tags ADD CONSTRAINT FK_153DD8EFA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_tags ADD CONSTRAINT FK_153DD8EF8D7B4FB4 FOREIGN KEY (tags_id) REFERENCES tags (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE TABLE image_object_translations (id SERIAL NOT NULL, locale VARCHAR(8) NOT NULL, object_class VARCHAR(191) NOT NULL, field VARCHAR(32) NOT NULL, foreign_key VARCHAR(64) NOT NULL, content TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX image_object_translation_idx ON image_object_translations (locale, object_class, field, foreign_key)');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE person DROP CONSTRAINT FK_34DCD1762673D1D');
        $this->addSql('ALTER TABLE lesson_answer DROP CONSTRAINT FK_A94BBA72C0A03712');
        $this->addSql('ALTER TABLE bank_account DROP CONSTRAINT FK_53A23E0A11C8FB41');
        $this->addSql('ALTER TABLE lesson DROP CONSTRAINT FK_F87474F3CF74F395');
        $this->addSql('ALTER TABLE project DROP CONSTRAINT FK_2FB3D0EE5DFCD4B8');
        $this->addSql('ALTER TABLE question DROP CONSTRAINT FK_B6F7494E1474703B');
        $this->addSql('ALTER TABLE project DROP CONSTRAINT FK_2FB3D0EE98C901D6');
        $this->addSql('ALTER TABLE lesson_question DROP CONSTRAINT FK_FEB003571474703B');
        $this->addSql('ALTER TABLE lesson_answer DROP CONSTRAINT FK_A94BBA7260272618');
        $this->addSql('ALTER TABLE creative_work DROP CONSTRAINT FK_16B9077AC33F7837');
        $this->addSql('ALTER TABLE creative_work DROP CONSTRAINT FK_16B9077A2956195F');
        $this->addSql('ALTER TABLE creative_work DROP CONSTRAINT FK_16B9077A29C1004E');
        $this->addSql('ALTER TABLE lesson DROP CONSTRAINT FK_F87474F3C33F7837');
        $this->addSql('ALTER TABLE lesson DROP CONSTRAINT FK_F87474F32956195F');
        $this->addSql('ALTER TABLE lesson DROP CONSTRAINT FK_F87474F329C1004E');
        $this->addSql('ALTER TABLE lesson_question DROP CONSTRAINT FK_FEB003573DA5256D');
        $this->addSql('ALTER TABLE organization DROP CONSTRAINT FK_C1EE637C3DA5256D');
        $this->addSql('ALTER TABLE person DROP CONSTRAINT FK_34DCD1763DA5256D');
        $this->addSql('ALTER TABLE question DROP CONSTRAINT FK_B6F7494E3DA5256D');
        $this->addSql('ALTER TABLE answer DROP CONSTRAINT FK_DADD4A2561220EA6');
        $this->addSql('ALTER TABLE creative_work DROP CONSTRAINT FK_16B9077A61220EA6');
        $this->addSql('ALTER TABLE lesson DROP CONSTRAINT FK_F87474F361220EA6');
        $this->addSql('ALTER TABLE organization_user DROP CONSTRAINT FK_B49AE8D432C8A3DE');
        $this->addSql('ALTER TABLE organization_unconfirmed_members DROP CONSTRAINT FK_E43AD2032C8A3DE');
        $this->addSql('ALTER TABLE question DROP CONSTRAINT FK_B6F7494E61220EA6');
        $this->addSql('ALTER TABLE review DROP CONSTRAINT FK_794381C632C8A3DE');
        $this->addSql('ALTER TABLE contact DROP CONSTRAINT FK_4C62E638217BBB47');
        $this->addSql('ALTER TABLE creative_work DROP CONSTRAINT FK_16B9077A2F97E6E2');
        $this->addSql('ALTER TABLE lesson DROP CONSTRAINT FK_F87474F32F97E6E2');
        $this->addSql('ALTER TABLE organization DROP CONSTRAINT FK_C1EE637C166D1F9C');
        $this->addSql('ALTER TABLE project_tags DROP CONSTRAINT FK_562D5C3E166D1F9C');
        $this->addSql('ALTER TABLE answer DROP CONSTRAINT FK_DADD4A2560272618');
        $this->addSql('ALTER TABLE lesson_question DROP CONSTRAINT FK_FEB00357E9281695');
        $this->addSql('ALTER TABLE review_tags DROP CONSTRAINT FK_8D08D93E3E2E969B');
        $this->addSql('ALTER TABLE project_tags DROP CONSTRAINT FK_562D5C3E8D7B4FB4');
        $this->addSql('ALTER TABLE review_tags DROP CONSTRAINT FK_8D08D93E8D7B4FB4');
        $this->addSql('ALTER TABLE user_tags DROP CONSTRAINT FK_153DD8EF8D7B4FB4');
        $this->addSql('ALTER TABLE bank_account DROP CONSTRAINT FK_53A23E0A7E3C61F9');
        $this->addSql('ALTER TABLE contact DROP CONSTRAINT FK_4C62E638A76ED395');
        $this->addSql('ALTER TABLE media_object DROP CONSTRAINT FK_14D43132F675F31B');
        $this->addSql('ALTER TABLE organization DROP CONSTRAINT FK_C1EE637C19113B3C');
        $this->addSql('ALTER TABLE organization_user DROP CONSTRAINT FK_B49AE8D4A76ED395');
        $this->addSql('ALTER TABLE organization_unconfirmed_members DROP CONSTRAINT FK_E43AD207597D3FE');
        $this->addSql('ALTER TABLE person DROP CONSTRAINT FK_34DCD176A76ED395');
        $this->addSql('ALTER TABLE review DROP CONSTRAINT FK_794381C6F675F31B');
        $this->addSql('ALTER TABLE review DROP CONSTRAINT FK_794381C6E54D1663');
        $this->addSql('ALTER TABLE user_tags DROP CONSTRAINT FK_153DD8EFA76ED395');
        $this->addSql('DROP SEQUENCE refresh_tokens_id_seq CASCADE');
        $this->addSql('DROP TABLE address_region');
        $this->addSql('DROP TABLE answer');
        $this->addSql('DROP TABLE bank');
        $this->addSql('DROP TABLE bank_account');
        $this->addSql('DROP TABLE contact');
        $this->addSql('DROP TABLE creative_work');
        $this->addSql('DROP TABLE educational_level');
        $this->addSql('DROP TABLE ext_translations');
        $this->addSql('DROP TABLE lesson');
        $this->addSql('DROP TABLE lesson_answer');
        $this->addSql('DROP TABLE lesson_answer_translations');
        $this->addSql('DROP TABLE lesson_question');
        $this->addSql('DROP TABLE lesson_question_translations');
        $this->addSql('DROP TABLE lesson_translations');
        $this->addSql('DROP TABLE locale');
        $this->addSql('DROP TABLE image_object_translations');
        $this->addSql('DROP TABLE media_object');
        $this->addSql('DROP TABLE organization');
        $this->addSql('DROP TABLE organization_user');
        $this->addSql('DROP TABLE organization_unconfirmed_members');
        $this->addSql('DROP TABLE person');
        $this->addSql('DROP TABLE project');
        $this->addSql('DROP TABLE project_tags');
        $this->addSql('DROP TABLE question');
        $this->addSql('DROP TABLE refresh_tokens');
        $this->addSql('DROP TABLE review');
        $this->addSql('DROP TABLE review_tags');
        $this->addSql('DROP TABLE tags');
        $this->addSql('DROP TABLE "user"');
        $this->addSql('DROP TABLE user_tags');
    }
}
