<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201103154949 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE image_object_translations_id_seq CASCADE');
        $this->addSql('DROP TABLE image_object_translations');
        $this->addSql('ALTER TABLE lesson_question ADD image_en_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE lesson_question ADD CONSTRAINT FK_FEB00357C24C179B FOREIGN KEY (image_en_id) REFERENCES media_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_FEB00357C24C179B ON lesson_question (image_en_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE SEQUENCE image_object_translations_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE image_object_translations (id SERIAL NOT NULL, locale VARCHAR(8) NOT NULL, object_class VARCHAR(191) NOT NULL, field VARCHAR(32) NOT NULL, foreign_key VARCHAR(64) NOT NULL, content TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX image_object_translation_idx ON image_object_translations (locale, object_class, field, foreign_key)');
        $this->addSql('ALTER TABLE lesson_question DROP CONSTRAINT FK_FEB00357C24C179B');
        $this->addSql('DROP INDEX IDX_FEB00357C24C179B');
        $this->addSql('ALTER TABLE lesson_question DROP image_en_id');
    }
}
