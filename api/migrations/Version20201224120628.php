<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201224120628 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE organization_unconfirmed_members ADD email_sent_counter SMALLINT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE organization_unconfirmed_members DROP new_invitation');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE organization_unconfirmed_members ADD new_invitation BOOLEAN DEFAULT NULL');
        $this->addSql('ALTER TABLE organization_unconfirmed_members DROP email_sent_counter');
    }
}
