<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201113171749 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE creative_work_image_object (creative_work_id UUID NOT NULL, image_object_id UUID NOT NULL, PRIMARY KEY(creative_work_id, image_object_id))');
        $this->addSql('CREATE INDEX IDX_A7AED509C2DBB54F ON creative_work_image_object (creative_work_id)');
        $this->addSql('CREATE INDEX IDX_A7AED509FBAF8D7F ON creative_work_image_object (image_object_id)');
        $this->addSql('ALTER TABLE creative_work_image_object ADD CONSTRAINT FK_A7AED509C2DBB54F FOREIGN KEY (creative_work_id) REFERENCES creative_work (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE creative_work_image_object ADD CONSTRAINT FK_A7AED509FBAF8D7F FOREIGN KEY (image_object_id) REFERENCES media_object (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE creative_work_image_object');
    }
}
