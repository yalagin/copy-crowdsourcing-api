<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201103162517 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE project DROP CONSTRAINT fk_2fb3d0ee5dfcd4b8');
        $this->addSql('DROP INDEX uniq_2fb3d0ee5dfcd4b8');
        $this->addSql('ALTER TABLE project DROP winner_id');
        $this->addSql('ALTER TABLE project DROP award');
        $this->addSql('ALTER TABLE project DROP email_sent');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE project ADD winner_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE project ADD award TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE project ADD email_sent BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT fk_2fb3d0ee5dfcd4b8 FOREIGN KEY (winner_id) REFERENCES creative_work (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX uniq_2fb3d0ee5dfcd4b8 ON project (winner_id)');
    }
}
